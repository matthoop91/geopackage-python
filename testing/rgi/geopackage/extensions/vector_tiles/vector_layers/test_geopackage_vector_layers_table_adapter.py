#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from pytest import raises

from rgi.geopackage.extensions.vector_tiles.geopackage_geojson_vector_tiles_table_adapter import \
    GeoPackageGeoJSONVectorTilesTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import \
    GeoPackageVectorLayersTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_layers.vector_layers_entry import VectorLayersEntry
from rgi.geopackage.extensions.vector_tiles.vector_tiles_constants import GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME
from rgi.geopackage.utility.sql_utility import get_database_connection
from testing.rgi.test_utility import create_table


class TestGeoPackageVectorLayersTableAdapter(object):

    def test_get_all_entries_invalid_vector_layers_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=db.cursor())

    def test_get_all_entries_by_table_name_invalid_vector_layers_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name(cursor=db.cursor(),
                                                                                          vector_tiles_table_name='tables')

    def test_get_all_entries_by_table_name_and_layer_name_invalid_vector_layers_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name_and_layer_name(
                    cursor=db.cursor(),
                    vector_tiles_table_name='tables',
                    layer_name='layer')

    def test_insert_or_update_invalid_vector_layers_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)

            layer_entry = VectorLayersEntry(table_name="vector-table",
                                            name='layer name',
                                            description='my description',
                                            min_zoom=3,
                                            max_zoom=12)
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=db.cursor(),
                                                                                            vector_tile_layer_entry=layer_entry)

    def test_insert_or_update_bulk_invalid_vector_layers_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)

            layer_entry = VectorLayersEntry(table_name="vector-table",
                                            name='layer name',
                                            description='my description',
                                            min_zoom=3,
                                            max_zoom=12)

            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.insert_vector_layers_entries_bulk(cursor=db.cursor(),
                                                                                     vector_layers_entries=[
                                                                                         layer_entry])

    def test_all_vector_layer_entries_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

    def test_all_vector_layer_entries_not_empty(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageGeoJSONVectorTilesTableAdapter.create_default_tiles_tables(cursor=cursor)

            expected_layer_entry_1 = VectorLayersEntry(table_name="vector-table",
                                                       name='layer name',
                                                       description='my description',
                                                       min_zoom=3,
                                                       max_zoom=12)

            # add the vector layer entry to the table
            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        expected_layer_entry_1)
            # make sure the id has been updated
            assert expected_layer_entry_1.identifier is not None

            # test to see if the retrieval works
            vector_layer_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(vector_layer_entries) == 1

            # check to see if the values are what is expected
            TestGeoPackageVectorLayersTableAdapter.assert_vector_layer_entries_are_equal(
                expected_vector_layer_entry=expected_layer_entry_1,
                actual_vector_layer_entry=vector_layer_entries[0])

    def test_all_vector_layer_entries_two_entries(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageGeoJSONVectorTilesTableAdapter.create_default_tiles_tables(cursor=cursor)

            expected_layer_entry_1 = VectorLayersEntry(table_name="vector-table",
                                                       name='layer name',
                                                       description='my description',
                                                       min_zoom=3,
                                                       max_zoom=12)

            # add the vector layer entry to the table
            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        expected_layer_entry_1)
            # make sure the id has been updated
            assert expected_layer_entry_1.identifier is not None

            # test to see if the retrieval works
            vector_layer_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(vector_layer_entries) == 1

            # check to see if the values are what is expected
            TestGeoPackageVectorLayersTableAdapter.assert_vector_layer_entries_are_equal(expected_layer_entry_1,
                                                                                         vector_layer_entries[0])

            # add a second entry to the table
            expected_layer_entry_2 = VectorLayersEntry(table_name='vector-table2',
                                                       name='layer2',
                                                       description='mdsfa',
                                                       min_zoom=1,
                                                       max_zoom=18)

            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        expected_layer_entry_2)

            # make sure the id has been updated
            assert expected_layer_entry_2.identifier is not None

            # test to see if the retrieval works
            vector_layer_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(vector_layer_entries) == 2

            # check to see if the values are what is expected
            TestGeoPackageVectorLayersTableAdapter.assert_vector_layer_entries_are_equal(
                expected_vector_layer_entry=expected_layer_entry_1,
                actual_vector_layer_entry=next(vector_layer_entry
                                               for vector_layer_entry
                                               in vector_layer_entries
                                               if vector_layer_entry.identifier ==
                                               expected_layer_entry_1.identifier))

            # check to see if the values are what is expected
            TestGeoPackageVectorLayersTableAdapter.assert_vector_layer_entries_are_equal(
                expected_vector_layer_entry=expected_layer_entry_2,
                actual_vector_layer_entry=next(vector_layer_entry
                                               for vector_layer_entry
                                               in vector_layer_entries
                                               if vector_layer_entry.identifier ==
                                               expected_layer_entry_2.identifier))

    def test_get_vector_layers_by_table_name_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name(cursor=cursor,
                                                                                          vector_tiles_table_name
                                                                                          ='tablesss')

    def test_insert_or_update_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            expected_layer_entry_1 = VectorLayersEntry(table_name="vector-table",
                                                       name='layer name',
                                                       description='my description',
                                                       min_zoom=3,
                                                       max_zoom=12)

            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        expected_layer_entry_1)

            returned_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(returned_entries) == 1

            self.assert_vector_layer_entries_are_equal(expected_vector_layer_entry=expected_layer_entry_1,
                                                       actual_vector_layer_entry=returned_entries[0])

    def test_insert_vector_layers_extended(self, make_gpkg):
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            entry = VectorLayersEntry(table_name='tablesss',
                                      name='layer name',
                                      description='boo',
                                      min_zoom=1,
                                      max_zoom=2)
            GeoPackageVectorLayersTableAdapter.create_vector_layers_table(cursor=cursor)
            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=entry)

            returned_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(returned_entries) == 1
            self.assert_vector_layer_entries_are_equal(entry, returned_entries[0])

    def test_update_vector_layers_extended(self, make_gpkg):
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            entry = VectorLayersEntry(table_name='tablesss',
                                      name='layer name',
                                      description='boo',
                                      min_zoom=1,
                                      max_zoom=2)
            GeoPackageVectorLayersTableAdapter.create_vector_layers_table(cursor=cursor)
            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=entry)

            returned_entries = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=cursor)

            assert len(returned_entries) == 1
            self.assert_vector_layer_entries_are_equal(entry, returned_entries[0])

            updated_entry = VectorLayersEntry(table_name=entry.table_name, name=entry.name, description='descrp',
                                              min_zoom=2, max_zoom=9)
            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=updated_entry)

            returned_entries_after_update = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(
                cursor=cursor)

            assert len(returned_entries_after_update) == 1

            self.assert_vector_layer_entries_are_equal(updated_entry, returned_entries_after_update[0])

    def test_get_vector_layers_by_table_name(self, make_gpkg):
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            entry_1 = VectorLayersEntry(table_name='tablesss',
                                        name='layer name',
                                        description='boo',
                                        min_zoom=1,
                                        max_zoom=2)

            entry_2 = VectorLayersEntry(table_name=entry_1.table_name,
                                        name='layer name 2',
                                        description='boo 2',
                                        min_zoom=3,
                                        max_zoom=4)

            entry_3 = VectorLayersEntry(table_name='different table',
                                        name='layer name 2',
                                        description='boo 2',
                                        min_zoom=5,
                                        max_zoom=6)

            for entry in [entry_1, entry_2, entry_3]:
                GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                            vector_tile_layer_entry=entry)

            returned_entries = GeoPackageVectorLayersTableAdapter \
                .get_vector_layer_entries_by_table_name(cursor=cursor,
                                                        vector_tiles_table_name=entry_1.table_name)

            assert len(returned_entries) == 2
            actual_entry1_returned = next((returned_entry
                                           for returned_entry
                                           in returned_entries
                                           if returned_entry.name == entry_1.name),
                                          None)

            assert actual_entry1_returned is not None

            self.assert_vector_layer_entries_are_equal(entry_1, actual_entry1_returned)

            actual_entry2_returned = next((returned_entry
                                           for returned_entry
                                           in returned_entries
                                           if returned_entry.name == entry_2.name),
                                          None)

            assert actual_entry2_returned is not None

            self.assert_vector_layer_entries_are_equal(entry_2, actual_entry2_returned)

    def test_get_vector_layers_by_table_name_and_layer_name(self, make_gpkg):
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            entry_1 = VectorLayersEntry(table_name='tablesss',
                                        name='layer name',
                                        description='boo',
                                        min_zoom=1,
                                        max_zoom=2)

            entry_2 = VectorLayersEntry(table_name=entry_1.table_name,
                                        name='layer name 2',
                                        description='boo 2',
                                        min_zoom=3,
                                        max_zoom=4)

            entry_3 = VectorLayersEntry(table_name='different table',
                                        name='layer name 2',
                                        description='boo 2',
                                        min_zoom=5,
                                        max_zoom=6)

            for entry in [entry_1, entry_2, entry_3]:
                GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                            vector_tile_layer_entry=entry)

            returned_entry = GeoPackageVectorLayersTableAdapter \
                .get_vector_layer_entries_by_table_name_and_layer_name(cursor=cursor,
                                                                       vector_tiles_table_name=entry_1.table_name,
                                                                       layer_name=entry_1.name)
            self.assert_vector_layer_entries_are_equal(entry_1, returned_entry)

    def test_get_vector_layers_by_table_name_and_layer_name_no_table(self, make_gpkg):
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageVectorLayersTableAdapter \
                    .get_vector_layer_entries_by_table_name_and_layer_name(cursor=cursor,
                                                                           vector_tiles_table_name='tabbbe',
                                                                           layer_name='layyrer')

    def test_insert_bulk_no_table(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            GeoPackageVectorLayersTableAdapter.insert_vector_layers_entries_bulk(cursor=db_conn.cursor(),
                                                                                 vector_layers_entries=[
                                                                                     VectorLayersEntry(
                                                                                         table_name='table',
                                                                                         name='name',
                                                                                         description='',
                                                                                         min_zoom=None,
                                                                                         max_zoom=10)])

            vector_layers = GeoPackageVectorLayersTableAdapter.get_all_vector_layers_entries(cursor=db_conn.cursor())
            assert len(vector_layers) == 1

    @staticmethod
    def assert_vector_layer_entries_are_equal(expected_vector_layer_entry,
                                              actual_vector_layer_entry):
        """
        Asserts whether the actual_vector_layer_entry values are equivalent to the expected_vector_layer_entry values.
        If not, will raise an AssertionError
        :param expected_vector_layer_entry: the entry with the expected values
        :type expected_vector_layer_entry: VectorLayersEntry

        :param actual_vector_layer_entry:  the entry returned from a query to check its values
        :type actual_vector_layer_entry: VectorLayersEntry
        """
        assert expected_vector_layer_entry.name == actual_vector_layer_entry.name and \
               expected_vector_layer_entry.table_name == actual_vector_layer_entry.table_name and \
               expected_vector_layer_entry.min_zoom == actual_vector_layer_entry.min_zoom and \
               expected_vector_layer_entry.max_zoom == actual_vector_layer_entry.max_zoom and \
               expected_vector_layer_entry.description == actual_vector_layer_entry.description and \
               expected_vector_layer_entry.identifier == actual_vector_layer_entry.identifier
