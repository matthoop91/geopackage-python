#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
import filecmp
import os
from os.path import join, dirname
from tempfile import gettempdir

from pytest import raises

from rgi.tools.styled_layer_descriptor.styled_layer_descriptor_parser import ExternalGraphic
from rgi.tools.styled_layer_descriptor.styled_layer_descriptor_writer import StyledLayerDescriptorWriter
from testing import resources

SLD_OVERLAY_FILE_PATH = path = join(dirname(resources.__file__),
                                    'SLD_Overlay.xml')


class TestStyledLayerDescriptorWriter(object):

    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """
        self.temp_sld_file = None

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """
        if self.temp_sld_file is not None:
            os.remove(self.temp_sld_file)

    def test_update_external_graphic(self, copy_to_temp_file):
        self.temp_sld_file = copy_to_temp_file(path=SLD_OVERLAY_FILE_PATH)
        # copy file to temp dir
        style_writer = StyledLayerDescriptorWriter(sld_file_path=self.temp_sld_file)

        existing_external_graphics = style_writer.get_external_graphics()

        first_graphic = existing_external_graphics[0]
        expected_updated_graphic = ExternalGraphic(url_to_graphic='test test',
                                                   graphic_format='image/png')

        style_writer.update_external_graphic(current_graphic=first_graphic,
                                             updated_graphic=expected_updated_graphic)

        updated_external_graphics = style_writer.get_external_graphics()
        # check to make sure the graphic has been updated with its new value
        returned_external_graphic = next((graphic
                                          for graphic
                                          in updated_external_graphics
                                          if graphic.url_to_graphic == expected_updated_graphic.url_to_graphic and
                                          graphic.graphic_format == expected_updated_graphic.graphic_format),
                                         None)

        assert returned_external_graphic is not None
        # check to make sure the original graphic is no longer there
        original_graphic = next((graphic
                                 for graphic
                                 in updated_external_graphics
                                 if graphic.url_to_graphic == first_graphic.url_to_graphic and
                                 graphic.graphic_format == first_graphic.graphic_format),
                                None)

        assert original_graphic is None

    def test_no_graphic_to_update(self, copy_to_temp_file):
        self.temp_sld_file = copy_to_temp_file(SLD_OVERLAY_FILE_PATH)

        style_writer = StyledLayerDescriptorWriter(sld_file_path=self.temp_sld_file)
        with raises(ValueError):
            style_writer.update_external_graphic(current_graphic=ExternalGraphic(url_to_graphic='I dont exist',
                                                                                 graphic_format='image/svg'),
                                                 updated_graphic=ExternalGraphic(url_to_graphic='update url',
                                                                                 graphic_format='image/png'))

    def test_write_to_file(self, copy_to_temp_file):
        self.temp_sld_file = copy_to_temp_file(SLD_OVERLAY_FILE_PATH)

        style_writer = StyledLayerDescriptorWriter(sld_file_path=self.temp_sld_file)

        temp_path = os.path.join(gettempdir(), 'test_file.xml')
        style_writer.write_changes_to_new_file(new_file_path=temp_path)

        second_style_writer = StyledLayerDescriptorWriter(sld_file_path=temp_path)
        assert second_style_writer.get_sld_as_string() == style_writer.get_sld_as_string()

        existing_external_graphics = style_writer.get_external_graphics()

        first_graphic = existing_external_graphics[0]
        expected_updated_graphic = ExternalGraphic(url_to_graphic='test test',
                                                   graphic_format='image/png')
        style_writer.update_external_graphic(current_graphic=first_graphic,
                                             updated_graphic=expected_updated_graphic)

        style_writer.write_changes_to_file()

        assert second_style_writer.get_sld_as_string() != style_writer.get_sld_as_string()

        os.remove(temp_path)
