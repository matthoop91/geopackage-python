
from rgi.geopackage.extensions.ows.geopackage_context_offering_table_adapter import GeoPackageContextOfferingTableAdapter
from rgi.geopackage.extensions.ows.geopackage_context_offering_table_adapter import ContextOfferingEntry
from rgi.geopackage.utility.sql_utility import table_exists, get_database_connection, column_exists

CONTEXT_OFFERINGS_TABLE_NAME = "gpkgext_context_offerings"
TEST_CONTEXT_OFFERING_ID = 60
TEST_CONTEXT_OFFERING_RESOURCE_ID = 13


# noinspection PyClassHasNoInit
class TestGeoPackageContextOfferingTableAdapter:

    def test_create_context_table(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

        GeoPackageContextOfferingTableAdapter.create_context_offering_table(cursor)

        assert table_exists(cursor=cursor, table_name=CONTEXT_OFFERINGS_TABLE_NAME)
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "id")
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "resource_id")
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "code")
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "method")
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "stylesheet_id")
        assert column_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME, "contents")

    def test_get_context_offering(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextOfferingTableAdapter.create_context_offering_table(cursor)
            assert table_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME)
            context_offering_entry = self.create_context_offering_entry()
            GeoPackageContextOfferingTableAdapter.insert_or_update_context_offering_entry(cursor, context_offering_entry)

            this_context = GeoPackageContextOfferingTableAdapter.get_context_offering_entry(cursor, TEST_CONTEXT_OFFERING_ID)
            assert this_context.contents == 'Dog Food'
            assert this_context.stylesheet_id == 50
            assert this_context.mime_type == 'This_Mime_Type'
            assert this_context.method == 'test_method'
            assert this_context.code == 'Code_3'
            assert this_context.context_offering_resource_id == TEST_CONTEXT_OFFERING_RESOURCE_ID
            assert this_context.context_offering_id == TEST_CONTEXT_OFFERING_ID

    def test_get_all_context_offerings(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextOfferingTableAdapter.create_context_offering_table(cursor)
            assert table_exists(cursor, CONTEXT_OFFERINGS_TABLE_NAME)

            context_offering_count = 15
            inserted_context_offering_entries = []

            for x in range(context_offering_count):
                inserted_context_offering = self.create_context_offering_entry()
                inserted_context_offering.context_offering_id = x
                inserted_context_offering.code = "Code" + str(x)
                inserted_context_offering.contents = "Contents" + str(x)
                GeoPackageContextOfferingTableAdapter.insert_or_update_context_offering_entry(cursor, inserted_context_offering)
                inserted_context_offering_entries.append(inserted_context_offering)

            retrieved_context_offering_entries = GeoPackageContextOfferingTableAdapter.get_all_context_offering_entries(cursor)

            assert len(retrieved_context_offering_entries) == context_offering_count  # 15 records

            #We're sorting complex objects by that object's id.
            retrieved_context_offering_entries.sort(key=lambda j: j.context_offering_id, reverse=False)
            inserted_context_offering_entries.sort(key=lambda k: k.context_offering_id, reverse=False)

            for y in range(len(retrieved_context_offering_entries)):
                assert retrieved_context_offering_entries[y].context_offering_id == inserted_context_offering_entries[y].context_offering_id
                assert retrieved_context_offering_entries[y].contents == inserted_context_offering_entries[y].contents
                assert retrieved_context_offering_entries[y].code == inserted_context_offering_entries[y].code
                assert retrieved_context_offering_entries[y].context_offering_resource_id == inserted_context_offering_entries[y].context_offering_resource_id
                assert retrieved_context_offering_entries[y].stylesheet_id == inserted_context_offering_entries[y].stylesheet_id
                assert retrieved_context_offering_entries[y].mime_type == inserted_context_offering_entries[y].mime_type
                assert retrieved_context_offering_entries[y].method == inserted_context_offering_entries[y].method

    def test_insert_or_update_context_offering(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextOfferingTableAdapter.create_context_offering_table(cursor)

            # INSERT
            # Create ALL the new fields.
            new_code = "This_is_a_code"
            new_context_resource_id = 45
            new_mime_type = "Jenifer"
            new_stylesheet_id = 81
            new_contents = "Forget_Contents"
            new_context_offering_id = TEST_CONTEXT_OFFERING_ID
            new_method = "this_method"

            # Insert ALL those new fields.
            inserted_context_offering_entry = self.create_context_offering_entry()
            inserted_context_offering_entry.code = new_code
            inserted_context_offering_entry.context_offering_resource_id = new_context_resource_id
            inserted_context_offering_entry.mime_type = new_mime_type
            inserted_context_offering_entry.stylesheet_id = new_stylesheet_id
            inserted_context_offering_entry.contents = new_contents
            inserted_context_offering_entry.context_offering_id = new_context_offering_id
            inserted_context_offering_entry.method = new_method

            # Insert the new context offering.
            GeoPackageContextOfferingTableAdapter.insert_or_update_context_offering_entry(cursor, inserted_context_offering_entry)

            retrieved_context_offering_entry = GeoPackageContextOfferingTableAdapter.get_context_offering_entry(cursor=cursor,
                                                                                                                context_offering_id=TEST_CONTEXT_OFFERING_ID)
            # Check ALL the new fields.
            assert retrieved_context_offering_entry.context_offering_resource_id == inserted_context_offering_entry.context_offering_resource_id
            assert retrieved_context_offering_entry.code == inserted_context_offering_entry.code
            assert retrieved_context_offering_entry.mime_type == inserted_context_offering_entry.mime_type
            assert retrieved_context_offering_entry.stylesheet_id == inserted_context_offering_entry.stylesheet_id
            assert retrieved_context_offering_entry.context_offering_id == inserted_context_offering_entry.context_offering_id
            assert retrieved_context_offering_entry.contents == inserted_context_offering_entry.contents
            assert retrieved_context_offering_entry.method == inserted_context_offering_entry.method

            # UPDATE
            # Create ALL the new fields.
            updated_code = "Some_Other_Code"
            updated_context_resource_id = 39
            updated_mime_type = "Matt"
            updated_stylesheet_id = 56
            updated_contents = "What_Contents?"
            updated_context_offering_id = TEST_CONTEXT_OFFERING_ID
            updated_method = "some_other_method"

            # Insert ALL those updated fields.
            updated_context_offering_entry = self.create_context_offering_entry()
            updated_context_offering_entry.context_offering_id = TEST_CONTEXT_OFFERING_ID
            updated_context_offering_entry.code = updated_code
            updated_context_offering_entry.context_offering_resource_id = updated_context_resource_id
            updated_context_offering_entry.mime_type = updated_mime_type
            updated_context_offering_entry.stylesheet_id = updated_stylesheet_id
            updated_context_offering_entry.contents = updated_contents
            updated_context_offering_entry.method = updated_method

            # Insert the new context entry.
            GeoPackageContextOfferingTableAdapter.insert_or_update_context_offering_entry(cursor, updated_context_offering_entry)

            retrieved_updated_context_offering_entry = GeoPackageContextOfferingTableAdapter.get_context_offering_entry(cursor=cursor,
                                                                                                                        context_offering_id=updated_context_offering_id)

            # Check ALL the updated fields.
            assert retrieved_updated_context_offering_entry.context_offering_id == updated_context_offering_entry.context_offering_id
            assert retrieved_updated_context_offering_entry.code == updated_context_offering_entry.code
            assert retrieved_updated_context_offering_entry.context_offering_resource_id == updated_context_offering_entry.context_offering_resource_id
            assert retrieved_updated_context_offering_entry.mime_type == updated_context_offering_entry.mime_type
            assert retrieved_updated_context_offering_entry.stylesheet_id == updated_context_offering_entry.stylesheet_id
            assert retrieved_updated_context_offering_entry.contents == updated_context_offering_entry.contents
            assert retrieved_updated_context_offering_entry.method == updated_context_offering_entry.method

    def create_context_offering_entry(self):

        return ContextOfferingEntry(context_offering_id=TEST_CONTEXT_OFFERING_ID,
                                    context_offering_resource_id=TEST_CONTEXT_OFFERING_RESOURCE_ID,
                                    code='Code_3',
                                    method='test_method',
                                    mime_type='This_Mime_Type',
                                    stylesheet_id=50,
                                    contents='Dog Food')
