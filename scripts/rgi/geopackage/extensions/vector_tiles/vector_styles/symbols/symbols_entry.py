#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""

class SymbolsEntry(object):
    """
    The object that represents an entry to the GeoPackage Symbols Table (gpkgext_symbols)
    """

    def __init__(self,
                 symbol_id,
                 content_type,
                 symbol,
                 title,
                 description,
                 id=None):
        """
        Constructor

        :param symbol_id: a style set id that groups a set of stylesheets
        :type symbol_id: str

        :param content_type: alternative styles for the same set of layers (day, night) associated with an int value.
        :type content_type: str

        :param symbol: stylesheet's encoding standard/format
        :type symbol: str
        
        :param title: the optional description of the symbol
        :type title: str, None

        :param description: optional description of the symbol
        :type description: str, None

        :param id: primary key int identifier for the column id.  If None, a value will be assigned to it when
        added to the GeoPackage
        :type id: int, None
        """

        self.symbol_id = symbol_id
        self.content_type = content_type
        self.symbol = symbol
        self.title = title
        self.description = description
        self.id = id
