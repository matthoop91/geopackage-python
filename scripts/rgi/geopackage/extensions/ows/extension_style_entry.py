#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""


class ExtensionStyleEntry(object):
    """
    Represents a row in the gpkgext_styles table.
    """

    def __init__(self,
                 extension_style_id,
                 style,
                 content_type
                 ):
        """
        Constructor

        :param extension_style_id: The unique identifier for a GeoPackage extension style.
        :type extension_style_id: int

        :param style: The actual style
        :type style: blob

        :param content_type: MIME type of style
        :type content_type: str

        """

        self.extension_style_id = extension_style_id
        self.style = style
        self.content_type = content_type



