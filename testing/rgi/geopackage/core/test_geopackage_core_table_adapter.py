#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
import threading

from pytest import raises

from rgi.geopackage.core.content_entry import ContentEntry
from rgi.geopackage.core.geopackage_core_table_adapter import GeoPackageCoreTableAdapter, \
    GEOPACKAGE_CONTENTS_TABLE_NAME, GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME
from rgi.geopackage.srs.mercator import Mercator
from rgi.geopackage.srs.scaled_world_mercator import ScaledWorldMercator
from rgi.geopackage.tiles.tiles_content_entry import TilesContentEntry
from rgi.geopackage.utility.sql_utility import get_database_connection, table_exists
from testing.rgi.test_utility import create_table, ExThread


class TestGeoPackageCoreTableAdapter(object):

    def test_get_srs_by_id(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            # check to make sure it doesn't give anything back the first time
            srs_expected = ScaledWorldMercator()
            none_expected = GeoPackageCoreTableAdapter.get_spatial_reference_system_by_srs_id(cursor=cursor,
                                                                                              srs_id=srs_expected.srs_id)

            assert none_expected is None

            # now add it to the table and check that the expected srs is returned
            GeoPackageCoreTableAdapter.insert_spatial_reference_system_row(cursor=cursor,
                                                                           spatial_reference_system=srs_expected)

            srs_returned = GeoPackageCoreTableAdapter.get_spatial_reference_system_by_srs_id(cursor=cursor,
                                                                                             srs_id=srs_expected.srs_id)

            assert srs_returned is not None and \
                   srs_returned.srs_id == srs_expected.srs_id and \
                   srs_returned.description == srs_expected.description and \
                   srs_returned.definition == srs_expected.definition and \
                   srs_returned.organization == srs_expected.organization and \
                   srs_returned.srs_name == srs_expected.srs_name

    def test_get_srs_by_id_without_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            with raises(ValueError):
                GeoPackageCoreTableAdapter.get_spatial_reference_system_by_srs_id(cursor=cursor,
                                                                                  srs_id=4326)

    def test_insert_gpkg_contents_with_table_name_not_existing_in_gpkg(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                    content=TilesContentEntry(table_name='I Dont exist',
                                                                                              identifier='hello',
                                                                                              min_x=0.0,
                                                                                              min_y=0.0,
                                                                                              max_x=1.0,
                                                                                              max_y=2.0,
                                                                                              srs_id=4326))

    def test_insert_gpkg_contents_with_invalid_srs_id(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            tiles_entry = TilesContentEntry(table_name='tiles',
                                            srs_id=1233,
                                            min_y=0,
                                            min_x=0,
                                            max_x=1,
                                            max_y=1,
                                            identifier="")
            create_table(cursor=cursor,
                         table_name=tiles_entry.table_name)
            with raises(ValueError):
                GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                    content=tiles_entry)

    def test_insert_srs_without_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            srs_expected = Mercator()
            GeoPackageCoreTableAdapter.insert_spatial_reference_system_row(cursor=cursor,
                                                                           spatial_reference_system=srs_expected)
            srs_entries = GeoPackageCoreTableAdapter.get_all_spatial_reference_system_entries(cursor=cursor)

            mercator_entry = [srs_entry for srs_entry in srs_entries if srs_entry.srs_id == srs_expected.srs_id][0]

            assert mercator_entry is not None and \
                   mercator_entry.srs_id == srs_expected.srs_id and \
                   mercator_entry.description == srs_expected.description and \
                   mercator_entry.definition == srs_expected.definition and \
                   mercator_entry.organization == srs_expected.organization and \
                   mercator_entry.srs_name == srs_expected.srs_name

    def test_insert_contents_without_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            tiles_entry = TilesContentEntry(table_name="table",
                                            identifier="id",
                                            min_y=0,
                                            min_x=0,
                                            max_x=0,
                                            max_y=0,
                                            srs_id=0)
            create_table(cursor=cursor,
                         table_name=tiles_entry.table_name)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=tiles_entry)

    def test_get_all_content_entries(self, make_gpkg):
        gpkg = make_gpkg
        gpkg.initialize()

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            content1 = ContentEntry(table_name="tiles",
                                    data_type="tiles",
                                    identifier="idennntititi",
                                    min_x=0,
                                    min_y=1.0,
                                    max_x=2.2,
                                    max_y=3.3,
                                    srs_id=4326)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=content1)
            content2 = ContentEntry(table_name="1231table",
                                    data_type="tiles",
                                    identifier="ddeentntity",
                                    min_x=4.7,
                                    min_y=3.2,
                                    max_x=2.1,
                                    max_y=1.1,
                                    srs_id=3857)

            create_table(cursor=cursor,
                         table_name=content2.table_name)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=content2)

            actual_content1 = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=cursor,
                                                                                         table_name=content1.table_name)

            TestGeoPackageCoreTableAdapter.assert_content_entries_equal(expected_content_entry=content1,
                                                                        actual_content_entry=actual_content1)

            actual_content2 = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=cursor,
                                                                                         table_name=content2.table_name)

            TestGeoPackageCoreTableAdapter.assert_content_entries_equal(expected_content_entry=content2,
                                                                        actual_content_entry=actual_content2)

            all_entries = GeoPackageCoreTableAdapter.get_all_content_entries(cursor=cursor)
            assert len(all_entries) == 2

    def test_get_all_content_entries_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            with raises(ValueError):
                cursor = db_conn.cursor()
                GeoPackageCoreTableAdapter.get_all_content_entries(cursor=cursor)

    def test_retrieve_content_entry_after_initialize(self, make_gpkg):
        make_gpkg.initialize()
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            assert len(GeoPackageCoreTableAdapter.get_all_content_entries(cursor=cursor)) == 1

    def test_get_content_by_table_name_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=cursor,
                                                                           table_name="tiles")

    def test_get_content_non_existent_entry(self, make_gpkg):
        make_gpkg.initialize()
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            assert GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=cursor,
                                                                              table_name="Dont_Exist") is None

    def test_all_spatial_reference_system_entries_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            with raises(ValueError):
                cursor = db_conn.cursor()
                GeoPackageCoreTableAdapter.get_all_spatial_reference_system_entries(cursor=cursor)

    def test_insert_row_with_no_core_tables(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db_conn:
            table_name = 'tables'
            create_table(cursor=db_conn.cursor(),
                         table_name=table_name)
            db_conn.commit()

            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=db_conn.cursor(),
                                                                content=ContentEntry(table_name=table_name,
                                                                                     data_type='type',
                                                                                     identifier='id',
                                                                                     min_x=1.0,
                                                                                     min_y=1.0,
                                                                                     max_x=10,
                                                                                     max_y=11,
                                                                                     srs_id=4326))
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_CONTENTS_TABLE_NAME)
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME)

    def test_insert_row_with_no_srs_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db_conn:
            table_name = 'tables'
            create_table(cursor=db_conn.cursor(),
                         table_name=table_name)

            db_conn.commit()

            GeoPackageCoreTableAdapter.create_geopackage_contents_table(cursor=db_conn.cursor())

            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=db_conn.cursor(),
                                                                content=ContentEntry(table_name=table_name,
                                                                                     data_type='type',
                                                                                     identifier='id',
                                                                                     min_x=1.0,
                                                                                     min_y=1.0,
                                                                                     max_x=10,
                                                                                     max_y=11,
                                                                                     srs_id=4326))
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_CONTENTS_TABLE_NAME)
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME)

    def test_insert_row_with_no_contents_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db_conn:
            table_name = 'tables'
            create_table(cursor=db_conn.cursor(),
                         table_name=table_name)

            db_conn.commit()

            GeoPackageCoreTableAdapter.create_spatial_reference_table(cursor=db_conn.cursor())

            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=db_conn.cursor(),
                                                                content=ContentEntry(table_name=table_name,
                                                                                     data_type='type',
                                                                                     identifier='id',
                                                                                     min_x=1.0,
                                                                                     min_y=1.0,
                                                                                     max_x=10,
                                                                                     max_y=11,
                                                                                     srs_id=4326))
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_CONTENTS_TABLE_NAME)
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME)

    def test_insert_row_with_invalid_contents_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db_conn:
            table_name = 'tables'
            create_table(cursor=db_conn.cursor(),
                         table_name=table_name)

            create_table(cursor=db_conn.cursor(),
                         table_name=GEOPACKAGE_CONTENTS_TABLE_NAME)
            db_conn.commit()
            with raises(ValueError):
                GeoPackageCoreTableAdapter.insert_or_update_content(cursor=db_conn.cursor(),
                                                                    content=ContentEntry(table_name=table_name,
                                                                                         data_type='type',
                                                                                         identifier='id',
                                                                                         min_x=1.0,
                                                                                         min_y=1.0,
                                                                                         max_x=10,
                                                                                         max_y=11,
                                                                                         srs_id=4326))

    def test_insert_row_with_invalid_srs_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db_conn:
            table_name = 'tables'
            create_table(cursor=db_conn.cursor(),
                         table_name=table_name)

            create_table(cursor=db_conn.cursor(),
                         table_name=GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME)

            db_conn.commit()
            with raises(ValueError):
                GeoPackageCoreTableAdapter.insert_or_update_content(cursor=db_conn.cursor(),
                                                                    content=ContentEntry(table_name=table_name,
                                                                                         data_type='type',
                                                                                         identifier='id',
                                                                                         min_x=1.0,
                                                                                         min_y=1.0,
                                                                                         max_x=10,
                                                                                         max_y=11,
                                                                                         srs_id=4326))

    def test_insert_content_entry_with_invalid_identifier(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            content_entry_1 = ContentEntry(table_name='my table',
                                           data_type='somethin',
                                           identifier='my identifier',
                                           min_x=0.0,
                                           min_y=0.0,
                                           max_x=1.0,
                                           max_y=2.0,
                                           srs_id=4326)
            create_table(cursor=cursor,
                         table_name=content_entry_1.table_name)
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)

            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=content_entry_1)

            invalid_content_entry_2 = ContentEntry(table_name='different table name',
                                                   data_type='data',
                                                   identifier=content_entry_1.identifier,
                                                   min_x=1,
                                                   min_y=2,
                                                   max_x=3,
                                                   max_y=4,
                                                   srs_id=0)

            create_table(cursor=cursor,
                         table_name=invalid_content_entry_2.table_name)

            with raises(ValueError):
                GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                    content=invalid_content_entry_2)

    def test_update_content_entry(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            content_entry_1 = ContentEntry(table_name='my table',
                                           data_type='somethin',
                                           identifier='my identifier',
                                           min_x=0.0,
                                           min_y=1.0,
                                           max_x=2.0,
                                           max_y=3.0,
                                           srs_id=4326)
            create_table(cursor=cursor,
                         table_name=content_entry_1.table_name)
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)

            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=content_entry_1)

            returned_entry = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=cursor,
                                                                                        table_name=content_entry_1.table_name)

            assert returned_entry is not None
            assert returned_entry.table_name == content_entry_1.table_name and \
                   returned_entry.identifier == content_entry_1.identifier and \
                   returned_entry.srs_id == content_entry_1.srs_id and \
                   returned_entry.min_x == content_entry_1.min_x and \
                   returned_entry.min_y == content_entry_1.min_y and \
                   returned_entry.max_x == content_entry_1.max_x and \
                   returned_entry.max_y == content_entry_1.max_y and \
                   returned_entry.data_type == content_entry_1.data_type

            content_entry_2 = ContentEntry(table_name=content_entry_1.table_name,
                                           data_type='data',
                                           identifier=content_entry_1.identifier,
                                           min_x=4.0,
                                           min_y=5.0,
                                           max_x=6.0,
                                           max_y=7.0,
                                           srs_id=0)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=content_entry_2)

            all_entries = GeoPackageCoreTableAdapter.get_all_content_entries(cursor=cursor)

            assert len(all_entries) == 1

            updated_entry = all_entries[0]
            assert updated_entry.table_name == content_entry_2.table_name and \
                   updated_entry.identifier == content_entry_2.identifier and \
                   updated_entry.srs_id == content_entry_2.srs_id and \
                   updated_entry.min_x == content_entry_2.min_x and \
                   updated_entry.min_y == content_entry_2.min_y and \
                   updated_entry.max_x == content_entry_2.max_x and \
                   updated_entry.max_y == content_entry_2.max_y and \
                   updated_entry.data_type == content_entry_2.data_type

    @staticmethod
    def assert_content_entries_equal(expected_content_entry,
                                     actual_content_entry):
        """

        :param expected_content_entry:
        :type expected_content_entry: Content
        :param actual_content_entry:
        :type actual_content_entry: Content
        """
        assert expected_content_entry.table_name == actual_content_entry.table_name and \
               expected_content_entry.srs_id == actual_content_entry.srs_id and \
               expected_content_entry.min_x == actual_content_entry.min_x and \
               expected_content_entry.min_y == actual_content_entry.min_y and \
               expected_content_entry.max_y == actual_content_entry.max_y and \
               expected_content_entry.max_x == actual_content_entry.max_x and \
               expected_content_entry.data_type == actual_content_entry.data_type
