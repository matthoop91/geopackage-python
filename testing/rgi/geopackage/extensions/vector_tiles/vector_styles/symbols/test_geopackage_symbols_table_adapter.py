#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2019-1-30
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
import sqlite3

from rgi.geopackage.extensions.vector_tiles.vector_styles.symbols.geopackage_symbols_table_adapter import \
    GeoPackageSymbolsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_styles.symbols.symbols_entry import SymbolsEntry
from rgi.geopackage.utility.sql_utility import get_database_connection
from testing.rgi.test_utility import get_raster_data


class TestGeoPackageSymbolsTableAdapter(object):

    def test_insert_and_retrieval_entry(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            added_entry = SymbolsEntry(symbol_id='my symbol identifier',
                                       symbol=sqlite3.Binary(get_raster_data(image_type='png')),
                                       content_type='image/png',
                                       title="my symbol",
                                       description='description')

            GeoPackageSymbolsTableAdapter.insert_or_update_symbols_entry(cursor=db_conn.cursor(),
                                                                         symbols_entry=added_entry)

            returned_entries = GeoPackageSymbolsTableAdapter.get_all_symbol_entries(cursor=db_conn.cursor())
            assert len(returned_entries) == 1
            returned_entry = returned_entries[0]
            self.assert_entries_equal(added_entry, returned_entry)

    def test_insert_and_update_entry(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            # insert test
            added_entry = SymbolsEntry(symbol_id='my symbol identifier',
                                       symbol=sqlite3.Binary(get_raster_data(image_type='png')),
                                       content_type='image/png',
                                       title="my symbol",
                                       description='description')

            GeoPackageSymbolsTableAdapter.insert_or_update_symbols_entry(cursor=db_conn.cursor(),
                                                                         symbols_entry=added_entry)

            returned_entries = GeoPackageSymbolsTableAdapter.get_all_symbol_entries(cursor=db_conn.cursor())
            assert len(returned_entries) == 1
            returned_entry = returned_entries[0]
            self.assert_entries_equal(added_entry, returned_entry)

            # now test for proper update
            updated_entry = SymbolsEntry(symbol_id='a different identifier',
                                         content_type='image/jpeg',
                                         symbol=sqlite3.Binary(get_raster_data(image_type='jpeg')),
                                         title=added_entry.title,
                                         description='description',
                                         id=added_entry.id)

            GeoPackageSymbolsTableAdapter.insert_or_update_symbols_entry(cursor=db_conn.cursor(),
                                                                         symbols_entry=updated_entry)

            returned_entries_after_update = GeoPackageSymbolsTableAdapter\
                .get_all_symbol_entries(cursor=db_conn.cursor())
            assert len(returned_entries_after_update) == 1
            returned_updated_entry = returned_entries_after_update[0]
            self.assert_entries_equal(updated_entry, returned_updated_entry)

    def test_insert_and_update_entry_by_unique_values(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            # insert test
            added_entry = SymbolsEntry(symbol_id='my symbol identifier',
                                       symbol=sqlite3.Binary(get_raster_data(image_type='png')),
                                       content_type='image/png',
                                       title="my symbol",
                                       description='description')

            GeoPackageSymbolsTableAdapter.insert_or_update_symbols_entry(cursor=db_conn.cursor(),
                                                                         symbols_entry=added_entry)

            returned_entries = GeoPackageSymbolsTableAdapter.get_all_symbol_entries(cursor=db_conn.cursor())
            assert len(returned_entries) == 1
            returned_entry = returned_entries[0]
            self.assert_entries_equal(added_entry, returned_entry)

            # now test for proper update
            updated_entry = SymbolsEntry(symbol_id=added_entry.symbol_id,
                                         content_type=added_entry.content_type,
                                         symbol=sqlite3.Binary(get_raster_data(image_type='jpeg')),
                                         title='a different title',
                                         description='a different description')

            GeoPackageSymbolsTableAdapter.insert_or_update_symbols_entry(cursor=db_conn.cursor(),
                                                                         symbols_entry=updated_entry)

            returned_entries_after_update = GeoPackageSymbolsTableAdapter\
                .get_all_symbol_entries(cursor=db_conn.cursor())
            assert len(returned_entries_after_update) == 1
            returned_updated_entry = returned_entries_after_update[0]
            self.assert_entries_equal(updated_entry, returned_updated_entry)

    def test_get_all_symbols_entries_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            assert len(GeoPackageSymbolsTableAdapter.get_all_symbol_entries(cursor=cursor)) == 0

    @staticmethod
    def assert_entries_equal(expected_entry, actual_entry):
        """
        Asserts that all the properties of the two SymbolsEntry objects are equivalent.  If not, will throw an
        AssertionError

        :param expected_entry:
        :type expected_entry: SymbolsEntry

        :param actual_entry:
        :type actual_entry: SymbolsEntry
        """

        assert expected_entry.id == actual_entry.id and \
               expected_entry.symbol_id == actual_entry.symbol_id and \
               expected_entry.symbol == actual_entry.symbol and \
               expected_entry.content_type == actual_entry.content_type and \
               expected_entry.title == actual_entry.title and \
               expected_entry.description == actual_entry.description
