#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2019-1-30
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from collections import namedtuple

from pytest import raises

from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import \
    GeoPackageVectorLayersTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_layers.vector_layers_entry import VectorLayersEntry
from rgi.geopackage.extensions.vector_tiles.vector_styles.geopackage_stylesheets_table_adapter import \
    GeoPackageStylesheetsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_styles.styles_extension_constants import \
    GEOPACKAGE_STYLESHEETS_TABLE_NAME
from rgi.geopackage.extensions.vector_tiles.vector_styles.stylesheets_entry import StyleSheetsEntry, StyleFormat
from rgi.geopackage.utility.sql_utility import get_database_connection
from testing.rgi.test_utility import get_sld_style_data_path, get_file_as_binary, get_mapbox_style_data_path, \
    get_cmss_style_data_path, create_table


class TestGeoPackageStyleSheetsTableAdapter(object):

    def test_get_all_entries_invalid_stylesheets_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_STYLESHEETS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=db.cursor())

    def test_get_all_stylesheet_entries_invalid_stylesheets_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_STYLESHEETS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=db.cursor())


    def test_insert_or_update_invalid_stylesheets_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_STYLESHEETS_TABLE_NAME)
            stylesheet = StyleSheetsEntry(styles_set='1',
                                          style='0',
                                          style_format=StyleFormat.SLD_STYLE.value,
                                          stylesheet=self.get_style_data(StyleFormat.SLD_STYLE),
                                          title='my layer',
                                          description='description')
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=db.cursor(),
                                                                                    stylesheet_entry=stylesheet)

    def test_insert_and_retrieval_entry(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            added_entry = StyleSheetsEntry(styles_set='1',
                                           style='0',
                                           style_format=StyleFormat.SLD_STYLE.value,
                                           stylesheet=self.get_style_data(StyleFormat.SLD_STYLE),
                                           title='my layer',
                                           description='description')

            GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=db_conn.cursor(),
                                                                                stylesheet_entry=added_entry)

            returned_entries = GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=db_conn.cursor())
            assert len(returned_entries) == 1
            returned_entry = returned_entries[0]
            self.assert_entries_equal(added_entry, returned_entry)

    def test_insert_and_update_entry(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            # insert test
            added_entry = StyleSheetsEntry(styles_set='3',
                                           style='2',
                                           style_format=StyleFormat.MAPBOX_STYLE.value,
                                           stylesheet=self.get_style_data(StyleFormat.MAPBOX_STYLE),
                                           title='my layer',
                                           description='description')

            GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=db_conn.cursor(),
                                                                                stylesheet_entry=added_entry)

            returned_entries = GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=db_conn.cursor())
            assert len(returned_entries) == 1
            returned_entry = returned_entries[0]
            self.assert_entries_equal(added_entry, returned_entry)

            # now test for proper update
            updated_entry = StyleSheetsEntry(styles_set='1',
                                             style='1',
                                             style_format=StyleFormat.SLD_STYLE.value,
                                             stylesheet=self.get_style_data(StyleFormat.SLD_STYLE),
                                             title=added_entry.title,
                                             description='description',
                                             identifier=added_entry.identifier)

            GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=db_conn.cursor(),
                                                                                stylesheet_entry=updated_entry)

            returned_entries_after_update = GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(
                cursor=db_conn.cursor())
            assert len(returned_entries_after_update) == 1
            returned_updated_entry = returned_entries_after_update[0]
            self.assert_entries_equal(updated_entry, returned_updated_entry)

    def test_get_all_stylesheet_entries_by_layer(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            # insert 3 vector layer entries
            first_layer = VectorLayersEntry(table_name='table',
                                            name='my first layer',
                                            description='',
                                            min_zoom=0,
                                            max_zoom=10)

            second_layer = VectorLayersEntry(table_name='table',
                                             name='my second layer',
                                             description='',
                                             min_zoom=1,
                                             max_zoom=11)

            third_layer = VectorLayersEntry(table_name='table 2',
                                            name='my third layer',
                                            description='',
                                            min_zoom=2,
                                            max_zoom=12)

            GeoPackageVectorLayersTableAdapter.create_vector_layers_table(cursor=cursor)
            for layer_entry in [first_layer, second_layer, third_layer]:
                GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                            vector_tile_layer_entry=layer_entry)

            ExpectedStyle = namedtuple('ExpectedStyle',
                                       'layer_set style format stylesheet layer')

            expected_stylesheet_1 = ExpectedStyle('first',
                                                  'day',
                                                  StyleFormat.SLD_STYLE.value,
                                                  self.get_style_data(StyleFormat.SLD_STYLE),
                                                  first_layer.identifier)

            expected_stylesheet_2 = ExpectedStyle('first',
                                                  'night',
                                                  StyleFormat.MAPBOX_STYLE.value,
                                                  self.get_style_data(StyleFormat.MAPBOX_STYLE),
                                                  second_layer.identifier)

            expected_stylesheet_3 = ExpectedStyle('first',
                                                  'default',
                                                  StyleFormat.MAPBOX_STYLE.value,
                                                  self.get_style_data(StyleFormat.MAPBOX_STYLE),
                                                  third_layer.identifier)

            expected_stylesheet_4 = ExpectedStyle('second',
                                                  'other',
                                                  StyleFormat.G_CMSS_STYLE.value,
                                                  self.get_style_data(StyleFormat.G_CMSS_STYLE),
                                                  third_layer.identifier)

            expected_stylesheet_5 = ExpectedStyle('second',
                                                  'day',
                                                  StyleFormat.SLD_STYLE.value,
                                                  self.get_style_data(StyleFormat.SLD_STYLE),
                                                  None)

            for style_sheet in [expected_stylesheet_1,
                                expected_stylesheet_2,
                                expected_stylesheet_3,
                                expected_stylesheet_4,
                                expected_stylesheet_5]:
                GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=cursor,
                                                                                    stylesheet_entry=StyleSheetsEntry(
                                                                                        styles_set=style_sheet.layer_set,
                                                                                        style=style_sheet.style,
                                                                                        style_format=style_sheet.format,
                                                                                        stylesheet=style_sheet.stylesheet,
                                                                                        title=str(style_sheet.layer),
                                                                                        description='description'))
            db_conn.commit()
            layer_1_style_sheets = GeoPackageStylesheetsTableAdapter.get_all_style_sheets_by_styles_set(cursor=cursor,
                                                                                                        styles_set='first')

            assert len(layer_1_style_sheets) == 3

            layer_1_style_sheets_1 = next(style_sheet
                                          for style_sheet
                                          in layer_1_style_sheets
                                          if style_sheet.style == expected_stylesheet_1.style)

            self.assert_entries_equal(layer_1_style_sheets_1,
                                      StyleSheetsEntry(styles_set=expected_stylesheet_1.layer_set,
                                                       style=expected_stylesheet_1.style,
                                                       style_format=expected_stylesheet_1.format,
                                                       stylesheet=expected_stylesheet_1.stylesheet,
                                                       title=expected_stylesheet_1.layer, description='description',
                                                       identifier=layer_1_style_sheets_1.identifier))

            layer_1_style_sheets_2 = next(style_sheet
                                          for style_sheet
                                          in layer_1_style_sheets
                                          if style_sheet.style == expected_stylesheet_2.style)

            self.assert_entries_equal(layer_1_style_sheets_2,
                                      StyleSheetsEntry(styles_set=expected_stylesheet_2.layer_set,
                                                       style=expected_stylesheet_2.style,
                                                       style_format=expected_stylesheet_2.format,
                                                       stylesheet=expected_stylesheet_2.stylesheet,
                                                       title=expected_stylesheet_2.layer, description='description',
                                                       identifier=layer_1_style_sheets_2.identifier))
            layer_1_style_sheets_3 = next(style_sheet
                                          for style_sheet
                                          in layer_1_style_sheets
                                          if style_sheet.style == expected_stylesheet_3.style)

            self.assert_entries_equal(layer_1_style_sheets_3,
                                      StyleSheetsEntry(styles_set=expected_stylesheet_3.layer_set,
                                                       style=expected_stylesheet_3.style,
                                                       style_format=expected_stylesheet_3.format,
                                                       stylesheet=expected_stylesheet_3.stylesheet,
                                                       title=expected_stylesheet_3.layer, description='description',
                                                       identifier=layer_1_style_sheets_3.identifier))

            layer_2_style_sheets = GeoPackageStylesheetsTableAdapter.get_all_style_sheets_by_styles_set(cursor=cursor,
                                                                                                        styles_set='first')

            assert len(layer_2_style_sheets) == 3

            layer_3_style_sheets = GeoPackageStylesheetsTableAdapter.get_all_style_sheets_by_styles_set(cursor=cursor,
                                                                                                        styles_set='second')

            assert len(layer_3_style_sheets) == 2

            layer_1_style_sheets_4 = next(style_sheet
                                          for style_sheet
                                          in layer_3_style_sheets
                                          if style_sheet.style == expected_stylesheet_4.style)

            self.assert_entries_equal(layer_1_style_sheets_4,
                                      StyleSheetsEntry(styles_set=expected_stylesheet_4.layer_set,
                                                       style=expected_stylesheet_4.style,
                                                       style_format=expected_stylesheet_4.format,
                                                       stylesheet=expected_stylesheet_4.stylesheet,
                                                       title=expected_stylesheet_4.layer, description='description',
                                                       identifier=layer_1_style_sheets_4.identifier))
            layer_1_style_sheets_5 = next(style_sheet
                                          for style_sheet
                                          in layer_3_style_sheets
                                          if style_sheet.style == expected_stylesheet_5.style)

            self.assert_entries_equal(layer_1_style_sheets_5,
                                      StyleSheetsEntry(styles_set=expected_stylesheet_5.layer_set,
                                                       style=expected_stylesheet_5.style,
                                                       style_format=expected_stylesheet_5.format,
                                                       stylesheet=expected_stylesheet_5.stylesheet,
                                                       title=expected_stylesheet_5.layer, description='description',
                                                       identifier=layer_1_style_sheets_5.identifier))

    def test_get_all_stylesheet_entries_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=cursor)

    def test_get_all_stylesheets_for_tileset_no_styles_table(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=cursor)

    def test_get_all_stylesheets(self, get_vector_tiles_gpkg):
        with get_database_connection(get_vector_tiles_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            expected_style_sheet_1 = StyleSheetsEntry(styles_set='hello',
                                                      style='day',
                                                      style_format=StyleFormat.MAPBOX_STYLE.value,
                                                      stylesheet=self.get_style_data(StyleFormat.MAPBOX_STYLE),
                                                      title='my title',
                                                      description='my description')

            expected_style_sheet_2 = StyleSheetsEntry(styles_set='other set',
                                                      style='night',
                                                      style_format=StyleFormat.SLD_STYLE.value,
                                                      title='woot',
                                                      description='test',
                                                      stylesheet=self.get_style_data(StyleFormat.SLD_STYLE))

            GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=cursor,
                                                                                stylesheet_entry=expected_style_sheet_1)
            GeoPackageStylesheetsTableAdapter.insert_or_update_stylesheet_entry(cursor=cursor,
                                                                                stylesheet_entry=expected_style_sheet_2)

            expected_vector_tile_entry = VectorLayersEntry(table_name=get_vector_tiles_gpkg.tile_table_name,
                                                           name='my_layer',
                                                           description='other description',
                                                           min_zoom=0,
                                                           max_zoom=10)

            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        expected_vector_tile_entry)

            other_vector_tile_entry = VectorLayersEntry(table_name=get_vector_tiles_gpkg.tile_table_name,
                                                        name='other layer',
                                                        description='desc',
                                                        min_zoom=1,
                                                        max_zoom=11)

            GeoPackageVectorLayersTableAdapter.insert_or_update_vector_tile_layer_entry(cursor=cursor,
                                                                                        vector_tile_layer_entry=
                                                                                        other_vector_tile_entry)

            db_conn.commit()

            stylesheets = GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=cursor)
            assert len(stylesheets) == 2
            returned_stylesheet_one = next(returned_sheet
                                           for returned_sheet
                                           in stylesheets
                                           if returned_sheet.title == expected_style_sheet_1.title)
            self.assert_entries_equal(returned_stylesheet_one, expected_style_sheet_1)

            returned_stylesheet_two = next(returned_sheet
                                           for returned_sheet
                                           in stylesheets
                                           if returned_sheet.title == expected_style_sheet_2.title)

            self.assert_entries_equal(returned_stylesheet_two, expected_style_sheet_2)

    def test_get_all_stylesheet_entries_no_styles_table(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_stylesheet_entries(cursor=cursor)

    def test_get_all_style_sheets_by_styles_set_no_styles_table(self, make_gpkg):

        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageStylesheetsTableAdapter.get_all_style_sheets_by_styles_set(cursor=cursor,
                                                                                     styles_set='styles')

    @staticmethod
    def get_style_data(style_format):
        """

        :param style_format:
        :type style_format: StyleFormat
        :return:
        """

        if style_format == StyleFormat.SLD_STYLE:
            return get_file_as_binary(get_sld_style_data_path())

        if style_format == StyleFormat.MAPBOX_STYLE:
            return get_file_as_binary(get_mapbox_style_data_path())

        if style_format == StyleFormat.G_CMSS_STYLE:
            return get_file_as_binary(get_cmss_style_data_path())

        raise ValueError("No test style for format: {format}".format(format=style_format.value))

    @staticmethod
    def assert_entries_equal(expected_entry, actual_entry):
        """
        Asserts that all the properties of the two StyleSheetEntry objects are equivalent.  If not, will throw an
        AssertionError

        :param expected_entry:
        :type expected_entry: StyleSheetsEntry

        :param actual_entry:
        :type actual_entry: StyleSheetsEntry
        """

        assert expected_entry.style == actual_entry.style and \
               expected_entry.style_format == actual_entry.style_format and \
               expected_entry.styles_set == actual_entry.styles_set and \
               expected_entry.identifier == actual_entry.identifier and \
               expected_entry.stylesheet == actual_entry.stylesheet
