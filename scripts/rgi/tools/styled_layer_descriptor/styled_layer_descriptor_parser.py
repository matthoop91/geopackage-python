#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""

import lxml.etree as DET
from xml.etree.ElementTree import Element

STYLED_LAYER_DESCRIPTOR_NAMESPACE = "http://www.opengis.net/sld"
XLINK_NAMESPACE = "http://www.w3.org/1999/xlink"
GRAPHIC_FORMAT_TAG = "{{{sld_name_space}}}Format".format(sld_name_space=STYLED_LAYER_DESCRIPTOR_NAMESPACE)
EXTERNAL_GRAPHIC_ELEMENT = "{{{sld_name_space}}}ExternalGraphic" \
    .format(sld_name_space=STYLED_LAYER_DESCRIPTOR_NAMESPACE)
ONLINE_RESOURCE_TAG = "{{{sld_name_space}}}OnlineResource".format(sld_name_space=STYLED_LAYER_DESCRIPTOR_NAMESPACE)
ONLINE_RESOURCE_URL_ATTRIBUTE_TAG = "{{{xlink}}}href".format(xlink=XLINK_NAMESPACE)


class ExternalGraphic(object):
    """
    The object representation of the ExternalGraphic element in an SLD xml.
    """

    def __init__(self,
                 url_to_graphic,
                 graphic_format):
        """
        Constructor.

        :param url_to_graphic: the url to the graphic resource as a string
        (i.e. "http://vtp2018.s3-eu-west-1.amazonaws.com/static/mapstorestyle/sprites/mosque_w.svg")
        :type url_to_graphic: str

        :param graphic_format: the graphic format/Mime type (i.e. "image/svg")
        :type graphic_format: str
        """
        self.url_to_graphic = url_to_graphic
        self.graphic_format = graphic_format


class StyledLayerDescriptorParser(object):
    """
    Parses information from Styled Layer Descriptor XML.
    """

    def __init__(self,
                 sld_file_path):
        """
        Constructor
        :param sld_file_path: the file path location to the sld file
        :type sld_file_path: str
        """
        self.sld_file_path = sld_file_path
        self.tree = DET.parse(source=sld_file_path)
        self.root = self.tree.getroot()

    def get_external_graphics(self):
        """
        Returns a list of ExternalGraphic objects that are in the SLD xml
        :rtype: list of ExternalGraphic
        """
        external_graphics = self.get_external_graphic_elements()

        graphics = []
        for external_graphic in external_graphics:
            url_to_graphic = self.get_url_to_graphic_from_external_graphic_element(external_graphic)
            graphic_format = self.get_graphic_format_from_external_graphic_element(external_graphic)
            graphics.append(ExternalGraphic(url_to_graphic=url_to_graphic,
                                            graphic_format=graphic_format))

        return graphics

    @staticmethod
    def get_graphic_format_from_external_graphic_element(external_graphic_element):
        """
        Returns the string representation of the graphic format (i.e. image/svg).
        :param external_graphic_element: the ExternalGraphic Element
        :type external_graphic_element: Element
        :rtype: str
        """
        return external_graphic_element.find(GRAPHIC_FORMAT_TAG) \
            .text

    @staticmethod
    def get_url_to_graphic_from_external_graphic_element(external_graphic_element):
        """
        Returns the string representation of the url the graphic is pointing to.

        :param external_graphic_element: the ExternalGraphic Element
        :type external_graphic_element: Element
        :rtype: str
        """
        return external_graphic_element.find(ONLINE_RESOURCE_TAG) \
            .get(ONLINE_RESOURCE_URL_ATTRIBUTE_TAG)

    def get_external_graphic_elements(self):
        """
        Returns a list of Elements in the SLD that represent the External Graphics.
        :rtype: list of Element
        """
        return self.root.findall(".//{external_graphic_element}"
                                 .format(external_graphic_element=EXTERNAL_GRAPHIC_ELEMENT))
