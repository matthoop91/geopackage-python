#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter, \
    GEOPACKAGE_EXTENSIONS_TABLE_NAME
from rgi.geopackage.utility.sql_column_query import SqlColumnQuery
from rgi.geopackage.utility.sql_utility import table_exists, select_all_query, select_query, insert_or_update_row
from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE
from rgi.geopackage.extensions.ows.context_offering_entry import ContextOfferingEntry
from sqlite3 import Cursor

CONTEXT_OFFERING_TABLE_NAME = "gpkgext_context_offerings"
EXTENSION_NAME = "ows_context"

#TODO: Contact / email the appropriate source for a context resource definition.
EXTENSION_CONTEXT_RESOURCES_DEFINITION = "http://www.owscontext.org/#standard"


class GeoPackageContextOfferingTableAdapter(ExtensionEntry):
    """
    Object representing a GeoPackage Context Offering.
    """

    def __init__(self):
        super(GeoPackageContextOfferingTableAdapter, self).__init__(table_name=CONTEXT_OFFERING_TABLE_NAME,
                                                                    column_name=None,
                                                                    scope=EXTENSION_READ_WRITE_SCOPE,
                                                                    definition=EXTENSION_CONTEXT_RESOURCES_DEFINITION,
                                                                    extension_name=EXTENSION_NAME)

    @staticmethod
    def create_context_offering_table(cursor):
        """
        Creates the table gpkgext_context_offerings to store all Geopackage context offering entries.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor
        """
        cursor.execute(""" 
                            CREATE TABLE IF NOT EXISTS {table_name}
                            (
                            id	INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT UNIQUE,
                            resource_id	INTEGER NOT NULL,
                            code TEXT NOT NULL, --foreign key
                            method TEXT  NOT NULL,
                            type TEXT  NOT NULL,
                            stylesheet_id INTEGER,
                            contents BLOB,
                            FOREIGN KEY('resource_id') REFERENCES 'gpkgext_context_resources'('id'),
                            FOREIGN KEY('stylesheet_id') REFERENCES 'gpkgext_stylesheets'('id')
                            );
                            """.format(table_name=CONTEXT_OFFERING_TABLE_NAME))

        # register extension in the extensions table
        if not GeoPackageExtensionsTableAdapter.has_extension(cursor=cursor,
                                                              extension=GeoPackageContextOfferingTableAdapter()):
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=GeoPackageContextOfferingTableAdapter())

    @staticmethod
    def get_context_offering_entry(cursor, context_offering_id):
        """
        Returns a single GeoPackage context offering entry based on a given context offering id.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_offering_id: the id of the GeoPackage context offering looking for
        :type context_offering_id: int

        :return: A single GeoPackage context offering entry [1 row of data]
        :rtype: ContextOfferingEntry
        """

        rows = select_query(cursor=cursor,
                            table_name=CONTEXT_OFFERING_TABLE_NAME,
                            select_columns=['id', 'resource_id', 'code', 'stylesheet_id', 'contents', 'method', 'type'],
                            where_columns_dictionary={'id': context_offering_id})

        if rows is None or len(rows) == 0:
            return None

        row = rows[0]
        return ContextOfferingEntry(context_offering_id=row['id'],
                                    context_offering_resource_id=row['resource_id'],
                                    code=row['code'],
                                    stylesheet_id=row['stylesheet_id'],
                                    contents=row['contents'],
                                    method=row['method'],
                                    mime_type=row['type'])

    @staticmethod
    def get_all_context_offering_entries(cursor):
        """
        Returns all GeoPackage context offering entries (rows) from the gpkgext_context_offerings table.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :return: All context offering entries from the gpkgext_context_offerings table
        :rtype: list of ContextOfferingEntry
        """
        if not table_exists(cursor, CONTEXT_OFFERING_TABLE_NAME):
            return []

        rows = select_all_query(cursor=cursor, table_name=CONTEXT_OFFERING_TABLE_NAME)

        return [ContextOfferingEntry(context_offering_id=row['id'],
                                     context_offering_resource_id=row['resource_id'],
                                     code=row['code'],
                                     stylesheet_id=row['stylesheet_id'],
                                     contents=row['contents'],
                                     method=row['method'],
                                     mime_type=row['type']
                                     ) for row in rows]

    @staticmethod
    def insert_or_update_context_offering_entry(cursor, context_offering_entry):
        """
        Inserts a new GeoPackage context offering entry (row) - or, updates an existing GeoPackage context entry - into
        the gpkgext_context_offerings table.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_offering_entry: the context offering resource we want to insert or update
        :type context_offering_entry: ContextOfferingEntry

        """

        if not table_exists(cursor, table_name=CONTEXT_OFFERING_TABLE_NAME):
            GeoPackageContextOfferingTableAdapter.create_context_offering_table(cursor=cursor)

        insert_or_update_row(cursor=cursor,
                             table_name=CONTEXT_OFFERING_TABLE_NAME,
                             sql_columns_list=[
                                 SqlColumnQuery(column_name='id',
                                                include_in_where_clause=True,
                                                column_value=context_offering_entry.context_offering_id),
                                 SqlColumnQuery(column_name='resource_id',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.context_offering_resource_id),
                                 SqlColumnQuery(column_name='method',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.method),
                                 SqlColumnQuery(column_name='type',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.mime_type),
                                 SqlColumnQuery(column_name='code',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.code),
                                 SqlColumnQuery(column_name='stylesheet_id',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.stylesheet_id),
                                 SqlColumnQuery(column_name='contents',
                                                include_in_where_clause=False,
                                                column_value=context_offering_entry.contents)
                             ])
        