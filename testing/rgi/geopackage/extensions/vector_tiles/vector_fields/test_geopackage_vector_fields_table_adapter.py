#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from pytest import raises

from rgi.geopackage.extensions.vector_tiles.geopackage_mapbox_vector_tiles_table_adapter import \
    GeoPackageMapBoxVectorTilesTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.geopackage_vector_fields_table_adapter import \
    GeoPackageVectorFieldsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.vector_fields_entry import VectorFieldsEntry, \
    VectorFieldType
from rgi.geopackage.extensions.vector_tiles.vector_tiles_constants import GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME
from rgi.geopackage.utility.sql_utility import get_database_connection
from testing.rgi.test_utility import create_table


class TestGeoPackageVectorFieldsTableAdapter(object):

    def test_get_all_entries_invalid_vector_fields_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.get_all_vector_fields_entries(cursor=db.cursor())

    def test_get_all_entries_by_value_invalid_vector_fields_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=db.cursor(),
                                                                                    id=1)

    def test_insert_or_update_in_bulk_invalid_vector_fields_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME)
            vector_fields_entry = VectorFieldsEntry(layer_id=1,
                                                    name='field name',
                                                    field_type=VectorFieldType.BOOLEAN)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.insert_or_update_vector_field_entries_in_bulk(cursor=db.cursor(),
                                                                                                 vector_tiles_table_name='tiles',
                                                                                                 vector_layer_name='layer',
                                                                                                 vector_field_entries=[
                                                                                                     vector_fields_entry])
    def test_insert_or_update_invalid_vector_fields_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME)
            vector_fields_entry = VectorFieldsEntry(layer_id=1,
                                                    name='field name',
                                                    field_type=VectorFieldType.BOOLEAN)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.insert_or_update_vector_tile_field_entry(cursor=db.cursor(),
                                                                                            vector_tiles_field_entry=vector_fields_entry)

    def test_get_all_vector_entries_empty(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.get_all_vector_fields_entries(cursor=cursor)

    def test_get_all_vector_entries_by_values_no_match(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageVectorFieldsTableAdapter.create_vector_fields_table(cursor=cursor)
            assert len(GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=cursor,
                                                                                           id=None,
                                                                                           layer_id=1,
                                                                                           name="name",
                                                                                           type=VectorFieldType.BOOLEAN)) == 0

    def test_get_all_vector_entries_by_values2(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            vector_fields_entry = VectorFieldsEntry(layer_id=1,
                                                    name='field name',
                                                    field_type=VectorFieldType.BOOLEAN)
            GeoPackageMapBoxVectorTilesTableAdapter(vector_fields_entry.name).create_default_tiles_tables(cursor=cursor)
            GeoPackageVectorFieldsTableAdapter.insert_or_update_vector_tile_field_entry(cursor=cursor,
                                                                                        vector_tiles_field_entry=vector_fields_entry)
            assert len(GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=cursor,
                                                                                           id=1,
                                                                                           layer_id=1,
                                                                                           name="name",
                                                                                           type=VectorFieldType.BOOLEAN)) == 0

            assert len(GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=cursor,
                                                                                           id=None,
                                                                                           layer_id=vector_fields_entry.layer_id,
                                                                                           name=vector_fields_entry.name,
                                                                                           type=vector_fields_entry.type)) == 1

    def test_insert_or_udpate_vector_field_entry_empty(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            vector_fields_entry = VectorFieldsEntry(layer_id=1,
                                                    name='field name',
                                                    field_type=VectorFieldType.BOOLEAN)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.insert_or_update_vector_tile_field_entry(cursor=cursor,
                                                                                            vector_tiles_field_entry=
                                                                                            vector_fields_entry)

    def test_get_all_vector_field_entries(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            vector_fields_entry = VectorFieldsEntry(layer_id=1,
                                                    name='field name',
                                                    field_type=VectorFieldType.BOOLEAN)
            GeoPackageMapBoxVectorTilesTableAdapter(vector_fields_entry.name).create_default_tiles_tables(cursor=cursor)

            assert len(GeoPackageVectorFieldsTableAdapter.get_all_vector_fields_entries(cursor=cursor)) == 0

            GeoPackageVectorFieldsTableAdapter.insert_or_update_vector_tile_field_entry(cursor=cursor,
                                                                                        vector_tiles_field_entry=vector_fields_entry)
            assert len(GeoPackageVectorFieldsTableAdapter.get_all_vector_fields_entries(cursor=cursor)) == 1

    def test_get_vector_field_entry_by_values_all_none_values(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageVectorFieldsTableAdapter.create_vector_fields_table(cursor=cursor)
            with raises(ValueError):
                GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=cursor)
