from datetime import datetime

from rgi.geopackage.extensions.ows.geopackage_context_table_adapter import GeoPackageContextTableAdapter
from rgi.geopackage.extensions.ows.context_entry import ContextEntry
from rgi.geopackage.utility.sql_utility import table_exists, get_database_connection, column_exists

CONTEXT_TABLE_NAME = "gpkgext_context"
TEST_CONTEXT_ID = 70

DATETIME_FORMAT = "%Y%m-%dT%H:%M:%fZ"

#Format the dates as they're created. They're easier to work with later.
DEFAULT_MIN_TIME = datetime.now().strftime(DATETIME_FORMAT)
DEFAULT_MAX_TIME = datetime.now().strftime(DATETIME_FORMAT)
DEFAULT_LAST_CHANGED_DATE = datetime.now().strftime(DATETIME_FORMAT)


# noinspection PyClassHasNoInit
class TestGeoPackageContextTableAdapter:

    def test_create_context_entry(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

        GeoPackageContextTableAdapter.create_context_table(cursor)

        assert table_exists(cursor=cursor, table_name=CONTEXT_TABLE_NAME)
        assert column_exists(cursor, CONTEXT_TABLE_NAME, "id")
        assert column_exists(cursor, CONTEXT_TABLE_NAME, "title")
        assert column_exists(cursor, CONTEXT_TABLE_NAME, "abstract")
        assert column_exists(cursor, CONTEXT_TABLE_NAME, "last_change")
        assert column_exists(cursor, CONTEXT_TABLE_NAME, "max_time")

    def test_insert_or_update_context_entry(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            # INSERT
            # Create ALL the new fields.
            new_context_id = TEST_CONTEXT_ID
            new_title = "Another_Random_Title"
            new_metadata_id = 4
            new_abstract = "Abstract 5"
            new_min_x = 4
            new_min_y = 5
            new_max_x = 5000.2
            new_max_y = 8800.7
            new_srs_id = 56
            new_author = "Jim_Beam"
            new_publisher = "Mary_Carome_Evil_Twin"
            new_creator = "Mr_Creator"
            new_rights = "Miranda_Rights"
            new_keywords = "Samus_Aran"

            # Insert ALL those new fields.
            inserted_context_entry = self.create_context_entry()
            inserted_context_entry.context_id = new_context_id
            inserted_context_entry.metadata_id = new_metadata_id
            inserted_context_entry.title = new_title
            inserted_context_entry.abstract = new_abstract
            inserted_context_entry.max_x = new_max_x
            inserted_context_entry.max_y = new_max_y
            inserted_context_entry.min_x = new_min_x
            inserted_context_entry.min_y = new_min_y
            inserted_context_entry.srs_id = new_srs_id
            inserted_context_entry.author = new_author
            inserted_context_entry.publisher = new_publisher
            inserted_context_entry.creator = new_creator
            inserted_context_entry.rights = new_rights
            inserted_context_entry.keywords = new_keywords
            #FORMAT the dates as they're inserted!
            inserted_context_entry.max_time = DEFAULT_MAX_TIME
            inserted_context_entry.min_time = DEFAULT_MIN_TIME

            # Insert the new context entry.
            GeoPackageContextTableAdapter.insert_or_update_context_entry(cursor, inserted_context_entry)

            retrieved_context_entry = GeoPackageContextTableAdapter.get_context_entry(cursor=cursor,
                                                                                      context_id=new_context_id)

            # Check ALL the new fields.
            assert retrieved_context_entry.context_id == inserted_context_entry.context_id
            assert retrieved_context_entry.metadata_id == inserted_context_entry.metadata_id
            assert retrieved_context_entry.title == inserted_context_entry.title
            assert retrieved_context_entry.abstract == inserted_context_entry.abstract
            assert retrieved_context_entry.min_x == inserted_context_entry.min_x
            assert retrieved_context_entry.min_y == inserted_context_entry.min_y
            assert retrieved_context_entry.max_x == inserted_context_entry.max_x
            assert retrieved_context_entry.max_y == inserted_context_entry.max_y
            assert retrieved_context_entry.srs_id == inserted_context_entry.srs_id
            assert retrieved_context_entry.author == inserted_context_entry.author
            assert retrieved_context_entry.publisher == inserted_context_entry.publisher
            assert retrieved_context_entry.creator == inserted_context_entry.creator
            assert retrieved_context_entry.rights == inserted_context_entry.rights
            assert retrieved_context_entry.keywords == inserted_context_entry.keywords
            #FORMAT the dates as you pull them out.
            datetime.strptime(retrieved_context_entry.max_time, DATETIME_FORMAT)
            datetime.strptime(retrieved_context_entry.min_time, DATETIME_FORMAT)

            # Compare datetime object values. Make sure they have the same format.
            assert retrieved_context_entry.max_time == inserted_context_entry.max_time
            assert retrieved_context_entry.min_time == inserted_context_entry.min_time

            # UPDATE
            # Create all the fields to update
            updated_metadata_id = 223
            updated_title = "No_Title"
            updated_abstract = "Abstract 33.3"
            updated_min_x = 7
            updated_min_y = 9
            updated_max_x = 303.3
            updated_max_y = 202.2
            updated_srs_id = 99
            updated_author = "Richard_Kim"
            updated_publisher = "Tori"
            updated_creator = "Queen_Creator"
            updated_rights = "Super_User"
            updated_keywords = "Shrimp"

            updated_context_entry = self.create_context_entry()
            updated_context_entry.metadata_id = updated_metadata_id
            updated_context_entry.title = updated_title
            updated_context_entry.abstract = updated_abstract
            updated_context_entry.max_x = updated_max_x
            updated_context_entry.max_y = updated_max_y
            updated_context_entry.min_x = updated_min_x
            updated_context_entry.min_y = updated_min_y
            updated_context_entry.srs_id = updated_srs_id
            updated_context_entry.author = updated_author
            updated_context_entry.publisher = updated_publisher
            updated_context_entry.creator = updated_creator
            updated_context_entry.rights = updated_rights
            updated_context_entry.keywords = updated_keywords
            # FORMAT the dates as they're inserted!
            updated_context_entry.max_time = DEFAULT_MAX_TIME
            updated_context_entry.min_time = DEFAULT_MIN_TIME

            # Insert the new context entry.
            GeoPackageContextTableAdapter.insert_or_update_context_entry(cursor, updated_context_entry)

            retrieved_updated_context_entry = GeoPackageContextTableAdapter.get_context_entry(cursor=cursor,
                                                                                              context_id=new_context_id)

            # Check ALL the updated fields.
            assert retrieved_updated_context_entry.context_id == updated_context_entry.context_id
            assert retrieved_updated_context_entry.metadata_id == updated_context_entry.metadata_id
            assert retrieved_updated_context_entry.title == updated_context_entry.title
            assert retrieved_updated_context_entry.abstract == updated_context_entry.abstract
            assert retrieved_updated_context_entry.min_x == updated_context_entry.min_x
            assert retrieved_updated_context_entry.min_y == updated_context_entry.min_y
            assert retrieved_updated_context_entry.max_x == updated_context_entry.max_x
            assert retrieved_updated_context_entry.max_y == updated_context_entry.max_y
            assert retrieved_updated_context_entry.srs_id == updated_context_entry.srs_id
            assert retrieved_updated_context_entry.author == updated_context_entry.author
            assert retrieved_updated_context_entry.publisher == updated_context_entry.publisher
            assert retrieved_updated_context_entry.creator == updated_context_entry.creator
            assert retrieved_updated_context_entry.rights == updated_context_entry.rights
            assert retrieved_updated_context_entry.keywords == updated_context_entry.keywords
            #FORMAT the dates as you pull them out.
            datetime.strptime(retrieved_updated_context_entry.max_time, DATETIME_FORMAT)
            datetime.strptime(retrieved_updated_context_entry.min_time, DATETIME_FORMAT)

            # Compare datetime object values. Make sure they have the same format.
            assert retrieved_updated_context_entry.max_time == updated_context_entry.max_time
            assert retrieved_updated_context_entry.min_time == updated_context_entry.min_time

    def test_get_context_entry(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextTableAdapter.create_context_table(cursor)
            assert table_exists(cursor, CONTEXT_TABLE_NAME)
            inserted_context_entry = self.create_context_entry()
            GeoPackageContextTableAdapter.insert_or_update_context_entry(cursor, inserted_context_entry)

            retrieved_context_entry = GeoPackageContextTableAdapter.get_context_entry(cursor, TEST_CONTEXT_ID)

            #Make sure all context entry fields match.
            assert retrieved_context_entry.context_id == inserted_context_entry.context_id
            assert retrieved_context_entry.title == inserted_context_entry.title
            assert retrieved_context_entry.abstract == inserted_context_entry.abstract
            assert retrieved_context_entry.min_x == inserted_context_entry.min_x
            assert retrieved_context_entry.min_y == inserted_context_entry.min_y
            assert retrieved_context_entry.max_x == inserted_context_entry.max_x
            assert retrieved_context_entry.max_y == inserted_context_entry.max_y
            assert retrieved_context_entry.srs_id == inserted_context_entry.srs_id
            assert retrieved_context_entry.author == inserted_context_entry.author
            assert retrieved_context_entry.publisher == inserted_context_entry.publisher
            assert retrieved_context_entry.creator == inserted_context_entry.creator
            assert retrieved_context_entry.rights == inserted_context_entry.rights
            assert retrieved_context_entry.keywords == inserted_context_entry.keywords
            assert retrieved_context_entry.metadata_id == inserted_context_entry.metadata_id

    def test_get_all_context_entries(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextTableAdapter.create_context_table(cursor)
            assert table_exists(cursor, CONTEXT_TABLE_NAME)

            context_record_count = 8
            inserted_context_entries = []

            #Insert 8 records. Make sure they're inserted in order.
            for x in range(context_record_count):
                inserted_context_entry = self.create_context_entry()
                inserted_context_entry.context_id = x
                inserted_context_entry.title = "Title " + str(x)
                inserted_context_entry.abstract = "Abstract " + str(x)
                GeoPackageContextTableAdapter.insert_or_update_context_entry(cursor, inserted_context_entry)
                inserted_context_entries.append(inserted_context_entry)

            #Retrieve all entries.
            retrieved_context_entries = GeoPackageContextTableAdapter.get_all_context_entries(cursor)
            assert len(retrieved_context_entries) == context_record_count #8 records

            retrieved_context_entries.sort(key=lambda j: j.context_id, reverse=False)
            inserted_context_entries.sort(key=lambda k: k.context_id, reverse=False)

            #Check ALL the values of each entry.
            #Make sure they were retrieved IN ORDER.
            for index in range(len(retrieved_context_entries)):
                assert retrieved_context_entries[index].context_id == inserted_context_entries[index].context_id
                assert retrieved_context_entries[index].title == inserted_context_entries[index].title
                assert retrieved_context_entries[index].abstract == inserted_context_entries[index].abstract
                assert retrieved_context_entries[index].min_x == inserted_context_entries[index].min_x
                assert retrieved_context_entries[index].min_y == inserted_context_entries[index].min_y
                assert retrieved_context_entries[index].max_x == inserted_context_entries[index].max_x
                assert retrieved_context_entries[index].max_y == inserted_context_entries[index].max_y
                assert retrieved_context_entries[index].srs_id == inserted_context_entries[index].srs_id
                assert retrieved_context_entries[index].author == inserted_context_entries[index].author
                assert retrieved_context_entries[index].publisher == inserted_context_entries[index].publisher
                assert retrieved_context_entries[index].creator == inserted_context_entries[index].creator
                assert retrieved_context_entries[index].rights == inserted_context_entries[index].rights
                assert retrieved_context_entries[index].keywords == inserted_context_entries[index].keywords
                assert retrieved_context_entries[index].metadata_id == inserted_context_entries[index].metadata_id

    def create_context_entry(self):

        context_entry = ContextEntry(context_id=TEST_CONTEXT_ID,
                                     title="Frederick_Title",
                                     abstract="Testing, 1, 2, 3...",
                                     min_x=0.0,
                                     min_y=0.0,
                                     max_x=1000.0,
                                     max_y=1000.0,
                                     srs_id=36,
                                     author="Frederick Boyd Jr.",
                                     publisher="Mary Carome",
                                     creator="Jenifer Cochran",
                                     rights="NONE!",
                                     keywords='dog',
                                     metadata_id=2,
                                     max_time=DEFAULT_MAX_TIME,
                                     min_time=DEFAULT_MIN_TIME)
        return context_entry
