#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter, \
    GEOPACKAGE_EXTENSIONS_TABLE_NAME
from rgi.geopackage.utility.sql_column_query import SqlColumnQuery
from rgi.geopackage.utility.sql_utility import select_query, select_all_query, table_exists, insert_or_update_row
from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE
from rgi.geopackage.extensions.ows.extension_style_entry import ExtensionStyleEntry
from sqlite3 import Cursor

EXTENSION_STYLE_TABLE_NAME = "gpkgext_styles"
EXTENSION_NAME = "ows_context"


#TODO: Contact / email the appropriate source for a default request URL.
EXTENSION_STYLES_DEFINITION = " http://www.owscontext.org/#standard"


class GeoPackageExtensionStyleTableAdapter(ExtensionEntry):
    """
    Object representing a GeoPackage Extension Style.
    """

    def __init__(self):
        super(GeoPackageExtensionStyleTableAdapter, self).__init__(table_name=EXTENSION_STYLE_TABLE_NAME,
                                                                   column_name=None,
                                                                   scope=EXTENSION_READ_WRITE_SCOPE,
                                                                   definition=EXTENSION_STYLES_DEFINITION,
                                                                   extension_name=EXTENSION_NAME)

    @staticmethod
    def create_extension_style_table(cursor):
        """
        Creates the table gpkgext_styles to store all Geopackage extension style entries.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor
        """
        cursor.execute(""" 
                        CREATE TABLE IF NOT EXISTS {table_name}
                        (id	INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT UNIQUE,
                        style BLOB NOT NULL,
                        content_type TEXT NOT NULL);
                        """.format(table_name=EXTENSION_STYLE_TABLE_NAME))

        if not GeoPackageExtensionsTableAdapter.has_extension(cursor=cursor,
                                                              extension=GeoPackageExtensionStyleTableAdapter()):
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor, extension=GeoPackageExtensionStyleTableAdapter())

    @staticmethod
    def get_extension_style_entry(cursor, extension_style_id):
        """
        Returns a single GeoPackage extension style entry based on a given extension style id.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param extension_style_id: the id of the extension style entry we're looking for
        :type extension_style_id: int

        :return: A single record in the gpkgext_styles table
        :rtype: ExtensionStyleEntry
        """

        rows = select_query(cursor=cursor,
                            table_name=EXTENSION_STYLE_TABLE_NAME,
                            select_columns=['id', 'style', 'content_type'],
                            where_columns_dictionary={'id': extension_style_id})

        if rows is None or len(rows) == 0:
            return None

        row = rows[0]

        return ExtensionStyleEntry(extension_style_id=row['id'],
                                   style=row['style'],
                                   content_type=row['content_type'])

    @staticmethod
    def get_all_extension_style_entries(cursor):
        """
        Returns all GeoPackage context entries (rows) from the gpkgext_context table.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :return: All extension style entries in the gpkgext_styles table
        :rtype: list of ExtensionStyleEntry
        """

        rows = select_all_query(cursor=cursor, table_name=EXTENSION_STYLE_TABLE_NAME)

        return [ExtensionStyleEntry(extension_style_id=row['id'],
                                    style=row['style'],
                                    content_type=row['content_type']) for row in rows]

    @staticmethod
    def insert_or_update_extension_style_entry(cursor, extension_style_entry):
        """
        Inserts a new GeoPackage extension style entry (row) - or, updates an existing GeoPackage extension style entry
         - into the gpkgext_styles table.

        :param cursor: The cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param extension_style_entry: An extension styles entry object for a geopackage
        :type extension_style_entry: ExtensionStyleEntry
        """

        if not table_exists(cursor, table_name=EXTENSION_STYLE_TABLE_NAME):
            GeoPackageExtensionStyleTableAdapter.create_extension_style_table(cursor=cursor)

        insert_or_update_row(cursor=cursor,
                             table_name=EXTENSION_STYLE_TABLE_NAME,
                             sql_columns_list=[
                                 SqlColumnQuery(column_name='id',
                                                include_in_where_clause=True,
                                                column_value=extension_style_entry.extension_style_id),
                                 SqlColumnQuery(column_name='style',
                                                include_in_where_clause=False,
                                                column_value=extension_style_entry.style),
                                 SqlColumnQuery(column_name='content_type',
                                                include_in_where_clause=False,
                                                column_value=extension_style_entry.content_type)
                             ])
