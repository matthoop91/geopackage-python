
from rgi.geopackage.extensions.ows.geopackage_extension_style_table_adapter import GeoPackageExtensionStyleTableAdapter
from rgi.geopackage.extensions.ows.geopackage_extension_style_table_adapter import ExtensionStyleEntry
from rgi.geopackage.utility.sql_utility import table_exists, get_database_connection, column_exists

EXTENSION_STYLES_TABLE_NAME = "gpkgext_styles"
TEST_EXTENSION_STYLE_ID = 40


# noinspection PyClassHasNoInit
class TestGeoPackageExtensionStyleTableAdapter:

    def test_create_extension_style(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

        GeoPackageExtensionStyleTableAdapter.create_extension_style_table(cursor)

        assert table_exists(cursor=cursor, table_name=EXTENSION_STYLES_TABLE_NAME)
        assert column_exists(cursor, EXTENSION_STYLES_TABLE_NAME, "id")
        assert column_exists(cursor, EXTENSION_STYLES_TABLE_NAME, "style")
        assert column_exists(cursor, EXTENSION_STYLES_TABLE_NAME, "content_type")

    def test_insert_or_update_extension_style(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageExtensionStyleTableAdapter.create_extension_style_table(cursor)
            assert table_exists(cursor, EXTENSION_STYLES_TABLE_NAME)

            #INSERT
            #Create ALL the new fields.
            new_content_type = "Strawberry_flavored"
            new_style = "Saucony_Kinvara"
            new_extension_style_id = TEST_EXTENSION_STYLE_ID

            #Insert ALL those new fields.
            inserted_extension_style_entry = self.create_extension_style_entry()
            inserted_extension_style_entry.content_type = new_content_type
            inserted_extension_style_entry.style = new_style
            inserted_extension_style_entry.extension_style_id = new_extension_style_id

            # Insert the new style entry.
            GeoPackageExtensionStyleTableAdapter.insert_or_update_extension_style_entry(cursor, inserted_extension_style_entry)

            retrieved_extension_style_entry = GeoPackageExtensionStyleTableAdapter.get_extension_style_entry(cursor=cursor,
                                                                                                             extension_style_id=TEST_EXTENSION_STYLE_ID)
            #Check ALL the new fields.
            assert retrieved_extension_style_entry.content_type == inserted_extension_style_entry.content_type
            assert retrieved_extension_style_entry.style == inserted_extension_style_entry.style
            assert retrieved_extension_style_entry.extension_style_id == inserted_extension_style_entry.extension_style_id

            #UPDATE
            #Create ALL the new fields.
            updated_content_type = "Vanilla_flavored"
            updated_style = "This_Style"
            updated_extension_style_id = TEST_EXTENSION_STYLE_ID

            #Insert ALL those updated fields.
            updated_extension_style_entry = self.create_extension_style_entry()
            updated_extension_style_entry.content_type = updated_content_type
            updated_extension_style_entry.style = updated_style
            updated_extension_style_entry.extension_style_id = updated_extension_style_id

            # Insert the new context entry.
            GeoPackageExtensionStyleTableAdapter.insert_or_update_extension_style_entry(cursor, updated_extension_style_entry)

            retrieved_updated_extension_style_entry = GeoPackageExtensionStyleTableAdapter.get_extension_style_entry(cursor=cursor,
                                                                                                                     extension_style_id=TEST_EXTENSION_STYLE_ID)

            # Check ALL the updated fields.
            assert retrieved_updated_extension_style_entry.extension_style_id == updated_extension_style_entry.extension_style_id
            assert retrieved_updated_extension_style_entry.style == updated_extension_style_entry.style
            assert retrieved_updated_extension_style_entry.content_type == updated_extension_style_entry.content_type

    def test_get_extension_style(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageExtensionStyleTableAdapter.create_extension_style_table(cursor)
            assert table_exists(cursor, EXTENSION_STYLES_TABLE_NAME)
            inserted_extension_style_entry = self.create_extension_style_entry()

            GeoPackageExtensionStyleTableAdapter.insert_or_update_extension_style_entry(cursor, inserted_extension_style_entry)

            retrieved_extension_style_entry = GeoPackageExtensionStyleTableAdapter.get_extension_style_entry(cursor, TEST_EXTENSION_STYLE_ID)

            # Make sure all extension style fields match.
            assert retrieved_extension_style_entry.extension_style_id == inserted_extension_style_entry.extension_style_id
            assert retrieved_extension_style_entry.content_type == inserted_extension_style_entry.content_type
            assert retrieved_extension_style_entry.style == inserted_extension_style_entry.style

    def test_get_all_extension_styles(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageExtensionStyleTableAdapter.create_extension_style_table(cursor)
            assert table_exists(cursor, EXTENSION_STYLES_TABLE_NAME)

            extension_style_record_count = 4
            inserted_extension_style_entries = []

            for x in range(extension_style_record_count):
                inserted_extension_style_entry = self.create_extension_style_entry()
                inserted_extension_style_entry.extension_style_id = x
                GeoPackageExtensionStyleTableAdapter.insert_or_update_extension_style_entry(cursor, inserted_extension_style_entry)
                inserted_extension_style_entries.append(inserted_extension_style_entry)

            retrieved_extension_style_entries = GeoPackageExtensionStyleTableAdapter.get_all_extension_style_entries(cursor)
            retrieved_style_entries_count = len(retrieved_extension_style_entries)
            assert retrieved_style_entries_count == extension_style_record_count  # 4 records

            #We're sorting complex objects by that object's id.
            retrieved_extension_style_entries.sort(key=lambda j: j.extension_style_id, reverse=False)
            inserted_extension_style_entries.sort(key=lambda r: r.extension_style_id, reverse=False)

            for y in range(retrieved_style_entries_count):
                assert retrieved_extension_style_entries[y].extension_style_id == inserted_extension_style_entries[y].extension_style_id
                assert retrieved_extension_style_entries[y].content_type == inserted_extension_style_entries[y].content_type
                assert retrieved_extension_style_entries[y].style == inserted_extension_style_entries[y].style

    def create_extension_style_entry(self):

        return ExtensionStyleEntry(extension_style_id=TEST_EXTENSION_STYLE_ID,
                                   style='Frederick_Boyd_Jr',
                                   content_type='text_or_something')
