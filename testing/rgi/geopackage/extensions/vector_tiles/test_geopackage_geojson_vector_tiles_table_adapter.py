#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from rgi.geopackage.extensions.vector_tiles.geopackage_geojson_vector_tiles_table_adapter import GeoPackageGeoJSONVectorTilesTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.geopackage_vector_fields_table_adapter import GeoPackageVectorFieldsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.vector_fields_entry import VectorFieldType
from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import GeoPackageVectorLayersTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_tiles_content_entry import VectorTilesContentEntry
from rgi.geopackage.utility.sql_utility import get_database_connection
from testing.rgi.test_utility import TEST_GEOJSON


class TestGeoPackageGeoJsonVectorTilesTableAdapter(object):

    def test_parsing_data(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            vector_tiles_entry = VectorTilesContentEntry(table_name="table",
                                                         identifier="id",
                                                         min_x=1.0,
                                                         min_y=2.0,
                                                         max_x=3.0,
                                                         max_y=4.0,
                                                         srs_id=4326)
            GeoPackageGeoJSONVectorTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                                    tiles_content=vector_tiles_entry)
            GeoPackageGeoJSONVectorTilesTableAdapter.insert_or_update_tile_data(cursor=cursor,
                                                                                table_name="table",
                                                                                zoom_level=1,
                                                                                tile_column=2,
                                                                                tile_row=3,
                                                                                tile_data=TEST_GEOJSON)
            db_conn.commit()
            vector_layers = GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name(cursor=cursor,
                                                                                                      vector_tiles_table_name=vector_tiles_entry.table_name)

            assert len(vector_layers) == 1

            assert vector_layers[0].table_name == vector_tiles_entry.table_name and \
                   vector_layers[0].name == vector_tiles_entry.table_name

            vector_fields = GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=cursor,
                                                                                                layer_id=vector_layers[0].identifier)

            assert len(vector_fields) == 3

            vector_field_hello = next((vector_field for vector_field in vector_fields if vector_field.name == "hello"),
                                      None)
            vector_field_count = next((vector_field for vector_field in vector_fields if vector_field.name == "count"),
                                      None)
            vector_field_h = next((vector_field for vector_field in vector_fields if vector_field.name == "h"), None)

            assert vector_field_hello is not None and \
                   vector_field_hello.type == VectorFieldType.STRING and \
                   vector_field_hello.layer_id == vector_layers[0].identifier

            assert vector_field_count is not None and \
                   vector_field_count.type == VectorFieldType.NUMBER and \
                   vector_field_count.layer_id == vector_layers[0].identifier

            assert vector_field_h is not None and \
                   vector_field_h.type == VectorFieldType.STRING and \
                   vector_field_h.layer_id == vector_layers[0].identifier
