#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from pytest import raises

from rgi.geopackage.core.geopackage_core_table_adapter import GeoPackageCoreTableAdapter, GEOPACKAGE_CONTENTS_TABLE_NAME
from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE, \
    EXTENSION_WRITE_ONLY_SCOPE
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter, \
    GEOPACKAGE_EXTENSIONS_TABLE_NAME
from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import \
    GeoPackageVectorLayersTableAdapter
from rgi.geopackage.tiles.geopackage_tiles_table_adapter import GeoPackageTilesTableAdapter
from rgi.geopackage.tiles.tiles_content_entry import TilesContentEntry
from rgi.geopackage.utility.sql_utility import get_database_connection, table_exists
from testing.rgi.test_utility import create_table


class TestGeoPackageExtensionsTableAdapter(object):

    def test_insert_or_update_row_invalid_extensions_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_EXTENSIONS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=db.cursor(),
                                                                                 extension=GeoPackageVectorLayersTableAdapter())

    def test_get_extensions_invalid_extensions_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_EXTENSIONS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageExtensionsTableAdapter.get_all_extensions(cursor=db.cursor())

    def test_get_extension_by_table_name_invalid_extensions_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_EXTENSIONS_TABLE_NAME)
            with raises(ValueError):
                GeoPackageExtensionsTableAdapter.get_extensions_with_table_name(cursor=db.cursor(),
                                                                                table_name="nammme")

    def test_getting_all_extensions(self, make_gpkg):
        table_name = "table table"
        my_extension = ExtensionEntry(table_name=table_name,
                                      column_name=None,
                                      extension_name="extension name name",
                                      definition="Defffinition",
                                      scope=EXTENSION_READ_WRITE_SCOPE)
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            assert not GeoPackageExtensionsTableAdapter.has_extension(cursor=cursor,
                                                                      extension=my_extension)

            # need to create the table to exist
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(
                                                                           table_name=table_name,
                                                                           identifier="Some identifier",
                                                                           min_x=0,
                                                                           min_y=0,
                                                                           max_x=0,
                                                                           max_y=0,
                                                                           srs_id=0))
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=my_extension)

            one_extension = GeoPackageExtensionsTableAdapter.get_all_extensions(cursor)

            assert len(one_extension) == 1

            assert one_extension[0].table_name == my_extension.table_name and \
                   one_extension[0].column_name == my_extension.column_name and \
                   one_extension[0].extension_name == my_extension.extension_name and \
                   one_extension[0].definition == my_extension.definition

    def test_getting_multiple_all_extensions(self, make_gpkg):
        my_extension = ExtensionEntry(table_name="table table",
                                      column_name="id",
                                      extension_name="extension name name",
                                      definition="Defffinition",
                                      scope=EXTENSION_READ_WRITE_SCOPE)

        my_extension2 = ExtensionEntry(table_name=None,
                                       column_name=None,
                                       extension_name="other extension name",
                                       definition="definition2",
                                       scope=EXTENSION_WRITE_ONLY_SCOPE)
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            # check to see if the table is currently empty
            assert not table_exists(cursor=cursor,
                                    table_name=GEOPACKAGE_EXTENSIONS_TABLE_NAME)

            # need to create the table to exist
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(
                                                                           table_name=my_extension
                                                                           .table_name,
                                                                           identifier="Some identifier",
                                                                           min_x=0,
                                                                           min_y=0,
                                                                           max_x=0,
                                                                           max_y=0,
                                                                           srs_id=0))
            # add the first extension
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=my_extension)
            # check to see if one extension is in the table
            one_extension = GeoPackageExtensionsTableAdapter.get_all_extensions(cursor)

            assert len(one_extension) == 1

            # add the second extension
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=my_extension2)

            # check to see if there are two extensions in the table
            two_extensions = GeoPackageExtensionsTableAdapter.get_all_extensions(cursor=cursor)

            assert len(two_extensions) == 2

            # check if the values of the two extensions are valid
            assert two_extensions[0].table_name == my_extension.table_name and \
                   two_extensions[0].column_name == my_extension.column_name and \
                   two_extensions[0].extension_name == my_extension.extension_name and \
                   two_extensions[0].definition == my_extension.definition and \
                   two_extensions[1].table_name == my_extension2.table_name and \
                   two_extensions[1].column_name == my_extension2.column_name and \
                   two_extensions[1].extension_name == my_extension2.extension_name and \
                   two_extensions[1].definition == my_extension2.definition

    def test_empty_extensions(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            # tests to make sure if table is created that the list to get extensions is still empty
            GeoPackageExtensionsTableAdapter.create_extensions_table(cursor=cursor)

            # checks to make sure list is empty
            empty_extensions = GeoPackageExtensionsTableAdapter.get_all_extensions(cursor=cursor)
            assert len(empty_extensions) == 0

    def test_table_needs_to_exist_exception(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                                 extension=ExtensionEntry(
                                                                                     table_name="Non_Existent_Table",
                                                                                     column_name=None,
                                                                                     extension_name="extension name",
                                                                                     definition="definition",
                                                                                     scope=EXTENSION_READ_WRITE_SCOPE))

    def test_column_needs_to_exist_exception(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            with raises(ValueError):
                GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                                 extension=ExtensionEntry(
                                                                                     table_name=GEOPACKAGE_CONTENTS_TABLE_NAME,
                                                                                     column_name="non-existent-column",
                                                                                     extension_name="extension name",
                                                                                     definition="definition",
                                                                                     scope=EXTENSION_READ_WRITE_SCOPE))

    def test_get_extension_by_table_name(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)

            test_extension = ExtensionEntry(table_name=GEOPACKAGE_CONTENTS_TABLE_NAME,
                                            column_name=None,
                                            extension_name='just a test',
                                            definition='my def',
                                            scope=EXTENSION_WRITE_ONLY_SCOPE)

            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=test_extension)

            returned_extensions = GeoPackageExtensionsTableAdapter.get_extensions_with_table_name(cursor=cursor,
                                                                                                  table_name=test_extension.table_name)

            assert len(returned_extensions) == 1
            self.assert_extension_entries_equal(expected_extension=test_extension,
                                                actual_extension=returned_extensions[0])

    @staticmethod
    def assert_extension_entries_equal(expected_extension, actual_extension):
        """
        asserts if the two extension entries are equal.

        :param expected_extension: the expected extension values
        :type expected_extension: ExtensionEntry
        :param actual_extension: the actual extension values
        :type actual_extension: ExtensionEntry
        """
        assert expected_extension.table_name == actual_extension.table_name and \
               expected_extension.extension_name == actual_extension.extension_name and \
               expected_extension.definition == actual_extension.definition and \
               expected_extension.column_name == actual_extension.column_name and \
               expected_extension.scope == actual_extension.scope
