#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2019-1-30
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from pytest import raises

from rgi.geopackage.common.bounding_box import BoundingBox
from rgi.geopackage.core.geopackage_core_table_adapter import GeoPackageCoreTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import \
    GeoPackageVectorLayersTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_layers.vector_layers_entry import VectorLayersEntry
from rgi.geopackage.extensions.vector_tiles.vector_styles.geopackage_stylesheets_table_adapter import \
    GeoPackageStylesheetsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_styles.stylesheets_entry import StyleFormat, StyleSheetsEntry
from rgi.geopackage.extensions.vector_tiles.vector_styles.styles_extension_constants import \
    GEOPACKAGE_STYLESHEETS_TABLE_NAME
from rgi.geopackage.extensions.vector_tiles.vector_tiles_constants import GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME
from rgi.geopackage.extensions.vector_tiles.vector_tiles_content_entry import VectorTilesContentEntry
from rgi.geopackage.writers.geopackage_tiles_writer import GeoPackageTilesWriter
from rgi.geopackage.writers.geopackage_vector_tile_styles_writer import GeoPackageVectorTileStylesWriter
from rgi.geopackage.tiles.tiles_content_entry import TilesContentEntry
from rgi.geopackage.utility.sql_utility import get_database_connection, table_exists, validate_table_schema
from rgi.geopackage.writers.geopackage_writer import APPLICATION_ID, USER_VERSION
from testing.rgi import test_utility
from testing.rgi.test_utility import get_file_as_binary, get_sld_style_data_path, create_table, \
    get_mapbox_style_data_path


class TestGeoPackageVectorTileStylesWriter(object):

    def test_application_id_and_user_version(self, get_vector_tiles_gpkg):
        # check if user version and application id are set
        with get_database_connection(get_vector_tiles_gpkg.file_path) as db_conn:
            # Check the application id
            cursor = db_conn.cursor()
            cursor.execute("PRAGMA application_id;")
            application_id_row = cursor.fetchone()
            application_id = application_id_row["application_id"]
            assert application_id == APPLICATION_ID

            # check the user version
            cursor.execute("PRAGMA user_version;")
            user_version_row = cursor.fetchone()
            user_version = user_version_row["user_version"]
            assert user_version == USER_VERSION

    def test_add_style_to_specified_layer(self, get_vector_tiles_gpkg):
        styles_set = 'new_styles'
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) \
                as gpkg_styles_writer:
            style_format = StyleFormat.MAPBOX_STYLE
            style_data = get_file_as_binary(get_mapbox_style_data_path())
            style = 'mystyle'
            description = 'describe stuff'
            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=style_format.value,
                                                          style_data=style_data,
                                                          description=description,
                                                          style=style,
                                                          style_set=styles_set)

            vector_style_entries = gpkg_styles_writer.get_all_style_sheets()

            assert len(vector_style_entries) == 1

            style_entry_returned_agriculture = next((entry
                                                     for entry
                                                     in vector_style_entries
                                                     if
                                                     entry.styles_set == styles_set),
                                                    None)

            assert style_entry_returned_agriculture is not None

            assert style_entry_returned_agriculture.style_format == style_format.value and \
                   style_entry_returned_agriculture.styles_set == styles_set and \
                   style_entry_returned_agriculture.title == style_entry_returned_agriculture.title and \
                   style_entry_returned_agriculture.stylesheet == style_data

    def test_add_two_styles(self, get_vector_tiles_gpkg):
        styles_set = 'my styles set'
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) \
                as gpkg_styles_writer:
            format_1 = StyleFormat.SLD_STYLE
            style_data_1 = get_file_as_binary(get_sld_style_data_path())
            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=format_1.value,
                                                          style_data=style_data_1,
                                                          style_set=styles_set)

            format_2 = StyleFormat.MAPBOX_STYLE
            style_data_2 = get_file_as_binary(get_mapbox_style_data_path())
            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=format_2.value,
                                                          style_data=style_data_2,
                                                          style_set=styles_set)

            vector_style_entries = gpkg_styles_writer.get_all_style_sheets()

            assert len(vector_style_entries) == 2

            style_entry_returned_format1 = next((entry
                                                 for entry
                                                 in vector_style_entries
                                                 if entry.style_format == format_1.value),
                                                None)

            assert style_entry_returned_format1 is not None

            assert style_entry_returned_format1.style_format == format_1.value and \
                   style_entry_returned_format1.styles_set == styles_set and \
                   style_entry_returned_format1.title == 'my-style' and \
                   style_entry_returned_format1.stylesheet == style_data_1

            style_entry_returned_format_2 = next((entry
                                                  for entry
                                                  in vector_style_entries
                                                  if entry.style_format == format_2.value),
                                                 None)

            assert style_entry_returned_format_2 is not None

            assert style_entry_returned_format_2.style_format == format_2.value and \
                   style_entry_returned_format_2.styles_set == styles_set and \
                   style_entry_returned_format_2.title == 'my-style' and \
                   style_entry_returned_format_2.stylesheet == style_data_2

    def test_initialization(self, get_vector_tiles_gpkg):
        # create the style writer, should create the two tables
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path):
            pass

        with get_database_connection(file_path=get_vector_tiles_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            assert table_exists(cursor=cursor,
                                table_name=GEOPACKAGE_STYLESHEETS_TABLE_NAME)

            assert table_exists(cursor=cursor,
                                table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)

    def test_initialization_without_vector_layers(self, make_gpkg):
        table_name = 'table'
        with get_database_connection(file_path=make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            create_table(cursor=cursor,
                         table_name=table_name)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=VectorTilesContentEntry(table_name=table_name,
                                                                                                identifier='hello',
                                                                                                min_x=0,
                                                                                                min_y=2.0,
                                                                                                max_x=3.0,
                                                                                                max_y=4.0,
                                                                                                srs_id=4326))
            db_conn.commit()
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=make_gpkg.file_path):
            assert table_exists(cursor=cursor,
                                table_name=GEOPACKAGE_STYLESHEETS_TABLE_NAME)
            GeoPackageStylesheetsTableAdapter().validate_stylesheets_table(cursor=cursor)

            assert table_exists(cursor=cursor,
                                table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)
            GeoPackageVectorLayersTableAdapter.validate_vector_layers_table(cursor=cursor)

    def test_add_one_style(self, get_vector_tiles_gpkg):
        styles_set = 'my styles set'
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) \
                as gpkg_styles_writer:
            format = StyleFormat.SLD_STYLE
            style_data = get_file_as_binary(get_sld_style_data_path())
            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=format.value,
                                                          style_data=style_data,
                                                          style_set=styles_set)

            vector_style_entries = gpkg_styles_writer.get_all_style_sheets()

            assert len(vector_style_entries) == 1

            style_entry_returned = vector_style_entries[0]

            assert style_entry_returned.style_format == format.value and \
                   style_entry_returned.styles_set == styles_set and \
                   style_entry_returned.title == 'my-style' and \
                   style_entry_returned.stylesheet == style_data

    def test_get_all_styles_for_tile_set(self, get_vector_tiles_gpkg):
        styles_set = 'my styles'

        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) as gpkg_styles_writer:
            assert len(gpkg_styles_writer.get_all_style_sheets()) == 0
            expected_style_sheet_entry = StyleSheetsEntry(styles_set=styles_set,
                                                          style='my style name',
                                                          style_format=StyleFormat.SLD_STYLE.value,
                                                          description='description of style',
                                                          title='my title',
                                                          stylesheet=get_file_as_binary(get_sld_style_data_path()))

            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=expected_style_sheet_entry.style_format,
                                                          style_data=expected_style_sheet_entry.stylesheet,
                                                          description=expected_style_sheet_entry.description,
                                                          title=expected_style_sheet_entry.title,
                                                          style=expected_style_sheet_entry.style,
                                                          style_set=styles_set)

            returned_styles = gpkg_styles_writer.get_all_style_sheets()

            assert len(returned_styles) == 1
            self.assert_stylesheets_equal(expected_stylesheet=expected_style_sheet_entry,
                                          actual_stylesheet=returned_styles[0])

    def test_add_style_to_layer(self, get_vector_tiles_gpkg):
        styles_set = 'my styles'

        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) as gpkg_styles_writer:
            assert len(gpkg_styles_writer.get_all_style_sheets()) == 0
            expected_style_sheet_entry = StyleSheetsEntry(styles_set=styles_set,
                                                          style='my style name',
                                                          style_format=StyleFormat.SLD_STYLE.value,
                                                          description='description of style',
                                                          title='my title',
                                                          stylesheet=get_file_as_binary(get_sld_style_data_path()))

            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=expected_style_sheet_entry.style_format,
                                                          style_data=expected_style_sheet_entry.stylesheet,
                                                          description=expected_style_sheet_entry.description,
                                                          title=expected_style_sheet_entry.title,
                                                          style=expected_style_sheet_entry.style,
                                                          style_set=styles_set)

            returned_styles = gpkg_styles_writer.get_all_style_sheets()

            assert len(returned_styles) == 1
            self.assert_stylesheets_equal(expected_stylesheet=expected_style_sheet_entry,
                                          actual_stylesheet=returned_styles[0])

    def test_add_two_styles_to_layer(self, get_vector_tiles_gpkg):
        styles_set = 'my styles'

        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) as gpkg_styles_writer:
            assert len(gpkg_styles_writer.get_all_style_sheets()) == 0
            expected_style_sheet_entry = StyleSheetsEntry(styles_set=styles_set,
                                                          style='my style name',
                                                          style_format=StyleFormat.SLD_STYLE.value,
                                                          description='description of style',
                                                          title='my title',
                                                          stylesheet=get_file_as_binary(get_sld_style_data_path()))

            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=expected_style_sheet_entry.style_format,
                                                          style_data=expected_style_sheet_entry.stylesheet,
                                                          description=expected_style_sheet_entry.description,
                                                          title=expected_style_sheet_entry.title,
                                                          style=expected_style_sheet_entry.style,
                                                          style_set=styles_set)

            returned_styles = gpkg_styles_writer.get_all_style_sheets()

            assert len(returned_styles) == 1
            self.assert_stylesheets_equal(expected_stylesheet=expected_style_sheet_entry,
                                          actual_stylesheet=returned_styles[0])

            expected_style_sheet_entry_2 = StyleSheetsEntry(styles_set=styles_set,
                                                            style='night',
                                                            style_format=StyleFormat.SLD_STYLE.value,
                                                            description='description of style',
                                                            title='my title',
                                                            stylesheet=get_file_as_binary(get_sld_style_data_path()))

            gpkg_styles_writer.add_new_stylesheet_to_gpkg(style_format=expected_style_sheet_entry_2.style_format,
                                                          style_data=expected_style_sheet_entry_2.stylesheet,
                                                          style=expected_style_sheet_entry_2.style,
                                                          description=expected_style_sheet_entry_2.description,
                                                          title=expected_style_sheet_entry_2.title,
                                                          style_set=styles_set)

            returned_styles_2 = gpkg_styles_writer.get_all_style_sheets()

            assert len(returned_styles_2) == 2

    def test_get_all_vector_layer_entries_for_tileset(self, get_vector_tiles_gpkg):
        with GeoPackageTilesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path,
                                   tile_table_name='diff_table',
                                   srs_id=3857,
                                   srs_bounds=BoundingBox(min_x=-50,
                                                          min_y=-50,
                                                          max_x=50,
                                                          max_y=50)) as gpkg_writer:
            gpkg_writer.add_tile(tile_row=1,
                                 tile_column=2,
                                 tile_zoom=3,
                                 tile_data=test_utility.get_mapbox_vector_tile_data2())

        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) as gpkg_styles_writer:
            returned_layers = gpkg_styles_writer.get_all_vector_layer_entries_for_tileset('my-vector-tiles')
            expected_layer_1 = VectorLayersEntry(table_name='my-vector-tiles',
                                                 name='AgricultureSrf',
                                                 min_zoom=None,
                                                 max_zoom=None,
                                                 description='')
            expected_layer_2 = VectorLayersEntry(table_name='my-vector-tiles',
                                                 name='TransportationGroundCrv',
                                                 min_zoom=None,
                                                 max_zoom=None,
                                                 description='')
            returned_layer_1 = next(layer for layer
                                    in returned_layers
                                    if layer.name == expected_layer_1.name)

            returned_layer_2 = next(layer for layer
                                    in returned_layers
                                    if layer.name == expected_layer_2.name)

            assert len(returned_layers) == 2
            self.assert_vector_layers_equal(expected_layer_1, returned_layer_1)
            self.assert_vector_layers_equal(expected_layer_2, returned_layer_2)

    def test_get_all_vector_layer_entries(self, get_vector_tiles_gpkg):
        with GeoPackageTilesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path,
                                   tile_table_name='diff_table',
                                   srs_id=3857,
                                   srs_bounds=BoundingBox(min_x=-50,
                                                          min_y=-50,
                                                          max_x=50,
                                                          max_y=50)) as gpkg_writer:
            gpkg_writer.add_tile(tile_row=1,
                                 tile_column=2,
                                 tile_zoom=3,
                                 tile_data=test_utility.get_mapbox_vector_tile_data2())
        with GeoPackageVectorTileStylesWriter(gpkg_file_path=get_vector_tiles_gpkg.file_path) as gpkg_styles_writer:
            returned_layers = gpkg_styles_writer.get_all_vector_layer_entries()
            expected_layer_1 = VectorLayersEntry(table_name='my-vector-tiles',
                                                 name='AgricultureSrf',
                                                 min_zoom=None,
                                                 max_zoom=None,
                                                 description='')
            expected_layer_2 = VectorLayersEntry(table_name='my-vector-tiles',
                                                 name='TransportationGroundCrv',
                                                 min_zoom=None,
                                                 max_zoom=None,
                                                 description='')

            expected_layer_3 = VectorLayersEntry(table_name='diff_table',
                                                 name='HydrographyCrv',
                                                 min_zoom=None,
                                                 max_zoom=None,
                                                 description='')

            returned_layer_1 = next(layer for layer
                                    in returned_layers
                                    if layer.name == expected_layer_1.name)

            returned_layer_2 = next(layer for layer
                                    in returned_layers
                                    if layer.name == expected_layer_2.name)

            returned_layer_3 = next(layer for layer
                                    in returned_layers
                                    if layer.name == expected_layer_3.name)
            assert len(returned_layers) == 3
            self.assert_vector_layers_equal(expected_layer_1, returned_layer_1)
            self.assert_vector_layers_equal(expected_layer_2, returned_layer_2)
            self.assert_vector_layers_equal(expected_layer_3, returned_layer_3)

    @staticmethod
    def assert_stylesheets_equal(expected_stylesheet,
                                 actual_stylesheet):
        """

        :param expected_stylesheet:
        :type expected_stylesheet: StyleSheetsEntry
        :param actual_stylesheet:
        :type actual_stylesheet: StyleSheetsEntry
        :return:
        """
        assert expected_stylesheet.style_format == actual_stylesheet.style_format and \
               expected_stylesheet.styles_set == actual_stylesheet.styles_set and \
               expected_stylesheet.title == actual_stylesheet.title and \
               expected_stylesheet.stylesheet == actual_stylesheet.stylesheet and \
               expected_stylesheet.description == actual_stylesheet.description and \
               expected_stylesheet.style == actual_stylesheet.style

    def assert_vector_layers_equal(self, expected_vector_layer, actual_vector_layer):
        """

        :param expected_vector_layer:
        :type expected_vector_layer: VectorLayersEntry
        :param actual_vector_layer:
        :type actual_vector_layer: VectorLayersEntry
        :return:
        """
        assert expected_vector_layer.table_name == actual_vector_layer.table_name and \
               expected_vector_layer.name == actual_vector_layer.name and \
               expected_vector_layer.description == actual_vector_layer.description and \
               expected_vector_layer.max_zoom == actual_vector_layer.max_zoom and \
               expected_vector_layer.min_zoom == actual_vector_layer.min_zoom
