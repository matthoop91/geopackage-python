#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from xml.etree.ElementTree import tostring

from rgi.tools.styled_layer_descriptor.styled_layer_descriptor_parser import ExternalGraphic
from rgi.tools.styled_layer_descriptor.styled_layer_descriptor_parser import StyledLayerDescriptorParser, \
    ONLINE_RESOURCE_TAG, GRAPHIC_FORMAT_TAG, ONLINE_RESOURCE_URL_ATTRIBUTE_TAG


class StyledLayerDescriptorWriter(StyledLayerDescriptorParser):
    """
    Writes/Edits an existing SLD file.
    """

    def __init__(self,
                 sld_file_path):
        """
        Constructor
        :param sld_file_path: the file path to the SLD
        :type sld_file_path: str
        """
        super(StyledLayerDescriptorWriter, self).__init__(sld_file_path=sld_file_path)

    def update_external_graphic(self,
                                current_graphic,
                                updated_graphic):
        """
        Finds all External Graphic elements with the values given in the "current_graphic" and updates them with
        the updated_graphic param's value.

        Note: This does not write to the file! Must call write to file to do so!
        :param current_graphic: the graphic(s) that currently exist in the given sld
        :type current_graphic: ExternalGraphic
        :param updated_graphic: the graphic values that need to be updated to
        :type updated_graphic: ExternalGraphic
        """
        all_external_graphic_elements = self.get_external_graphic_elements()

        # filter out the elements that do not match
        external_graphic_elements_to_update = [element
                                               for element
                                               in all_external_graphic_elements
                                               if
                                               self.get_url_to_graphic_from_external_graphic_element(element).lower() ==
                                               current_graphic.url_to_graphic.lower()
                                               and
                                               self.get_graphic_format_from_external_graphic_element(element).lower() ==
                                               current_graphic.graphic_format.lower()]
        # check to make sure something matches the criteria
        if len(external_graphic_elements_to_update) == 0:
            raise ValueError("No ExternalGraphic Elements exist with the values: url '{url}' and format '{format}'"
                             .format(url=current_graphic.url_to_graphic,
                                     format=current_graphic.graphic_format))

        # update the values
        for existing_graphic in external_graphic_elements_to_update:
            # update the url
            online_resource_element = existing_graphic.find(ONLINE_RESOURCE_TAG)
            online_resource_element.set(ONLINE_RESOURCE_URL_ATTRIBUTE_TAG, updated_graphic.url_to_graphic)
            # update the graphic format
            graphic_format_element = existing_graphic.find(GRAPHIC_FORMAT_TAG)
            graphic_format_element.text = updated_graphic.graphic_format

    def get_sld_as_string(self):
        """
        Returns the current SLD xml as a string (with the any modifcations made).
        :rtype: str
        """
        return tostring(self.root,
                        encoding=self.tree.docinfo.encoding,
                        method='xml')

    def write_changes_to_file(self):
        """
        Writes the SLD xml to the current file with any edits that have been made.
        """
        self.tree.write(file=self.sld_file_path,
                        inclusive_ns_prefixes=self.root.nsmap,
                        encoding=self.tree.docinfo.encoding)

    def write_changes_to_new_file(self,
                                  new_file_path):
        """
        Writes the SLD xml to the new file path given with any edits that have been made.

        :param new_file_path: the file path to where the writes should be placed
        :type new_file_path: str
        """
        self.tree.write(file=new_file_path,
                        inclusive_ns_prefixes=self.root.nsmap,
                        encoding=self.tree.docinfo.encoding)
