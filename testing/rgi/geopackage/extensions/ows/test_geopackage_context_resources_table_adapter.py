

from rgi.geopackage.extensions.ows.geopackage_context_resource_table_adapter import GeoPackageContextResourceTableAdapter
from rgi.geopackage.extensions.ows.context_resource_entry import ContextResourceEntry
from rgi.geopackage.utility.sql_utility import table_exists, get_database_connection, column_exists
from datetime import datetime

CONTEXT_RESOURCES_TABLE_NAME = "gpkgext_context_resources"

TEST_CONTEXT_RESOURCE_ID = 11
TEST_CONTEXT_ID = 50

DATETIME_FORMAT = "%Y%m-%dT%H:%M:%fZ"

#Format the dates as they're created. They're easier to wOrk with later.
DEFAULT_MIN_TIME = datetime.now().strftime(DATETIME_FORMAT)
DEFAULT_MAX_TIME = datetime.now().strftime(DATETIME_FORMAT)
DEFAULT_LAST_CHANGED_DATE = datetime.now().strftime(DATETIME_FORMAT)

# noinspection PyClassHasNoInit
class TestGeoPackageContextResourcesTableAdapter:

    def test_create_context_resource_table(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

        GeoPackageContextResourceTableAdapter.create_context_resource_table(cursor)

        assert table_exists(cursor=cursor, table_name=CONTEXT_RESOURCES_TABLE_NAME)
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "id")
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "context_id")
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "title")
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "code")
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "abstract")
        assert column_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME, "author")

    def test_insert_or_update_context_resource(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            if not table_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME):
                GeoPackageContextResourceTableAdapter.create_context_resource_table(cursor)

            # INSERT
            #Create ALL new fields.
            new_context_resource_id = TEST_CONTEXT_RESOURCE_ID
            new_context_id = 4
            new_title = "Some_Random_Title"
            new_code = "Code_13"
            new_abstract = "Abstract 2"
            new_author = "Floyd_Mayweather"
            new_publisher = "Mary_Carome"
            new_rights="Miranda_Rights"
            new_min_x = 2
            new_min_y = 2
            new_max_x = 100.1
            new_max_y = 500.4
            new_description = "No_Description"
            new_keywords = "Cat_In_The_ Hat"
            new_min_scale_denominator = 3.1
            new_max_scale_denominator = 5.5
            new_order = 9
            new_layer_name = "Richard_Kim_Layer"
            new_query = "No_Query"
            new_request_url = "www.easports.com/fifa"
            new_srs_id = 77
            new_active = False

            #Insert ALL those new fields.
            inserted_context_resource_entry = self.create_context_resource_entry()
            inserted_context_resource_entry.title = new_title
            inserted_context_resource_entry.context_resource_id = new_context_resource_id
            inserted_context_resource_entry.context_id = new_context_id
            inserted_context_resource_entry.code = new_code
            inserted_context_resource_entry.abstract = new_abstract
            inserted_context_resource_entry.author = new_author
            inserted_context_resource_entry.publisher = new_publisher
            inserted_context_resource_entry.rights = new_rights
            inserted_context_resource_entry.max_x = new_max_x
            inserted_context_resource_entry.max_y = new_max_y
            inserted_context_resource_entry.min_x = new_min_x
            inserted_context_resource_entry.min_y = new_min_y
            inserted_context_resource_entry.description = new_description
            inserted_context_resource_entry.keywords = new_keywords
            inserted_context_resource_entry.min_scale_denominator = new_min_scale_denominator
            inserted_context_resource_entry.max_scale_denominator = new_max_scale_denominator
            inserted_context_resource_entry.order = new_order
            inserted_context_resource_entry.layer_name = new_layer_name
            inserted_context_resource_entry.query = new_query
            inserted_context_resource_entry.request_url = new_request_url
            inserted_context_resource_entry.srs_id = new_srs_id
            inserted_context_resource_entry.active = new_active
            #FORMAT the dates as you insert them.
            inserted_context_resource_entry.max_time = DEFAULT_MAX_TIME
            inserted_context_resource_entry.min_time = DEFAULT_MIN_TIME

            #Insert the new context resource.
            GeoPackageContextResourceTableAdapter.insert_or_update_context_resource(cursor, inserted_context_resource_entry)

            retrieved_context_resource_entry = GeoPackageContextResourceTableAdapter.get_context_resource_entry(cursor, TEST_CONTEXT_RESOURCE_ID)

            #Check all the fields.
            assert retrieved_context_resource_entry.title == inserted_context_resource_entry.title
            assert retrieved_context_resource_entry.context_resource_id == inserted_context_resource_entry.context_resource_id
            assert retrieved_context_resource_entry.context_id == inserted_context_resource_entry.context_id
            assert retrieved_context_resource_entry.code == inserted_context_resource_entry.code
            assert retrieved_context_resource_entry.abstract == inserted_context_resource_entry.abstract
            assert retrieved_context_resource_entry.author == inserted_context_resource_entry.author
            assert retrieved_context_resource_entry.publisher == inserted_context_resource_entry.publisher
            assert retrieved_context_resource_entry.rights == inserted_context_resource_entry.rights
            assert retrieved_context_resource_entry.max_x == inserted_context_resource_entry.max_x
            assert retrieved_context_resource_entry.max_y == inserted_context_resource_entry.max_y
            assert retrieved_context_resource_entry.min_x == inserted_context_resource_entry.min_x
            assert retrieved_context_resource_entry.min_y == inserted_context_resource_entry.min_y
            assert retrieved_context_resource_entry.description == inserted_context_resource_entry.description
            assert retrieved_context_resource_entry.keywords == inserted_context_resource_entry.keywords
            assert retrieved_context_resource_entry.min_scale_denominator == inserted_context_resource_entry.min_scale_denominator
            assert retrieved_context_resource_entry.max_scale_denominator == inserted_context_resource_entry.max_scale_denominator
            assert retrieved_context_resource_entry.order == inserted_context_resource_entry.order
            assert retrieved_context_resource_entry.layer_name == inserted_context_resource_entry.layer_name
            assert retrieved_context_resource_entry.query == inserted_context_resource_entry.query
            assert retrieved_context_resource_entry.request_url == inserted_context_resource_entry.request_url
            assert retrieved_context_resource_entry.srs_id == inserted_context_resource_entry.srs_id
            assert retrieved_context_resource_entry.active == inserted_context_resource_entry.active

            # Compare datetime object values. Make sure they have the same format.
            assert retrieved_context_resource_entry.max_time == inserted_context_resource_entry.max_time
            assert retrieved_context_resource_entry.min_time == inserted_context_resource_entry.min_time

            # UPDATE
            # Create all the fields to update
            updated_context_resource_id = TEST_CONTEXT_RESOURCE_ID
            updated_context_id = 36
            updated_title = "Yet_Another_Title"
            updated_code = "Code_44"
            updated_abstract = "Abstract 6"
            updated_author = "Optimus_Prime"
            updated_publisher = "Some_Publisher"
            updated_rights = "Elevated_Privileges"
            updated_min_x = 3
            updated_min_y = 5
            updated_max_x = 200
            updated_max_y = 600
            updated_description = "Another_Description"
            updated_keywords = "Beast"
            updated_min_scale_denominator = 22.2
            updated_max_scale_denominator = 34.5
            updated_order = 55
            updated_layer_name = "Top_Layer"
            updated_query = "SQL_Query"
            updated_request_url = "www.spotify.com/"
            updated_srs_id = 21
            updated_active = True

            # Insert ALL those new fields.
            updated_context_resource_entry = self.create_context_resource_entry()
            updated_context_resource_entry.title = updated_title
            updated_context_resource_entry.context_id = updated_context_id
            updated_context_resource_entry.code = updated_code
            updated_context_resource_entry.abstract = updated_abstract
            updated_context_resource_entry.author = updated_author
            updated_context_resource_entry.publisher = updated_publisher
            updated_context_resource_entry.rights = updated_rights
            updated_context_resource_entry.max_x = updated_max_x
            updated_context_resource_entry.max_y = updated_max_y
            updated_context_resource_entry.min_x = updated_min_x
            updated_context_resource_entry.min_y = updated_min_y
            updated_context_resource_entry.description = updated_description
            updated_context_resource_entry.keywords = updated_keywords
            updated_context_resource_entry.min_scale_denominator = updated_min_scale_denominator
            updated_context_resource_entry.max_scale_denominator = updated_max_scale_denominator
            updated_context_resource_entry.order = updated_order
            updated_context_resource_entry.layer_name = updated_layer_name
            updated_context_resource_entry.query = updated_query
            updated_context_resource_entry.request_url = updated_request_url
            updated_context_resource_entry.srs_id = updated_srs_id
            updated_context_resource_entry.active = updated_active
            # FORMAT the dates as you insert them.
            updated_context_resource_entry.max_time = DEFAULT_MAX_TIME
            updated_context_resource_entry.min_time = DEFAULT_MIN_TIME

            # Insert the new context entry.
            GeoPackageContextResourceTableAdapter.insert_or_update_context_resource(cursor, updated_context_resource_entry)

            retrieved_updated_context_resource_entry = GeoPackageContextResourceTableAdapter.get_context_resource_entry(cursor=cursor,
                                                                                                                        context_resource_id=updated_context_resource_id)

            # Check ALL the new fields.
            assert retrieved_updated_context_resource_entry.context_id == updated_context_resource_entry.context_id
            assert retrieved_updated_context_resource_entry.title == updated_context_resource_entry.title
            assert retrieved_updated_context_resource_entry.code == updated_context_resource_entry.code
            assert retrieved_updated_context_resource_entry.abstract == updated_context_resource_entry.abstract
            assert retrieved_updated_context_resource_entry.author == updated_context_resource_entry.author
            assert retrieved_updated_context_resource_entry.publisher == updated_context_resource_entry.publisher
            assert retrieved_updated_context_resource_entry.rights == updated_context_resource_entry.rights
            assert retrieved_updated_context_resource_entry.min_x == updated_context_resource_entry.min_x
            assert retrieved_updated_context_resource_entry.min_y == updated_context_resource_entry.min_y
            assert retrieved_updated_context_resource_entry.max_x == updated_context_resource_entry.max_x
            assert retrieved_updated_context_resource_entry.max_y == updated_context_resource_entry.max_y
            assert retrieved_updated_context_resource_entry.description == updated_context_resource_entry.description
            assert retrieved_updated_context_resource_entry.keywords == updated_context_resource_entry.keywords
            assert retrieved_updated_context_resource_entry.min_scale_denominator == updated_context_resource_entry.min_scale_denominator
            assert retrieved_updated_context_resource_entry.max_scale_denominator == updated_context_resource_entry.max_scale_denominator
            assert retrieved_updated_context_resource_entry.order == updated_context_resource_entry.order
            assert retrieved_updated_context_resource_entry.layer_name == updated_context_resource_entry.layer_name
            assert retrieved_updated_context_resource_entry.query == updated_context_resource_entry.query
            assert retrieved_updated_context_resource_entry.keywords == updated_context_resource_entry.keywords
            assert retrieved_updated_context_resource_entry.request_url == updated_context_resource_entry.request_url
            assert retrieved_updated_context_resource_entry.srs_id == updated_context_resource_entry.srs_id
            assert retrieved_updated_context_resource_entry.active == updated_context_resource_entry.active
            # FORMAT the dates as you pull them out.
            datetime.strptime(retrieved_updated_context_resource_entry.max_time, DATETIME_FORMAT)
            datetime.strptime(retrieved_updated_context_resource_entry.min_time, DATETIME_FORMAT)

            # Compare datetime object values. Make sure they have the same format.
            assert retrieved_updated_context_resource_entry.max_time == updated_context_resource_entry.max_time
            assert retrieved_updated_context_resource_entry.min_time == updated_context_resource_entry.min_time

    def test_get_context_resource(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            GeoPackageContextResourceTableAdapter.create_context_resource_table(cursor)
            assert table_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME)
            inserted_context_resource_entry = self.create_context_resource_entry()
            GeoPackageContextResourceTableAdapter.insert_or_update_context_resource(cursor, inserted_context_resource_entry)

            retrieved_context_resource_entry = GeoPackageContextResourceTableAdapter.get_context_resource_entry(cursor, TEST_CONTEXT_RESOURCE_ID)

            assert retrieved_context_resource_entry.context_resource_id == inserted_context_resource_entry.context_resource_id
            assert retrieved_context_resource_entry.context_id == inserted_context_resource_entry.context_id
            assert retrieved_context_resource_entry.title == inserted_context_resource_entry.title
            assert retrieved_context_resource_entry.abstract == inserted_context_resource_entry.abstract
            assert retrieved_context_resource_entry.author == inserted_context_resource_entry.author
            assert retrieved_context_resource_entry.publisher == inserted_context_resource_entry.publisher
            assert retrieved_context_resource_entry.rights == inserted_context_resource_entry.rights
            assert retrieved_context_resource_entry.min_x == inserted_context_resource_entry.min_x
            assert retrieved_context_resource_entry.min_y == inserted_context_resource_entry.min_y
            assert retrieved_context_resource_entry.max_x == inserted_context_resource_entry.max_x
            assert retrieved_context_resource_entry.max_y == inserted_context_resource_entry.max_y
            assert retrieved_context_resource_entry.srs_id == inserted_context_resource_entry.srs_id
            assert retrieved_context_resource_entry.description == inserted_context_resource_entry.description
            assert retrieved_context_resource_entry.active == inserted_context_resource_entry.active
            assert retrieved_context_resource_entry.keywords == inserted_context_resource_entry.keywords
            assert retrieved_context_resource_entry.min_scale_denominator == inserted_context_resource_entry.min_scale_denominator
            assert retrieved_context_resource_entry.max_scale_denominator == inserted_context_resource_entry.max_scale_denominator
            assert retrieved_context_resource_entry.order == inserted_context_resource_entry.order
            assert retrieved_context_resource_entry.request_url == inserted_context_resource_entry.request_url
            assert retrieved_context_resource_entry.layer_name == inserted_context_resource_entry.layer_name
            assert retrieved_context_resource_entry.query == inserted_context_resource_entry.query

    def test_get_all_context_resources(self, make_gpkg):
        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            if not table_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME):
                GeoPackageContextResourceTableAdapter.create_context_resource_table(cursor)
                assert table_exists(cursor, CONTEXT_RESOURCES_TABLE_NAME)

            context_resource_record_count = 12
            inserted_context_resource_entries = []

            for x in range(context_resource_record_count):
                inserted_context_resource = self.create_context_resource_entry()
                inserted_context_resource.context_resource_id = x
                inserted_context_resource.layer_name = "Layer " + str(x)
                inserted_context_resource.keywords = "Keyword " + str(x)
                inserted_context_resource.description = "Description " + str(x)

                GeoPackageContextResourceTableAdapter.insert_or_update_context_resource(cursor, inserted_context_resource)
                inserted_context_resource_entries.append(inserted_context_resource)

            retrieved_context_resource_entries = GeoPackageContextResourceTableAdapter.get_all_context_resource_entries(cursor)
            assert len(retrieved_context_resource_entries) == context_resource_record_count  # 12 records

            #We're sorting complex objects by that object's id.
            retrieved_context_resource_entries.sort(key=lambda j: j.context_resource_id, reverse=False)
            inserted_context_resource_entries.sort(key=lambda k: k.context_resource_id, reverse=False)

            for y in range(len(retrieved_context_resource_entries)):
                assert retrieved_context_resource_entries[y].context_resource_id == inserted_context_resource_entries[y].context_resource_id
                assert retrieved_context_resource_entries[y].context_id == inserted_context_resource_entries[y].context_id
                assert retrieved_context_resource_entries[y].title == inserted_context_resource_entries[y].title
                assert retrieved_context_resource_entries[y].abstract == inserted_context_resource_entries[y].abstract
                assert retrieved_context_resource_entries[y].author == inserted_context_resource_entries[y].author
                assert retrieved_context_resource_entries[y].publisher == inserted_context_resource_entries[y].publisher
                assert retrieved_context_resource_entries[y].rights == inserted_context_resource_entries[
                    y].rights
                assert retrieved_context_resource_entries[y].min_x == inserted_context_resource_entries[
                    y].min_x
                assert retrieved_context_resource_entries[y].min_y == inserted_context_resource_entries[
                    y].min_y
                assert retrieved_context_resource_entries[y].max_x == inserted_context_resource_entries[
                    y].max_x
                assert retrieved_context_resource_entries[y].max_y == inserted_context_resource_entries[
                    y].max_y
                assert retrieved_context_resource_entries[y].srs_id == inserted_context_resource_entries[
                    y].srs_id
                assert retrieved_context_resource_entries[y].description == inserted_context_resource_entries[
                    y].description
                assert retrieved_context_resource_entries[y].active == inserted_context_resource_entries[y].active
                assert retrieved_context_resource_entries[y].keywords == inserted_context_resource_entries[y].keywords
                assert retrieved_context_resource_entries[y].min_scale_denominator == inserted_context_resource_entries[y].min_scale_denominator
                assert retrieved_context_resource_entries[y].max_scale_denominator == inserted_context_resource_entries[y].max_scale_denominator
                assert retrieved_context_resource_entries[y].order == inserted_context_resource_entries[y].order
                assert retrieved_context_resource_entries[y].request_url == inserted_context_resource_entries[y].request_url
                assert retrieved_context_resource_entries[y].layer_name == inserted_context_resource_entries[y].layer_name
                assert retrieved_context_resource_entries[y].query == inserted_context_resource_entries[y].query

    def create_context_resource_entry(self):

        context_resources_entry = ContextResourceEntry(context_resource_id=TEST_CONTEXT_RESOURCE_ID,
                                                       context_id=TEST_CONTEXT_ID,
                                                       title="Context_Resource_Number_1",
                                                       code="Code for Context Resource",
                                                       abstract="Test Abstract",
                                                       author="Frederick Boyd Jr.",
                                                       publisher="Mary Carome",
                                                       rights="Rights",
                                                       min_x=0.0,
                                                       min_y=0.0,
                                                       max_x=1000.0,
                                                       max_y=1000.0,
                                                       srs_id=22,
                                                       description="Description",
                                                       active=True,
                                                       keywords="dog,cat,orange",
                                                       min_scale_denominator=0.0,
                                                       max_scale_denominator=9.9,
                                                       order=3.0,
                                                       request_url="www.techcrunch.com",
                                                       layer_name="layer_2",
                                                       query='',
                                                       min_time=DEFAULT_MIN_TIME,
                                                       max_time=DEFAULT_MAX_TIME
                                                       )
        return context_resources_entry

    @staticmethod
    def are_equal(self, given_entry, expected_entry):

        if given_entry == expected_entry:
            are_equal = True
        else:
            are_equal = False

        return are_equal
