#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""


class ContextOfferingEntry(object):
    """
    Represents a row in the gpkgext_context_offerings table.
    """

    def __init__(self,
                 context_offering_id,
                 context_offering_resource_id,
                 method,
                 mime_type,
                 code,
                 stylesheet_id,
                 contents
                 ):

        """
        Constructor

        :param context_offering_id: The unique identifier for a GeoPackage context offering entry.
        :type context_offering_id: int

        :param context_offering_resource_id: id from gpkgext_context_resources
        :type context_offering_resource_id: int

        :param method: Name of operation method request
        :type method: str

        :param mime_type: MIME type of the return result
        :type mime_type: str

        :param code: Code of the identifying type of offering
        :type code: str

        :param stylesheet_id: id from gpkgext_stylesheets
        :type stylesheet_id: int

        :param contents: Actual data (for inline data)
        :type contents: Blob


        """

        self.context_offering_id = context_offering_id
        self.context_offering_resource_id = context_offering_resource_id
        self.code = code
        self.stylesheet_id = stylesheet_id
        self.contents = contents
        self.method = method
        self.mime_type = mime_type



