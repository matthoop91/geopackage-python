#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
import math
from os import remove
from os.path import join, exists
from sqlite3 import Binary
from tempfile import gettempdir
from uuid import uuid4

import pytest
from pytest import raises

from rgi.geopackage.common.bounding_box import BoundingBox
from rgi.geopackage.common.tile_coordinate import TileCoordinate
from rgi.geopackage.common.zoom_times_two import ZoomTimesTwo
from rgi.geopackage.core.content_entry import ContentEntry
from rgi.geopackage.core.geopackage_core_table_adapter import GEOPACKAGE_CONTENTS_TABLE_NAME, \
    GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME, GeoPackageCoreTableAdapter
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter, \
    GEOPACKAGE_EXTENSIONS_TABLE_NAME
from rgi.geopackage.extensions.vector_tiles.geopackage_geojson_vector_tiles_table_adapter import \
    GeoPackageGeoJSONVectorTilesTableAdapter
from rgi.geopackage.extensions.vector_tiles.geopackage_mapbox_vector_tiles_table_adapter import \
    GeoPackageMapBoxVectorTilesTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.geopackage_vector_fields_table_adapter import \
    GeoPackageVectorFieldsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_fields.vector_fields_entry import VectorFieldType
from rgi.geopackage.extensions.vector_tiles.vector_layers.geopackage_vector_layers_table_adapter import \
    GeoPackageVectorLayersTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_tiles_constants import GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME, \
    GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME, VECTOR_TILES_DATA_TYPE
from rgi.geopackage.writers.geopackage_tiles_writer import GeoPackageTilesWriter, TileType, WritingMode
from rgi.geopackage.srs.ellipsoidal_mercator import EllipsoidalMercator
from rgi.geopackage.tiles.geopackage_abstract_tiles_table_adapter import GEOPACKAGE_TILE_MATRIX_TABLE_NAME, \
    GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME
from rgi.geopackage.tiles.geopackage_tiles_table_adapter import GeoPackageTilesTableAdapter
from rgi.geopackage.tiles.tile_data_information import TileDataInformation
from rgi.geopackage.tiles.tiles_content_entry import TILES_DATA_TYPE
from rgi.geopackage.utility.sql_utility import get_database_connection, table_exists
from rgi.geopackage.writers.geopackage_writer import APPLICATION_ID, USER_VERSION
from testing.rgi import test_utility
from testing.rgi.test_utility import make_tiles, get_raster_data, create_table, ExThread, TEST_GEOJSON, TEST_GEOJSON_2


class TestGeoPackageWriter(object):

    def test_application_id_and_user_version(self, get_gpkg_file_path):
        GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                              tile_table_name="tiles table",
                              srs_id=4326,
                              srs_bounds=BoundingBox(min_x=-180.0,
                                                     max_x=180.0,
                                                     min_y=-90.0,
                                                     max_y=90.0),
                              tile_scheme=ZoomTimesTwo(base_matrix_width=2, base_matrix_height=1))
        # check if user version and application id are set
        with get_database_connection(get_gpkg_file_path) as db_conn:
            # Check the application id
            cursor = db_conn.cursor()
            cursor.execute("PRAGMA application_id;")
            application_id_row = cursor.fetchone()
            application_id = application_id_row["application_id"]
            assert application_id == APPLICATION_ID

            # check the user version
            cursor.execute("PRAGMA user_version;")
            user_version_row = cursor.fetchone()
            user_version = user_version_row["user_version"]
            assert user_version == USER_VERSION

    def test_mapbox(self, get_gpkg_file_path):
        tiles_table_name = "My tiles baby"
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name=tiles_table_name,
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(min_x=-180.0,
                                                                   max_x=180.0,
                                                                   min_y=-90.0,
                                                                   max_y=90.0),
                                            tile_scheme=ZoomTimesTwo(base_matrix_width=2, base_matrix_height=1))
        tile_row_expected = 1
        tile_column_expected = 2
        tile_zoom_expected = 3
        gpkg_writer.add_tile(tile_row=tile_row_expected,
                             tile_column=tile_column_expected,
                             tile_zoom=tile_zoom_expected,
                             tile_data=test_utility.get_mapbox_vector_tile_data())

        TestGeoPackageWriter.assert_all_vector_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_extension=GeoPackageMapBoxVectorTilesTableAdapter(
                                                                vector_tiles_table_name=tiles_table_name))

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                      table_name=tiles_table_name,
                                                                      tile_row=tile_row_expected,
                                                                      tile_column=tile_column_expected,
                                                                      zoom_level=tile_zoom_expected)
            assert tile_returned == test_utility.get_mapbox_vector_tile_data()
            # assert fields and layers were added properly

            # check if the mapbox layers and field entries were extracted and added to the layers table
            vector_layer_entries = GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name(
                cursor=db_conn.cursor(),
                vector_tiles_table_name
                =tiles_table_name)

            assert len(vector_layer_entries) == 2

            assert any(vector_layer_entry.table_name == tiles_table_name and
                       vector_layer_entry.name == 'AgricultureSrf'
                       for vector_layer_entry
                       in vector_layer_entries)

            assert any(vector_layer_entry.table_name == tiles_table_name and
                       vector_layer_entry.name == 'TransportationGroundCrv'
                       for vector_layer_entry
                       in vector_layer_entries)

            # check the field entries too
            vector_field_entries = GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(
                cursor=db_conn.cursor(),
                id=None,
                name='Feature ID',
                type=VectorFieldType.NUMBER,
                layer_id=None)

            assert len(vector_field_entries) == 2
            # assert the tile matrix entry was added
            tile_matrix_returned = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                              tile_table_name=tiles_table_name,
                                                                                              zoom_level=tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned) == 1
            assert tile_matrix_returned[0].table_name == tiles_table_name and \
                   tile_matrix_returned[0].matrix_height == 8 and \
                   tile_matrix_returned[0].matrix_width == 16 and \
                   tile_matrix_returned[0].zoom_level == tile_zoom_expected and \
                   tile_matrix_returned[0].pixel_x_size == 0.0878906250 and \
                   tile_matrix_returned[0].pixel_y_size == 0.0878906250 and \
                   tile_matrix_returned[0].tile_height == 256 and \
                   tile_matrix_returned[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == 4326 and \
                   tile_matrix_set_returned.min_x == -180.0 and \
                   tile_matrix_set_returned.min_y == -90.0 and \
                   tile_matrix_set_returned.max_x == 180.0 and \
                   tile_matrix_set_returned.max_y == 90.0 and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == 4326 and \
                   content.min_x == -180.0 and \
                   content.min_y == -90.0 and \
                   content.max_x == 180.0 and \
                   content.max_y == 90.0 and \
                   content.data_type == VECTOR_TILES_DATA_TYPE

    def test_geojson(self, get_gpkg_file_path):
        tiles_table_name = "My vector tiles baby"
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name=tiles_table_name,
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(min_x=-180.0,
                                                                   max_x=180.0,
                                                                   min_y=-90.0,
                                                                   max_y=90.0),
                                            tile_scheme=ZoomTimesTwo(base_matrix_width=2, base_matrix_height=1))
        tile_row_expected = 1
        tile_column_expected = 2
        tile_zoom_expected = 3
        gpkg_writer.add_tile(tile_row=tile_row_expected,
                             tile_column=tile_column_expected,
                             tile_zoom=tile_zoom_expected,
                             tile_data=TEST_GEOJSON_2)

        TestGeoPackageWriter.assert_all_vector_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_extension=GeoPackageGeoJSONVectorTilesTableAdapter(
                                                                vector_tiles_table_name=tiles_table_name))

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                      table_name=tiles_table_name,
                                                                      tile_row=tile_row_expected,
                                                                      tile_column=tile_column_expected,
                                                                      zoom_level=tile_zoom_expected)
            assert tile_returned == TEST_GEOJSON_2
            # assert fields and layers were added properly

            # check if the GeoJSON layers and field entries were extracted and added to the layers table
            vector_layer_entries = GeoPackageVectorLayersTableAdapter.get_vector_layer_entries_by_table_name(
                cursor=db_conn.cursor(),
                vector_tiles_table_name
                =tiles_table_name)

            assert len(vector_layer_entries) == 1

            assert vector_layer_entries[0].table_name == tiles_table_name and \
                   vector_layer_entries[0].name == tiles_table_name and \
                   vector_layer_entries[0].identifier == 1

            vector_fields = GeoPackageVectorFieldsTableAdapter.get_vector_field_entry_by_values(cursor=db_conn.cursor(),
                                                                                                layer_id=
                                                                                                vector_layer_entries[
                                                                                                    0].identifier)

            # Check the vector field entries to ensure they are the values that were expected
            assert len(vector_fields) == 5

            vector_field_prop_1 = next((vector_field for vector_field in vector_fields if vector_field.name == "prop1"),
                                       None)
            vector_field_prop_2 = next((vector_field for vector_field in vector_fields if vector_field.name == "prop2"),
                                       None)
            vector_field_prop_3 = next((vector_field for vector_field in vector_fields if vector_field.name == "prop3"),
                                       None)

            vector_field_prop_4 = next((vector_field for vector_field in vector_fields if vector_field.name == "prop4"),
                                       None)
            vector_field_prop_5 = next((vector_field for vector_field in vector_fields if vector_field.name == "prop5"),
                                       None)

            assert vector_field_prop_1 is not None and \
                   vector_field_prop_1.type == VectorFieldType.STRING and \
                   vector_field_prop_1.layer_id == vector_layer_entries[0].identifier

            assert vector_field_prop_2 is not None and \
                   vector_field_prop_2.type == VectorFieldType.STRING and \
                   vector_field_prop_2.layer_id == vector_layer_entries[0].identifier

            assert vector_field_prop_3 is not None and \
                   vector_field_prop_3.type == VectorFieldType.NUMBER and \
                   vector_field_prop_3.layer_id == vector_layer_entries[0].identifier

            assert vector_field_prop_4 is not None and \
                   vector_field_prop_4.type == VectorFieldType.STRING and \
                   vector_field_prop_4.layer_id == vector_layer_entries[0].identifier

            assert vector_field_prop_5 is not None and \
                   vector_field_prop_5.type == VectorFieldType.NUMBER and \
                   vector_field_prop_5.layer_id == vector_layer_entries[0].identifier

            # assert the tile matrix entry was added
            tile_matrix_returned = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                              tile_table_name=tiles_table_name,
                                                                                              zoom_level=tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned) == 1
            assert tile_matrix_returned[0].table_name == tiles_table_name and \
                   tile_matrix_returned[0].matrix_height == 8 and \
                   tile_matrix_returned[0].matrix_width == 16 and \
                   tile_matrix_returned[0].zoom_level == tile_zoom_expected and \
                   tile_matrix_returned[0].pixel_x_size == 0.0878906250 and \
                   tile_matrix_returned[0].pixel_y_size == 0.0878906250 and \
                   tile_matrix_returned[0].tile_height == 256 and \
                   tile_matrix_returned[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == 4326 and \
                   tile_matrix_set_returned.min_x == -180.0 and \
                   tile_matrix_set_returned.min_y == -90.0 and \
                   tile_matrix_set_returned.max_x == 180.0 and \
                   tile_matrix_set_returned.max_y == 90.0 and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == 4326 and \
                   content.min_x == -180.0 and \
                   content.min_y == -90.0 and \
                   content.max_x == 180.0 and \
                   content.max_y == 90.0 and \
                   content.data_type == VECTOR_TILES_DATA_TYPE

    def test_jpeg(self, get_gpkg_file_path):
        data = get_raster_data("jpeg")

        tile_type = GeoPackageTilesWriter.get_tile_type_from_data(tile_data=data)

        assert tile_type == TileType.JPEG

        tiles_table_name = "My raster tiles baby"
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name=tiles_table_name,
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(min_x=-180.0,
                                                                   max_x=180.0,
                                                                   min_y=-90.0,
                                                                   max_y=90.0))

        tile_row_expected = 2
        tile_column_expected = 3
        tile_zoom_expected = 4
        gpkg_writer.add_tile(tile_row=tile_row_expected,
                             tile_column=tile_column_expected,
                             tile_zoom=tile_zoom_expected,
                             tile_data=Binary(data))

        TestGeoPackageWriter.assert_all_raster_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_name=tiles_table_name)

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                      table_name=tiles_table_name,
                                                                      tile_row=tile_row_expected,
                                                                      tile_column=tile_column_expected,
                                                                      zoom_level=tile_zoom_expected)
            assert tile_returned == Binary(data)

            # assert the tile matrix entry was added
            tile_matrix_returned = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                              tile_table_name=tiles_table_name,
                                                                                              zoom_level=tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned) == 1
            assert tile_matrix_returned[0].table_name == tiles_table_name and \
                   tile_matrix_returned[0].matrix_height == 16 and \
                   tile_matrix_returned[0].matrix_width == 16 and \
                   tile_matrix_returned[0].zoom_level == tile_zoom_expected and \
                   tile_matrix_returned[0].pixel_x_size == 0.0878906250 and \
                   tile_matrix_returned[0].pixel_y_size == 0.0439453125 and \
                   tile_matrix_returned[0].tile_height == 256 and \
                   tile_matrix_returned[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == 4326 and \
                   tile_matrix_set_returned.min_x == -180.0 and \
                   tile_matrix_set_returned.min_y == -90.0 and \
                   tile_matrix_set_returned.max_x == 180.0 and \
                   tile_matrix_set_returned.max_y == 90.0 and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == 4326 and \
                   content.min_x == -180.0 and \
                   content.min_y == -90.0 and \
                   content.max_x == 180.0 and \
                   content.max_y == 90.0 and \
                   content.data_type == "tiles"

    def test_png(self, get_gpkg_file_path):
        data = get_raster_data("png")

        tile_type = GeoPackageTilesWriter.get_tile_type_from_data(tile_data=data)

        assert tile_type == TileType.PNG

        tiles_table_name = "My raster tiles baby PNG"
        srs_bounds = BoundingBox(min_x=-20037508.342789244,
                                 max_x=20037508.342789244,
                                 min_y=-20037508.342789244,
                                 max_y=20037508.342789244)

        with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                   tile_table_name=tiles_table_name,
                                   srs_id=3395,
                                   srs_bounds=srs_bounds) as gpkg_writer:
            tile_row_expected = 2
            tile_column_expected = 3
            tile_zoom_expected = 2
            gpkg_writer.add_tile(tile_row=tile_row_expected,
                                 tile_column=tile_column_expected,
                                 tile_zoom=tile_zoom_expected,
                                 tile_data=Binary(data))

        TestGeoPackageWriter.assert_all_raster_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_name=tiles_table_name)

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                      table_name=tiles_table_name,
                                                                      tile_row=tile_row_expected,
                                                                      tile_column=tile_column_expected,
                                                                      zoom_level=tile_zoom_expected)
            assert tile_returned == Binary(data)

            # assert the tile matrix entry was added
            tile_matrix_returned = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                              tile_table_name=tiles_table_name,
                                                                                              zoom_level=tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned) == 1
            assert tile_matrix_returned[0].table_name == tiles_table_name and \
                   tile_matrix_returned[0].matrix_height == 4 and \
                   tile_matrix_returned[0].matrix_width == 4 and \
                   tile_matrix_returned[0].zoom_level == tile_zoom_expected and \
                   tile_matrix_returned[0].pixel_x_size == 39135.7584820102421875 and \
                   tile_matrix_returned[0].pixel_y_size == 39135.7584820102421875 and \
                   tile_matrix_returned[0].tile_height == 256 and \
                   tile_matrix_returned[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == 3395 and \
                   tile_matrix_set_returned.min_x == srs_bounds.min_x and \
                   tile_matrix_set_returned.min_y == srs_bounds.min_y and \
                   tile_matrix_set_returned.max_x == srs_bounds.max_x and \
                   tile_matrix_set_returned.max_y == srs_bounds.max_y and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == 3395 and \
                   content.min_x == srs_bounds.min_x and \
                   content.min_y == srs_bounds.min_y and \
                   content.max_x == srs_bounds.max_x and \
                   content.max_y == srs_bounds.max_y and \
                   content.data_type == "tiles"

    def test_invalid_gpkg_path(self):
        with raises(ValueError):
            GeoPackageTilesWriter(gpkg_file_path=gettempdir(),
                                  tile_table_name="Richard_Kim",
                                  srs_id=1111,
                                  srs_bounds=BoundingBox(0.0, 0.0, 0.0, 0.0))

    def test_invalid_tile_add_zoom_level_value(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=1234,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_row=0,
                                 tile_column=0,
                                 tile_zoom=-9,
                                 tile_data=get_raster_data("jpeg"))

    def test_invalid_tile_add_mix_raster_vector(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=0,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        gpkg_writer.add_tile(tile_row=0,
                             tile_column=0,
                             tile_zoom=0,
                             tile_data=Binary(get_raster_data("jpeg")))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=Binary(test_utility.get_mapbox_vector_tile_data()))

    def test_invalid_tile_add_mix_vector_raster(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=-1,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        gpkg_writer.add_tile(tile_row=0,
                             tile_column=0,
                             tile_zoom=0,
                             tile_data=Binary(test_utility.get_mapbox_vector_tile_data()))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=Binary(get_raster_data("jpeg")))

    def test_invalid_tile_add_mix_vector_mapbox_vector_geojson(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=-1,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        gpkg_writer.add_tile(tile_row=0,
                             tile_column=0,
                             tile_zoom=0,
                             tile_data=Binary(test_utility.get_mapbox_vector_tile_data()))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=TEST_GEOJSON)

    def test_invalid_tile_add_mix_vector_geojson_vector_mapbox(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=-1,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        gpkg_writer.add_tile(tile_row=0,
                             tile_column=0,
                             tile_zoom=0,
                             tile_data=TEST_GEOJSON)

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=Binary(test_utility.get_mapbox_vector_tile_data()))

    def test_invalid_tile_add_bad_data(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=Binary(bytes("bad_data".encode('utf-8'))))

    def test_invalid_tile_raster_mime_type(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=1,
                                 tile_data=get_raster_data("gif"))

    def test_invalid_tile_column_value_negative(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=-1,
                                 tile_data=get_raster_data("jpeg"))

    def test_invalid_tile_column_value_larger_than_matrix_width(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=1,
                                 tile_column=2,
                                 tile_data=get_raster_data("jpeg"))

    def test_invalid_tile_row_value_negative(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=-1,
                                 tile_column=1,
                                 tile_data=get_raster_data("jpeg"))

    def test_invalid_tile_row_value_larger_than_matrix_height(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tile(tile_zoom=1,
                                 tile_row=2,
                                 tile_column=0,
                                 tile_data=get_raster_data("jpeg"))

    def test_adding_mixed_raster_tiles(self, get_gpkg_file_path):
        tiles_table_name = "Pirate_peg_leg"
        srs_id = 3857
        earth_radius_meters = 6378137

        srs_bounds = BoundingBox(min_x=-math.pi * earth_radius_meters,
                                 min_y=-math.pi * earth_radius_meters,
                                 max_x=math.pi * earth_radius_meters,
                                 max_y=math.pi * earth_radius_meters)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=join(get_gpkg_file_path),
                                            tile_table_name=tiles_table_name,
                                            srs_id=srs_id,
                                            srs_bounds=srs_bounds)

        red_tile_data = Binary(get_raster_data("png"))
        blue_tile_data = Binary(get_raster_data("jpeg", "aliceblue"))

        red_tile_row_expected = 2
        red_tile_column_expected = 3
        red_tile_zoom_expected = 6

        blue_tile_row_expected = 3
        blue_tile_column_expected = 4
        blue_tile_zoom_expected = 5
        gpkg_writer.add_tile(tile_row=red_tile_row_expected,
                             tile_column=red_tile_column_expected,
                             tile_zoom=red_tile_zoom_expected,
                             tile_data=red_tile_data)

        gpkg_writer.add_tile(tile_row=blue_tile_row_expected,
                             tile_column=blue_tile_column_expected,
                             tile_zoom=blue_tile_zoom_expected,
                             tile_data=blue_tile_data)

        TestGeoPackageWriter.assert_all_raster_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_name=tiles_table_name)

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            red_tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                          table_name=tiles_table_name,
                                                                          tile_row=red_tile_row_expected,
                                                                          tile_column=red_tile_column_expected,
                                                                          zoom_level=red_tile_zoom_expected)
            assert red_tile_returned == Binary(red_tile_data)

            blue_tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                           table_name=tiles_table_name,
                                                                           tile_row=blue_tile_row_expected,
                                                                           tile_column=blue_tile_column_expected,
                                                                           zoom_level=blue_tile_zoom_expected)
            assert blue_tile_returned == Binary(blue_tile_data)

            # assert the tile matrix entry was added
            tile_matrix_returned_red = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=red_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_red) == 1
            assert tile_matrix_returned_red[0].table_name == tiles_table_name and \
                   tile_matrix_returned_red[0].matrix_height == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].matrix_width == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].zoom_level == red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].pixel_x_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].pixel_y_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].tile_height == 256 and \
                   tile_matrix_returned_red[0].tile_width == 256

            # assert the tile matrix entry was added
            tile_matrix_returned_blue = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=blue_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_blue) == 1
            assert tile_matrix_returned_blue[0].table_name == tiles_table_name and \
                   tile_matrix_returned_blue[0].matrix_height == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].matrix_width == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].zoom_level == blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].pixel_x_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].pixel_y_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].tile_height == 256 and \
                   tile_matrix_returned_blue[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == srs_id and \
                   tile_matrix_set_returned.min_x == srs_bounds.min_x and \
                   tile_matrix_set_returned.min_y == srs_bounds.min_y and \
                   tile_matrix_set_returned.max_x == srs_bounds.max_x and \
                   tile_matrix_set_returned.max_y == srs_bounds.max_y and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == srs_id and \
                   content.min_x == srs_bounds.min_x and \
                   content.min_y == srs_bounds.min_y and \
                   content.max_x == srs_bounds.max_x and \
                   content.max_y == srs_bounds.max_y and \
                   content.data_type == "tiles"

    def test_adding_tiles_bulk(self, get_gpkg_file_path):
        tiles_table_name = "Pirate_peg_leg"
        srs_id = 3857
        earth_radius_meters = 6378137

        srs_bounds = BoundingBox(min_x=-math.pi * earth_radius_meters,
                                 min_y=-math.pi * earth_radius_meters,
                                 max_x=math.pi * earth_radius_meters,
                                 max_y=math.pi * earth_radius_meters)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=join(get_gpkg_file_path),
                                            tile_table_name=tiles_table_name,
                                            srs_id=srs_id,
                                            srs_bounds=srs_bounds)

        red_tile_data = Binary(get_raster_data("png"))
        blue_tile_data = Binary(get_raster_data("jpeg", "aliceblue"))

        red_tile_row_expected = 2
        red_tile_column_expected = 3
        red_tile_zoom_expected = 6

        blue_tile_row_expected = 3
        blue_tile_column_expected = 4
        blue_tile_zoom_expected = 5

        red_tile_information = TileDataInformation(tile_column=red_tile_column_expected, tile_row=red_tile_row_expected,
                                                   tile_zoom=red_tile_zoom_expected, tile_data=red_tile_data)

        blue_tile_information = TileDataInformation(tile_column=blue_tile_column_expected,
                                                    tile_row=blue_tile_row_expected, tile_zoom=blue_tile_zoom_expected,
                                                    tile_data=blue_tile_data)
        gpkg_writer.add_tiles(tiles=[red_tile_information, blue_tile_information])

        TestGeoPackageWriter.assert_all_raster_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_name=tiles_table_name)

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            red_tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                          table_name=tiles_table_name,
                                                                          tile_row=red_tile_row_expected,
                                                                          tile_column=red_tile_column_expected,
                                                                          zoom_level=red_tile_zoom_expected)
            assert red_tile_returned == Binary(red_tile_data)

            blue_tile_returned = GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                           table_name=tiles_table_name,
                                                                           tile_row=blue_tile_row_expected,
                                                                           tile_column=blue_tile_column_expected,
                                                                           zoom_level=blue_tile_zoom_expected)
            assert blue_tile_returned == Binary(blue_tile_data)

            # assert the tile matrix entry was added
            tile_matrix_returned_red = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=red_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_red) == 1
            assert tile_matrix_returned_red[0].table_name == tiles_table_name and \
                   tile_matrix_returned_red[0].matrix_height == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].matrix_width == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].zoom_level == red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].pixel_x_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].pixel_y_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].tile_height == 256 and \
                   tile_matrix_returned_red[0].tile_width == 256

            # assert the tile matrix entry was added
            tile_matrix_returned_blue = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=blue_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_blue) == 1
            assert tile_matrix_returned_blue[0].table_name == tiles_table_name and \
                   tile_matrix_returned_blue[0].matrix_height == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].matrix_width == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].zoom_level == blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].pixel_x_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].pixel_y_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].tile_height == 256 and \
                   tile_matrix_returned_blue[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == srs_id and \
                   tile_matrix_set_returned.min_x == srs_bounds.min_x and \
                   tile_matrix_set_returned.min_y == srs_bounds.min_y and \
                   tile_matrix_set_returned.max_x == srs_bounds.max_x and \
                   tile_matrix_set_returned.max_y == srs_bounds.max_y and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == srs_id and \
                   content.min_x == srs_bounds.min_x and \
                   content.min_y == srs_bounds.min_y and \
                   content.max_x == srs_bounds.max_x and \
                   content.max_y == srs_bounds.max_y and \
                   content.data_type == "tiles"

    def test_adding_tiles_geojson_tiles(self, get_gpkg_file_path):
        tiles_table_name = "Pirate_peg_leg"
        srs_id = 3857
        earth_radius_meters = 6378137

        srs_bounds = BoundingBox(min_x=-math.pi * earth_radius_meters,
                                 min_y=-math.pi * earth_radius_meters,
                                 max_x=math.pi * earth_radius_meters,
                                 max_y=math.pi * earth_radius_meters)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=join(get_gpkg_file_path),
                                            tile_table_name=tiles_table_name,
                                            srs_id=srs_id,
                                            srs_bounds=srs_bounds)

        red_tile_data = TEST_GEOJSON
        blue_tile_data = TEST_GEOJSON_2

        red_tile_row_expected = 2
        red_tile_column_expected = 3
        red_tile_zoom_expected = 6

        blue_tile_row_expected = 3
        blue_tile_column_expected = 4
        blue_tile_zoom_expected = 5

        red_tile_information = TileDataInformation(tile_column=red_tile_column_expected, tile_row=red_tile_row_expected,
                                                   tile_zoom=red_tile_zoom_expected, tile_data=red_tile_data)

        blue_tile_information = TileDataInformation(tile_column=blue_tile_column_expected,
                                                    tile_row=blue_tile_row_expected, tile_zoom=blue_tile_zoom_expected,
                                                    tile_data=blue_tile_data)

        gpkg_writer.add_tiles(tiles=[red_tile_information, blue_tile_information])

        TestGeoPackageWriter.assert_all_vector_tiles_tables(gpkg_file_path=gpkg_writer.file_path,
                                                            tiles_table_extension=
                                                            GeoPackageGeoJSONVectorTilesTableAdapter(tiles_table_name))

        all_tiles = gpkg_writer.get_all_tiles(tiles=[red_tile_information, blue_tile_information])

        red_tile_returned = next((tile
                                  for tile
                                  in all_tiles
                                  if tile.tile_row == red_tile_row_expected and \
                                  tile.tile_column == red_tile_column_expected and \
                                  tile.zoom_level == red_tile_zoom_expected),
                                 None)

        assert red_tile_returned is not None
        assert red_tile_returned.tile_data == red_tile_data

        blue_tile_returned = next((tile
                                   for tile
                                   in all_tiles
                                   if tile.tile_row == blue_tile_row_expected and \
                                   tile.tile_column == blue_tile_column_expected and \
                                   tile.zoom_level == blue_tile_zoom_expected),
                                  None)

        assert blue_tile_returned is not None
        assert blue_tile_returned.tile_data == blue_tile_data

        # assert the tile was added properly
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            # assert the tile matrix entry was added
            tile_matrix_returned_red = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=red_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_red) == 1
            assert tile_matrix_returned_red[0].table_name == tiles_table_name and \
                   tile_matrix_returned_red[0].matrix_height == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].matrix_width == 2 ** red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].zoom_level == red_tile_zoom_expected and \
                   tile_matrix_returned_red[0].pixel_x_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].pixel_y_size == 2445.98490512564 and \
                   tile_matrix_returned_red[0].tile_height == 256 and \
                   tile_matrix_returned_red[0].tile_width == 256

            # assert the tile matrix entry was added
            tile_matrix_returned_blue = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name,
                zoom_level=blue_tile_zoom_expected)
            # check to make sure only one
            assert len(tile_matrix_returned_blue) == 1
            assert tile_matrix_returned_blue[0].table_name == tiles_table_name and \
                   tile_matrix_returned_blue[0].matrix_height == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].matrix_width == 2 ** blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].zoom_level == blue_tile_zoom_expected and \
                   tile_matrix_returned_blue[0].pixel_x_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].pixel_y_size == 4891.96981025128 and \
                   tile_matrix_returned_blue[0].tile_height == 256 and \
                   tile_matrix_returned_blue[0].tile_width == 256

            # assert the tile matrix set entry was added
            tile_matrix_set_returned = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tiles_table_name)
            assert tile_matrix_set_returned.srs_id == srs_id and \
                   tile_matrix_set_returned.min_x == srs_bounds.min_x and \
                   tile_matrix_set_returned.min_y == srs_bounds.min_y and \
                   tile_matrix_set_returned.max_x == srs_bounds.max_x and \
                   tile_matrix_set_returned.max_y == srs_bounds.max_y and \
                   tile_matrix_set_returned.table_name == tiles_table_name

            # assert the vector tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tiles_table_name)
            assert content.table_name == tiles_table_name and \
                   content.srs_id == srs_id and \
                   content.min_x == srs_bounds.min_x and \
                   content.min_y == srs_bounds.min_y and \
                   content.max_x == srs_bounds.max_x and \
                   content.max_y == srs_bounds.max_y and \
                   content.data_type == VECTOR_TILES_DATA_TYPE

    def test_adding_tiles_bulk_invalid_tiles_empty(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tiles(tiles=[])

    def test_adding_tiles_bulk_invalid_tiles_none(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tiles(tiles=None)

    def test_adding_tiles_bulk_invalid_tiles_types_mixed(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=9804,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        with raises(ValueError):
            gpkg_writer.add_tiles(tiles=[
                TileDataInformation(tile_column=1, tile_row=1, tile_zoom=1, tile_data=get_raster_data("jpeg")),
                TileDataInformation(tile_column=0, tile_row=0, tile_zoom=2,
                                    tile_data=test_utility.get_mapbox_vector_tile_data())])

    def test_remove_tile(self, get_gpkg_file_path):
        tile_datas = [Binary(get_raster_data("png")),
                      Binary(get_raster_data("jpeg"))]

        for tile_data in tile_datas:
            gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                tile_table_name="table",
                                                srs_id=4326,
                                                srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

            tile_to_remove = TileDataInformation(tile_column=0,
                                                 tile_row=1,
                                                 tile_zoom=2,
                                                 tile_data=tile_data)
            gpkg_writer.add_tile_by_coordinate(tile_cooordinate=TileCoordinate(tile_zoom=tile_to_remove.zoom_level,
                                                                               tile_row=tile_to_remove.tile_row,
                                                                               tile_column=tile_to_remove.tile_column),
                                               tile_data=tile_to_remove.tile_data)

            with get_database_connection(gpkg_writer.file_path) as db_conn:
                assert GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                 table_name=gpkg_writer.tile_table_name,
                                                                 zoom_level=tile_to_remove.zoom_level,
                                                                 tile_column=tile_to_remove.tile_column,
                                                                 tile_row=tile_to_remove.tile_row) == tile_to_remove.tile_data

                assert len(GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                      tile_table_name=gpkg_writer.tile_table_name,
                                                                                      zoom_level=tile_to_remove.zoom_level)) == 1

            gpkg_writer.remove_tile(tile=TileCoordinate(tile_zoom=tile_to_remove.zoom_level,
                                                        tile_row=tile_to_remove.tile_row,
                                                        tile_column=tile_to_remove.tile_column))
            with get_database_connection(gpkg_writer.file_path) as db_conn:
                assert GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                                 table_name=gpkg_writer.tile_table_name,
                                                                 zoom_level=tile_to_remove.zoom_level,
                                                                 tile_column=tile_to_remove.tile_column,
                                                                 tile_row=tile_to_remove.tile_row) is None

                assert len(GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                      tile_table_name=gpkg_writer.tile_table_name,
                                                                                      zoom_level=tile_to_remove.zoom_level)) == 0

    def test_remove_tile_mapbox(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        tile_to_remove = TileDataInformation(tile_column=0,
                                             tile_row=1,
                                             tile_zoom=2,
                                             tile_data=test_utility.get_mapbox_vector_tile_data())
        gpkg_writer.add_tile_by_coordinate(tile_cooordinate=TileCoordinate(tile_zoom=tile_to_remove.zoom_level,
                                                                           tile_row=tile_to_remove.tile_row,
                                                                           tile_column=tile_to_remove.tile_column),
                                           tile_data=tile_to_remove.tile_data)

        with get_database_connection(gpkg_writer.file_path) as db_conn:
            assert GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                             table_name=gpkg_writer.tile_table_name,
                                                             zoom_level=tile_to_remove.zoom_level,
                                                             tile_column=tile_to_remove.tile_column,
                                                             tile_row=tile_to_remove.tile_row) == tile_to_remove.tile_data

            assert len(GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                  tile_table_name=gpkg_writer.tile_table_name,
                                                                                  zoom_level=tile_to_remove.zoom_level)) == 1

        gpkg_writer.remove_tile(tile=TileCoordinate(tile_zoom=tile_to_remove.zoom_level,
                                                    tile_row=tile_to_remove.tile_row,
                                                    tile_column=tile_to_remove.tile_column))
        with get_database_connection(gpkg_writer.file_path) as db_conn:
            assert GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                             table_name=gpkg_writer.tile_table_name,
                                                             zoom_level=tile_to_remove.zoom_level,
                                                             tile_column=tile_to_remove.tile_column,
                                                             tile_row=tile_to_remove.tile_row) is None

            assert len(GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db_conn.cursor(),
                                                                                  tile_table_name=gpkg_writer.tile_table_name,
                                                                                  zoom_level=tile_to_remove.zoom_level)) == 0

    def test_remove_tile_before_adding(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        tile_not_in_table = TileCoordinate(tile_column=1,
                                           tile_row=2,
                                           tile_zoom=3)
        with raises(ValueError):
            gpkg_writer.remove_tile(tile=tile_not_in_table)

    def test_remove_tile_not_in_table(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        tile_not_in_table = TileCoordinate(tile_column=1,
                                           tile_row=2,
                                           tile_zoom=3)
        # add a tile
        gpkg_writer.add_tile(tile_row=1,
                             tile_column=tile_not_in_table.tile_column + 1,
                             tile_zoom=tile_not_in_table.zoom_level + 1,
                             tile_data=Binary(get_raster_data("png")))

        # remove a tile not in table
        gpkg_writer.remove_tile(tile=tile_not_in_table)

        with get_database_connection(gpkg_writer.file_path) as db_conn:
            assert GeoPackageTilesTableAdapter.get_tile_data(cursor=db_conn.cursor(),
                                                             table_name=gpkg_writer.tile_table_name,
                                                             zoom_level=tile_not_in_table.zoom_level,
                                                             tile_column=tile_not_in_table.tile_column,
                                                             tile_row=tile_not_in_table.tile_row) is None

    def test_remove_one_zoom_level(self, get_gpkg_file_path):

        zoom_level_to_delete = 9
        tiles_to_delete = make_tiles(10, zoom_level=zoom_level_to_delete)

        zoom_level_to_stay = 6
        tiles_to_stay = make_tiles(7, zoom_level=zoom_level_to_stay)

        all_tiles = []
        for tile in tiles_to_stay:
            all_tiles.append(tile)

        for tile in tiles_to_delete:
            all_tiles.append(tile)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        gpkg_writer.add_tiles(all_tiles)

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()

            assert len(GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                     tile_table_name=gpkg_writer.tile_table_name,
                                                                     tiles=all_tiles)) == len(all_tiles)

        gpkg_writer.remove_zoom_levels(zoom_levels=[zoom_level_to_delete])
        db_conn.commit()

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()
            tiles_after_deletion = GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                                 tile_table_name=gpkg_writer.tile_table_name,
                                                                                 tiles=all_tiles)

            assert len(tiles_after_deletion) == len(tiles_to_stay)

            for expected_tile in tiles_to_stay:
                assert expected_tile.tile_data == next(returned_tile
                                                       for returned_tile
                                                       in tiles_after_deletion
                                                       if returned_tile.tile_row == expected_tile.tile_row
                                                       and returned_tile.tile_column == expected_tile.tile_column
                                                       and returned_tile.zoom_level == expected_tile.zoom_level).tile_data

    def test_remove_two_zoom_levels(self, get_gpkg_file_path):

        zoom_level_to_delete_1 = 9
        tiles_to_delete = make_tiles(10, zoom_level=zoom_level_to_delete_1)

        zoom_level_to_delete_2 = 6
        tiles_to_stay = make_tiles(7, zoom_level=zoom_level_to_delete_2)

        all_tiles = []
        for tile in tiles_to_stay:
            all_tiles.append(tile)

        for tile in tiles_to_delete:
            all_tiles.append(tile)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))

        gpkg_writer.add_tiles(all_tiles)

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()

            assert len(GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                     tile_table_name=gpkg_writer.tile_table_name,
                                                                     tiles=all_tiles)) == len(all_tiles)

        gpkg_writer.remove_zoom_levels(zoom_levels=[zoom_level_to_delete_1, zoom_level_to_delete_2])
        db_conn.commit()

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()
            tiles_after_deletion = GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                                 tile_table_name=gpkg_writer.tile_table_name,
                                                                                 tiles=all_tiles)

            assert len(tiles_after_deletion) == 0

    def test_invalid_tile_type_contents_entry(self, make_gpkg):

        invalid_tile_entry = ContentEntry(table_name="some_table",
                                          identifier="identfier",
                                          srs_id=4326,
                                          min_x=1,
                                          min_y=2,
                                          max_x=3,
                                          max_y=4,
                                          data_type="invalid_type")

        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            create_table(cursor=cursor,
                         table_name=invalid_tile_entry.table_name)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=invalid_tile_entry)

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=make_gpkg.file_path,
                                            tile_table_name=invalid_tile_entry.table_name,
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        # unsupported tile type-- since the contents entry says its invalid_type instead of tiles or vector-tiles
        with raises(ValueError):
            gpkg_writer.add_tile(tile_row=1,
                                 tile_column=2,
                                 tile_zoom=3,
                                 tile_data=get_raster_data("jpeg"))

    def test_remove_zoom_levels_with_no_tiles_table(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="some table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(1.0, 2.0, 3.0, 4.0))
        with raises(ValueError):
            gpkg_writer.remove_zoom_levels(zoom_levels=[1])

    def test_tile_table_exists_no_entry_in_gpkg_contents(self, get_gpkg_file_path):

        tile_table_name = "tile_table"
        with get_database_connection(file_path=get_gpkg_file_path) as db:
            db.cursor().execute("""
                                    CREATE TABLE IF NOT EXISTS "{table_name}"
                                    (id          INTEGER PRIMARY KEY AUTOINCREMENT, -- Autoincrement primary key
                                     zoom_level  INTEGER NOT NULL,                  -- min(zoom_level) <= zoom_level <= max(zoom_level) for t_table_name
                                     tile_column INTEGER NOT NULL,                  -- 0 to tile_matrix matrix_width - 1
                                     tile_row    INTEGER NOT NULL,                  -- 0 to tile_matrix matrix_height - 1
                                     tile_data   BLOB    NOT NULL,                  -- Of an image MIME type specified in clauses Tile Encoding PNG, Tile Encoding JPEG, Tile Encoding WEBP
                                     UNIQUE (zoom_level, tile_column, tile_row))
                                 """.format(table_name=tile_table_name))

        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name=tile_table_name,
                                            srs_id=4235,
                                            srs_bounds=BoundingBox(min_x=1.0, min_y=2.0, max_x=3.0, max_y=4.0))

        gpkg_writer.add_tile(tile_row=1,
                             tile_column=2,
                             tile_zoom=3,
                             tile_data=Binary(get_raster_data(image_type="png")))

        self.assert_all_raster_tiles_tables(gpkg_file_path=get_gpkg_file_path,
                                            tiles_table_name=tile_table_name)

        with get_database_connection(file_path=get_gpkg_file_path) as db_conn:
            # assert the tiles table is in the gpkg contents table
            content = GeoPackageCoreTableAdapter.get_content_entry_by_table_name(cursor=db_conn.cursor(),
                                                                                 table_name=tile_table_name)
            assert content.table_name == tile_table_name and \
                   content.srs_id == 4235 and \
                   content.min_x == 1.0 and \
                   content.min_y == 2.0 and \
                   content.max_x == 3.0 and \
                   content.max_y == 4.0 and \
                   content.data_type == TILES_DATA_TYPE

            # assert tile matrix set entry is in the gpkg_tile_matrix_set table
            tile_matrix_set = GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(
                cursor=db_conn.cursor(),
                tile_table_name=tile_table_name)

            assert tile_matrix_set.srs_id == 4235 and \
                   tile_matrix_set.min_x == 1.0 and \
                   tile_matrix_set.min_y == 2.0 and \
                   tile_matrix_set.max_x == 3.0 and \
                   tile_matrix_set.max_y == 4.0 and \
                   tile_matrix_set.table_name == tile_table_name

    def test_thread_safety(self, get_gpkg_file_path):
        tile_table_name = 'table table'
        srs_id = 4326
        srs_bounds = BoundingBox(1.0, 2.0, 3.0, 4.0)
        t = ExThread(target=lambda: write_tiles(GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                                      tile_table_name=tile_table_name,
                                                                      srs_id=srs_id,
                                                                      srs_bounds=srs_bounds)))
        t2 = ExThread(target=lambda: write_tiles(GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                                       tile_table_name=tile_table_name,
                                                                       srs_id=srs_id,
                                                                       srs_bounds=srs_bounds),
                                                 image_type='jpeg'))

        t3 = ExThread(target=lambda: write_tiles(GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                                       tile_table_name=tile_table_name,
                                                                       srs_id=srs_id,
                                                                       srs_bounds=srs_bounds)))
        t4 = ExThread(target=lambda: write_tiles(GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                                       tile_table_name=tile_table_name,
                                                                       srs_id=srs_id,
                                                                       srs_bounds=srs_bounds)))
        t5 = ExThread(target=lambda: write_tiles(GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                                                       tile_table_name=tile_table_name,
                                                                       srs_id=srs_id,
                                                                       srs_bounds=srs_bounds),
                                                 image_type='jpeg'))

        def write_tiles(gpkg_writer, image_type='png'):
            """
            writes a series of raster images
            :param gpkg_writer: the gpkg writer across multiple threads
            :type GeoPackageTilesWriter
            """
            gpkg_writer.add_tiles(make_tiles(number_of_tiles=100, zoom_level=19, image_type=image_type))
            gpkg_writer.close()

        threads = [t, t2, t3, t4, t5]
        for thread in threads:
            thread.start()
        for thread in threads:
            # ensure it throws if a thread
            thread.join_with_exception()

    def test_get_tile(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="tiles_table",
                                            srs_id=4326,
                                            srs_bounds=BoundingBox(-180, 180, -90, 90),
                                            tile_scheme=ZoomTimesTwo(base_matrix_width=2,
                                                                     base_matrix_height=1))
        tile_coord = TileCoordinate(tile_zoom=2,
                                    tile_row=0,
                                    tile_column=3)

        tile_returned = gpkg_writer.get_tile(tile=tile_coord)

        assert tile_returned is None

        tile_data_expected = Binary(get_raster_data(image_type="jpeg"))
        gpkg_writer.add_tile(tile_row=tile_coord.tile_row,
                             tile_column=tile_coord.tile_column,
                             tile_zoom=tile_coord.zoom_level,
                             tile_data=tile_data_expected)

        tile_returned_after_addition = gpkg_writer.get_tile(tile=tile_coord)

        assert tile_returned_after_addition is not None and \
               tile_returned_after_addition.zoom_level == tile_coord.zoom_level and \
               tile_returned_after_addition.tile_row == tile_coord.tile_row and \
               tile_returned_after_addition.tile_column == tile_coord.tile_column and \
               tile_returned_after_addition.tile_data == tile_data_expected

    def test_does_tile_table_exist(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="tableeeee",
                                            srs_id=3395,
                                            srs_bounds=BoundingBox(10, 20, 10, 20))

        assert not gpkg_writer.does_tiles_table_exist()

        gpkg_writer.add_tile(tile_zoom=2,
                             tile_row=0,
                             tile_column=1,
                             tile_data=Binary(get_raster_data("png")))

        assert gpkg_writer.does_tiles_table_exist()

    def test_get_entry(self, get_gpkg_file_path):
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="tableeeee",
                                            srs_id=3395,
                                            srs_bounds=BoundingBox(10, 20, 10, 20))

        assert gpkg_writer.get_gpkg_contents_entry() is None

        gpkg_writer.add_tile(tile_zoom=2,
                             tile_row=0,
                             tile_column=1,
                             tile_data=Binary(get_raster_data("png")))

        returned_entry = gpkg_writer.get_gpkg_contents_entry()

        assert returned_entry.srs_id == gpkg_writer.spatial_reference_system.srs_id and \
               returned_entry.table_name == gpkg_writer.tile_table_name and \
               returned_entry.data_type == TILES_DATA_TYPE and \
               returned_entry.min_x == gpkg_writer.srs_bounds.min_x and \
               returned_entry.min_y == gpkg_writer.srs_bounds.min_y and \
               returned_entry.max_x == gpkg_writer.srs_bounds.max_x and \
               returned_entry.max_y == gpkg_writer.srs_bounds.max_y

    def test_get_srs_by_id(self, get_gpkg_file_path):
        expected_srs = EllipsoidalMercator()
        gpkg_writer = GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                            tile_table_name="tableeeee",
                                            srs_id=expected_srs.srs_organization_coordsys_id,
                                            srs_bounds=BoundingBox(10, 20, 10, 20))

        returned_srs = gpkg_writer.get_srs_entry_by_id(expected_srs.srs_id)

        assert returned_srs.srs_id == expected_srs.srs_id and \
               returned_srs.organization_coordsys_id == expected_srs.organization_coordsys_id and \
               returned_srs.organization == expected_srs.organization and \
               returned_srs.description == expected_srs.description and \
               returned_srs.definition == expected_srs.definition and \
               returned_srs.srs_name == expected_srs.srs_name

    def test_create_directory(self):
        directory = join(gettempdir(), "doesnt_exist")
        full_path = join(directory, uuid4().hex + '.gpkg')
        GeoPackageTilesWriter(gpkg_file_path=full_path,
                              tile_table_name="tableeeee",
                              srs_id=EllipsoidalMercator().srs_organization_coordsys_id,
                              srs_bounds=BoundingBox(10, 20, 10, 20))

        assert exists(full_path)
        remove(full_path)

    def test_invalid_construction(self, get_gpkg_file_path):
        with raises(ValueError):
            # cannot do speed mode without saying the tile type
            with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                       tile_table_name='my tiles',
                                       srs_id=4326,
                                       srs_bounds=BoundingBox(min_x=-180.0,
                                                              max_x=180.0,
                                                              min_y=-90.0,
                                                              max_y=90.0),
                                       writing_mode=WritingMode.SPEED_MODE):
                pass

    def test_invalid_identifier(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            GeoPackageCoreTableAdapter.create_core_tables(cursor=cursor)
            existing_entry = ContentEntry(table_name='tabllle',
                                          data_type='stuff',
                                          identifier='will conflict',
                                          min_x=-180,
                                          min_y=-90.0,
                                          max_x=180.0,
                                          max_y=90.0,
                                          srs_id=4326)
            create_table(cursor=cursor,
                         table_name=existing_entry.table_name)
            GeoPackageCoreTableAdapter.insert_or_update_content(cursor=cursor,
                                                                content=existing_entry)
        # should raise an exception since there is already an entry in the geopackage contents table with that specified
        # identifier
        with raises(ValueError):
            with GeoPackageTilesWriter(gpkg_file_path=make_gpkg.file_path,
                                       tile_table_name='my tiles',
                                       srs_id=3395,
                                       srs_bounds=BoundingBox(min_x=-25.0,
                                                              min_y=-25.0,
                                                              max_x=25.0,
                                                              max_y=25.0),
                                       identifier=existing_entry.identifier):
                pass

    def test_speed_mode_initialization(self, get_gpkg_file_path):

        with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                   tile_table_name='my tiles',
                                   srs_id=4326,
                                   srs_bounds=BoundingBox(min_x=-180,
                                                          max_x=180,
                                                          min_y=-90,
                                                          max_y=90),
                                   tile_type=TileType.MAPBOX,
                                   writing_mode=WritingMode.SPEED_MODE,
                                   tile_scheme=ZoomTimesTwo(base_matrix_width=2,
                                                            base_matrix_height=1)) as gpkg_writer:
            self.assert_all_vector_tiles_tables(gpkg_file_path=get_gpkg_file_path,
                                                tiles_table_extension=GeoPackageMapBoxVectorTilesTableAdapter(
                                                    vector_tiles_table_name=gpkg_writer.tile_table_name))

        with get_database_connection(file_path=get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()

            matrix_entries = GeoPackageTilesTableAdapter.get_tile_matrix_entries_by_table_name(cursor=cursor,
                                                                                               tile_table_name=gpkg_writer.tile_table_name)

            assert len(matrix_entries) == 22
            matrix_entries.sort(key=lambda matrix_table_entry: matrix_table_entry.zoom_level)
            expected_pixel_size = 0.7031250  # should double each time
            expected_matrix_width = 2
            expected_matrix_height = 1
            for entry in matrix_entries:
                assert entry.table_name == gpkg_writer.tile_table_name and \
                       entry.pixel_x_size == expected_pixel_size and \
                       entry.pixel_y_size == expected_pixel_size and \
                       entry.matrix_width == expected_matrix_width and \
                       entry.matrix_height == expected_matrix_height and \
                       entry.tile_width == 256 and \
                       entry.tile_height == 256
                expected_matrix_height = expected_matrix_height * 2
                expected_matrix_width = expected_matrix_width * 2
                expected_pixel_size = expected_pixel_size / 2.0

    def test_add_tile_speed_mode(self, get_gpkg_file_path):

        with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                   tile_table_name='tiles my',
                                   srs_id=4326,
                                   srs_bounds=BoundingBox(min_x=-180.0,
                                                          max_x=180,
                                                          min_y=-90,
                                                          max_y=90),
                                   writing_mode=WritingMode.SPEED_MODE,
                                   tile_type=TileType.GEOJSON) as gpkg_writer:
            self.assert_all_vector_tiles_tables(gpkg_file_path=get_gpkg_file_path,
                                                tiles_table_extension=
                                                GeoPackageGeoJSONVectorTilesTableAdapter(vector_tiles_table_name=
                                                                                         gpkg_writer.tile_table_name))

            tile_row_expected = 1
            tile_column_expected = 2
            tile_zoom_expected = 3
            gpkg_writer.add_tile(tile_row=tile_row_expected,
                                 tile_column=tile_column_expected,
                                 tile_zoom=tile_zoom_expected,
                                 tile_data=TEST_GEOJSON_2)

            all_tiles_returned = gpkg_writer.get_all_tiles(tiles=[TileCoordinate(tile_column=tile_column_expected,
                                                                                 tile_row=tile_row_expected,
                                                                                 tile_zoom=tile_zoom_expected)])
            assert len(all_tiles_returned) == 1
            returned_tile = all_tiles_returned[0]
            assert returned_tile.tile_data == TEST_GEOJSON_2 and \
                   returned_tile.zoom_level == tile_zoom_expected and \
                   returned_tile.tile_column == tile_column_expected and \
                   returned_tile.tile_row == tile_row_expected

    def test_speed_mode_remove_tile(self, get_gpkg_file_path):

        with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                   tile_table_name='tiles my',
                                   srs_id=4326,
                                   srs_bounds=BoundingBox(min_x=-180.0,
                                                          max_x=180,
                                                          min_y=-90,
                                                          max_y=90),
                                   writing_mode=WritingMode.SPEED_MODE,
                                   tile_type=TileType.GEOJSON) as gpkg_writer:
            self.assert_all_vector_tiles_tables(gpkg_file_path=get_gpkg_file_path,
                                                tiles_table_extension=
                                                GeoPackageGeoJSONVectorTilesTableAdapter(vector_tiles_table_name=
                                                                                         gpkg_writer.tile_table_name))

            tile_row_expected = 1
            tile_column_expected = 2
            tile_zoom_expected = 3
            gpkg_writer.add_tile(tile_row=tile_row_expected,
                                 tile_column=tile_column_expected,
                                 tile_zoom=tile_zoom_expected,
                                 tile_data=TEST_GEOJSON_2)

            all_tiles_returned = gpkg_writer.get_all_tiles(tiles=[TileCoordinate(tile_column=tile_column_expected,
                                                                                 tile_row=tile_row_expected,
                                                                                 tile_zoom=tile_zoom_expected)])
            assert len(all_tiles_returned) == 1
            returned_tile = all_tiles_returned[0]
            assert returned_tile.tile_data == TEST_GEOJSON_2 and \
                   returned_tile.zoom_level == tile_zoom_expected and \
                   returned_tile.tile_column == tile_column_expected and \
                   returned_tile.tile_row == tile_row_expected

            gpkg_writer.remove_tile(tile=TileCoordinate(tile_column=tile_column_expected,
                                                        tile_row=tile_row_expected,
                                                        tile_zoom=tile_zoom_expected))

            all_tiles_after_removal = gpkg_writer.get_all_tiles(tiles=[TileCoordinate(tile_column=tile_column_expected,
                                                                                      tile_row=tile_row_expected,
                                                                                      tile_zoom=tile_zoom_expected)])
            assert len(all_tiles_after_removal) == 0

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()
            matrix_entry = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=cursor,
                                                                                      tile_table_name=gpkg_writer.tile_table_name,
                                                                                      zoom_level=tile_zoom_expected)
            assert matrix_entry is not None

    def test_speed_mode_remove_zoom_level(self, get_gpkg_file_path):
        with GeoPackageTilesWriter(gpkg_file_path=get_gpkg_file_path,
                                   tile_table_name='tiles my',
                                   srs_id=4326,
                                   srs_bounds=BoundingBox(min_x=-180.0,
                                                          max_x=180,
                                                          min_y=-90,
                                                          max_y=90),
                                   writing_mode=WritingMode.SPEED_MODE,
                                   tile_type=TileType.GEOJSON) as gpkg_writer:
            self.assert_all_vector_tiles_tables(gpkg_file_path=get_gpkg_file_path,
                                                tiles_table_extension=
                                                GeoPackageGeoJSONVectorTilesTableAdapter(vector_tiles_table_name=
                                                                                         gpkg_writer.tile_table_name))

            tile_row_expected = 1
            tile_column_expected = 2
            tile_zoom_expected = 3
            gpkg_writer.add_tile(tile_row=tile_row_expected,
                                 tile_column=tile_column_expected,
                                 tile_zoom=tile_zoom_expected,
                                 tile_data=TEST_GEOJSON_2)

            all_tiles_returned = gpkg_writer.get_all_tiles(tiles=[TileCoordinate(tile_column=tile_column_expected,
                                                                                 tile_row=tile_row_expected,
                                                                                 tile_zoom=tile_zoom_expected)])
            assert len(all_tiles_returned) == 1
            returned_tile = all_tiles_returned[0]
            assert returned_tile.tile_data == TEST_GEOJSON_2 and \
                   returned_tile.zoom_level == tile_zoom_expected and \
                   returned_tile.tile_column == tile_column_expected and \
                   returned_tile.tile_row == tile_row_expected

            gpkg_writer.remove_zoom_levels(zoom_levels=[tile_zoom_expected])

            all_tiles_after_removal = gpkg_writer.get_all_tiles(tiles=[TileCoordinate(tile_column=tile_column_expected,
                                                                                      tile_row=tile_row_expected,
                                                                                      tile_zoom=tile_zoom_expected)])
            assert len(all_tiles_after_removal) == 0

        with get_database_connection(get_gpkg_file_path) as db_conn:
            cursor = db_conn.cursor()
            matrix_entry = GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=cursor,
                                                                                      tile_table_name=gpkg_writer.tile_table_name,
                                                                                      zoom_level=tile_zoom_expected)
            assert matrix_entry is not None

    @pytest.fixture(scope="function")
    def get_gpkg_file_path(self):
        gpkg_file_path = join(gettempdir(), uuid4().hex + '.gpkg')
        yield gpkg_file_path
        remove(gpkg_file_path)

    @staticmethod
    def assert_default_tiles_tables(gpkg_file_path, tiles_table_name):
        """
        Checks to make sure all the tiles tables are accounted for: gpkg_tile_matrix_set, gpkg_tile_matrix,
        <tile_pyramid_user_data_table>
        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str
        :param tiles_table_name: the name of the tiles table
        :type tiles_table_name: str
        """
        with get_database_connection(gpkg_file_path) as db_conn:
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_TILE_MATRIX_TABLE_NAME)

            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME)

            assert table_exists(cursor=db_conn.cursor(),
                                table_name=tiles_table_name)

    @staticmethod
    def assert_default_core_tables(gpkg_file_path):
        """
        Checks to make sure all the core tables are accounted for: gpkg_contents, gpkg_spatial_ref_sys

        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str
        """
        with get_database_connection(gpkg_file_path) as db_conn:
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_CONTENTS_TABLE_NAME)

            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_SPATIAL_REFERENCE_SYSTEM_TABLE_NAME)

    @staticmethod
    def assert_default_vector_tiles_tables(gpkg_file_path):
        """
        Checks to make sure all the vector tables are accounted for: gpkgext_vt_layers, gpkgext_vt_fields

        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str
        """
        with get_database_connection(gpkg_file_path) as db_conn:
            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_VECTOR_LAYERS_TABLE_NAME)

            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_VECTOR_FIELDS_TABLE_NAME)

            assert table_exists(cursor=db_conn.cursor(),
                                table_name=GEOPACKAGE_EXTENSIONS_TABLE_NAME)

    @staticmethod
    def assert_vector_tiles_extensions_registered(gpkg_file_path,
                                                  tiles_table_extension):
        """
        Checks to make sure all the vector extensions are accounted for: gpkgext_vt_layers, gpkgext_vt_fields,
        and the vector tiles table with the different encoding

        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str

        :param tiles_table_extension: MapBoxVectorTilesExtension object or GeoJSONVectorTilesExtension object
        :type tiles_table_extension: Extension
        """
        with get_database_connection(gpkg_file_path) as db_conn:
            assert GeoPackageExtensionsTableAdapter.has_extension(cursor=db_conn.cursor(),
                                                                  extension=GeoPackageVectorLayersTableAdapter())

            assert GeoPackageExtensionsTableAdapter.has_extension(cursor=db_conn.cursor(),
                                                                  extension=GeoPackageVectorFieldsTableAdapter())

            assert GeoPackageExtensionsTableAdapter.has_extension(cursor=db_conn.cursor(),
                                                                  extension=tiles_table_extension)

    @staticmethod
    def assert_all_vector_tiles_tables(gpkg_file_path,
                                       tiles_table_extension):
        """
        Checks all the basic tables are existent for vector tiles and if the extensions have been registered properly

        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str

        :param tiles_table_extension: MapBoxVectorTilesExtension object or GeoJSONVectorTilesExtension object
        :type tiles_table_extension: Extension
        """

        TestGeoPackageWriter.assert_default_core_tables(gpkg_file_path=gpkg_file_path)
        TestGeoPackageWriter.assert_default_tiles_tables(gpkg_file_path=gpkg_file_path,
                                                         tiles_table_name=tiles_table_extension.table_name)
        TestGeoPackageWriter.assert_default_vector_tiles_tables(gpkg_file_path=gpkg_file_path)
        TestGeoPackageWriter.assert_vector_tiles_extensions_registered(gpkg_file_path=gpkg_file_path,
                                                                       tiles_table_extension=tiles_table_extension)

    @staticmethod
    def assert_all_raster_tiles_tables(gpkg_file_path,
                                       tiles_table_name):
        """
        Checks all the basic tables are existent for raster tiles

        :param gpkg_file_path: path to the gpkg to check
        :type gpkg_file_path: str

        :param tiles_table_name: the name of the tiles table
        :type tiles_table_name: str
        """

        TestGeoPackageWriter.assert_default_core_tables(gpkg_file_path=gpkg_file_path)
        TestGeoPackageWriter.assert_default_tiles_tables(gpkg_file_path=gpkg_file_path,
                                                         tiles_table_name=tiles_table_name)
