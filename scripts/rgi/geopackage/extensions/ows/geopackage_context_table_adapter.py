#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
import datetime
from sqlite3 import Cursor

from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter
from rgi.geopackage.extensions.ows.context_entry import ContextEntry
from rgi.geopackage.utility.sql_column_query import SqlColumnQuery
from rgi.geopackage.utility.sql_utility import select_query, select_all_query, table_exists, insert_or_update_row


CONTEXT_TABLE_NAME = "gpkgext_context"
EXTENSION_NAME = "ows_context"

#TODO: Contact / email the appropriate source for a default context definition.
GEOPACKAGE_EXTENSION_CONTEXT_DEFINITION = "http://www.owscontext.org/#standard"


class GeoPackageContextTableAdapter(ExtensionEntry):
    """
    Object representing a GeoPackage Context.
    """

    def __init__(self):
        super(GeoPackageContextTableAdapter, self).__init__(table_name=CONTEXT_TABLE_NAME,
                                                            column_name=None,
                                                            scope=EXTENSION_READ_WRITE_SCOPE,
                                                            definition=GEOPACKAGE_EXTENSION_CONTEXT_DEFINITION,
                                                            extension_name=EXTENSION_NAME)

    @staticmethod
    def create_context_table(cursor):
        """
        Creates the table gpkgext_context to store all Geopackage context entries.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor
        """
        cursor.execute(""" 
                        CREATE TABLE IF NOT EXISTS {table_name}
                        (
                        id INTEGER  PRIMARY KEY  ASC NOT NULL  UNIQUE,
                        title	TEXT  NOT NULL,
                        abstract TEXT  DEFAULT "",
                        last_change	DATETIME  NOT NULL,
                        min_x DOUBLE,
                        min_y DOUBLE,
                        max_x DOUBLE,
                        max_y DOUBLE,
                        srs_id INTEGER,
                        author TEXT,
                        publisher TEXT,
                        creator	TEXT,
                        rights	TEXT,
                        keywords TEXT,
                        metadata_id	INTEGER,
                        min_time DATETIME,
                        max_time DATETIME,
                        FOREIGN KEY('srs_id') REFERENCES 'gpkg_spatial_ref_sys'('srs_id'),
                        FOREIGN KEY('metadata_id') REFERENCES 'gpkg_metadata'('id')
                        ); """.format(table_name=CONTEXT_TABLE_NAME))

        # register extension in the extensions table
        if GeoPackageExtensionsTableAdapter.has_extension(cursor=cursor,
                                                          extension=GeoPackageContextTableAdapter()):
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=GeoPackageContextTableAdapter())

    @staticmethod
    def get_context_entry(cursor, context_id):
        """
        Returns a single GeoPackage context entry based on a given context id.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_id: the id of the resource we're looking for
        :type context_id: int

        :return A single record in the gpkgext_context table
        :rtype: ContextEntry
        """

        rows = select_query(cursor=cursor,
                            table_name=CONTEXT_TABLE_NAME,
                            select_columns=['id', 'title', 'abstract', 'last_change', 'min_x', 'min_y', 'max_x',
                                            'max_y', 'srs_id', 'author', 'publisher', 'creator', 'rights', 'keywords',
                                            'metadata_id', 'min_time', 'max_time'],
                            where_columns_dictionary={'id': context_id})

        if rows is None or len(rows) == 0:
            return None

        row = rows[0]
        return ContextEntry(context_id=row['id'],
                            title=row['title'],
                            abstract=row['abstract'],
                            last_change=row['last_change'],
                            min_x=row['min_x'],
                            min_y=row['min_y'],
                            max_x=row['max_x'],
                            max_y=row['max_y'],
                            srs_id=row['srs_id'],
                            author=row['author'],
                            publisher=row['publisher'],
                            creator=row['creator'],
                            rights=row['rights'],
                            keywords=row['keywords'],
                            metadata_id=row['metadata_id'],
                            min_time=row['min_time'],
                            max_time=row['max_time'])

    @staticmethod
    def get_all_context_entries(cursor):
        """
        Returns all GeoPackage context entries (rows) from the gpkgext_context table.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :return All context entries from the gpkgext_context table
        :rtype: list of ContextEntry
        """

        rows = select_all_query(cursor, CONTEXT_TABLE_NAME)

        return [ContextEntry(context_id=row['id'],
                             title=row['title'],
                             abstract=row['abstract'],
                             last_change=row['last_change'],
                             min_x=row['min_x'],
                             min_y=row['min_y'],
                             max_x=row['max_x'],
                             max_y=row['max_y'],
                             srs_id=row['srs_id'],
                             author=row['author'],
                             publisher=row['publisher'],
                             creator=row['creator'],
                             rights=row['rights'],
                             keywords=row['keywords'],
                             metadata_id=row['metadata_id'],
                             min_time=row['min_time'],
                             max_time=row['max_time']) for row in rows]

    @staticmethod
    def insert_or_update_context_entry(cursor, context_entry):
        """
        Inserts a new GeoPackage context entry (row) - or, updates an existing GeoPackage context entry - into the
        gpkgext_context table.

        :param cursor: The cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_entry: A context object for a GeoPackage
        :type context_entry: ContextEntry
        """

        if not table_exists(cursor, table_name=CONTEXT_TABLE_NAME):
            GeoPackageContextTableAdapter.create_context_table(cursor=cursor)

        insert_or_update_row(cursor=cursor,
                             table_name=CONTEXT_TABLE_NAME,
                             sql_columns_list=[
                                               SqlColumnQuery(column_name="id",
                                                              include_in_where_clause=True,
                                                              column_value=context_entry.context_id),
                                               SqlColumnQuery(column_name="title",
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.title),
                                               SqlColumnQuery(column_name='abstract',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.abstract),
                                               SqlColumnQuery(column_name='last_change',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.last_change),
                                               SqlColumnQuery(column_name='min_x',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.min_x),
                                               SqlColumnQuery(column_name='min_y',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.min_y),
                                               SqlColumnQuery(column_name='max_x',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.max_x),
                                               SqlColumnQuery(column_name='max_y',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.max_y),
                                               SqlColumnQuery(column_name='keywords',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.keywords),
                                               SqlColumnQuery(column_name='srs_id',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.srs_id),
                                               SqlColumnQuery(column_name='author',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.author),
                                               SqlColumnQuery(column_name='publisher',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.publisher),
                                               SqlColumnQuery(column_name='creator',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.creator),
                                               SqlColumnQuery(column_name='rights',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.rights),
                                               SqlColumnQuery(column_name='metadata_id',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.metadata_id),
                                               SqlColumnQuery(column_name='min_time',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.min_time),
                                               SqlColumnQuery(column_name='max_time',
                                                              include_in_where_clause=False,
                                                              column_value=context_entry.max_time)
                             ])

