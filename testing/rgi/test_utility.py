from os.path import join, dirname
from typing import Callable
from testing import resources

try:
    import Queue as queue
except:
    import queue
import sqlite3
import sys
import threading

from rgi.geopackage.tiles.tile_data_information import TileDataInformation
from PIL.Image import new
from rgi.packaging.tiles2gpkg_parallel import img_to_buf


class ExThread(threading.Thread):
    """
    Thread that will throw an exception to the main thread when joined.
    """

    def __init__(self, target):
        """
        Constructor

        :param target: the function to call on the thread
        :type target: Callable[]
        """
        threading.Thread.__init__(self)
        self.__status_queue = queue.Queue()
        self.target = target

    def run_with_exception(self):
        """This method should be overriden."""
        thread_name = threading.current_thread().name
        self.target()

    def run(self):
        """This method should NOT be overriden."""
        try:
            self.run_with_exception()
        except BaseException:
            self.__status_queue.put(sys.exc_info())
        self.__status_queue.put(None)

    def wait_for_exc_info(self):
        return self.__status_queue.get()

    def join(self, timeout=None, balancing=True):
        super(ExThread, self).join(timeout=timeout)
        self.join_with_exception()

    def join_with_exception(self):
        """
        join the thread but allow exception to be thrown on the main thread if the thread throws an exception
        :raises: Exception
        """
        ex_info = self.wait_for_exc_info()
        if ex_info is None:
            return
        else:
            raise ex_info[1]


def make_tiles(number_of_tiles, zoom_level, image_type='png'):
    tiles = []
    tile_row = 0
    tile_column = 0
    max_row = 2 ** zoom_level
    max_column = 2 ** zoom_level
    for number in range(number_of_tiles):
        tiles.append(TileDataInformation(tile_row=tile_row,
                                         tile_column=tile_column,
                                         tile_zoom=zoom_level,
                                         tile_data=sqlite3.Binary(get_raster_data(image_type=image_type))))

        tile_row += 1

        if tile_row >= max_row:
            tile_row = 0
            tile_column += 1

            if tile_column >= max_column:
                raise ValueError("cannot make this many tiles at zoom level: {z}".format(z=zoom_level))

    return tiles


def get_raster_data(image_type, color="red"):
    img = new("RGB", (256, 256), color)
    return img_to_buf(img, image_type).read()


def get_sld_xml():
    with open(join(dirname(resources.__file__),
                   "SLD_Overlay.xml"),
              'rb') as in_file:
        data = in_file.read()
        in_file.close()
        return sqlite3.Binary(data)


def get_sld_style_data_path():
    return join(dirname(resources.__file__), 'sld_style_data.xml')


def get_mapbox_style_data_path():
    return join(dirname(resources.__file__), 'mapbox_style_data.txt')


def get_cmss_style_data_path():
    return join(dirname(resources.__file__), 'cmss_style_data.txt')


def get_file_as_binary(file_path):
    with open(file_path, 'rb') as in_file:
        data = in_file.read()
        in_file.close()
        return sqlite3.Binary(data)


def get_mapbox_vector_tile_data():
    with open(join(dirname(resources.__file__),
                   "tile_data.pbf"),
              'rb') as in_file:
        data = in_file.read()
        in_file.close()
        return sqlite3.Binary(data)


def get_mapbox_vector_tile_data2():
    with open(join(dirname(resources.__file__),
                   "tile_data2.pbf"),
              'rb') as in_file:
        data = in_file.read()
        in_file.close()
        return sqlite3.Binary(data)


def create_table(cursor,
                 table_name):
    """
    Creates a table in a database
    :param cursor: connection to the database that the table should be created
    :type cursor: Cursor

    :param table_name: the name of the table to create
    :type table_name: str
    """
    cursor.execute("""
                            CREATE TABLE '{table_name}' (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                                        col_1 TEXT);
                       """.format(table_name=table_name))


TEST_GEOJSON = """
                 {
                     "type": "FeatureCollection",
                     "features": 
                     [
                         {
                             "geometry": 
                             {
                                 "type": "Point",
                                 "coordinates": 
                                 [
                                     -8247861.1000836585,
                                     4970241.327215323
                                 ]
                             },
                             "type": "Feature",
                             "properties": 
                             {
                                 "hello": "world",
                                 "h": "world",
                                 "count": 1.23
                             }
                         },
                         {
                             "geometry": 
                             {
                                 "type": "Point",
                                 "coordinates": 
                                 [
                                     -8247861.1000836585,
                                     4970241.327215323
                                 ]
                             },
                             "type": "Feature",
                             "properties": 
                             {
                                 "hello": "again",
                                 "count": 2
                             }
                         }
                     ]
                 }
                 """

TEST_GEOJSON_2 = """
                {
                    "type": "FeatureCollection",
                    "features": 
                    [
                        {
                            "geometry": 
                            {
                                "type": "LineString",
                                "coordinates": 
                                [
                                    [30, 10], [10, 30], [40, 40]
                                ]
                            },
                            "type": "Feature", 
                            "properties": 
                            {
                                "prop1": "world1",
                                "prop2": "world",
                                "prop3": 1.23
                            }
                        },
                        {
                            "geometry": 
                            {
                                "type":  "Polygon", 
                                "coordinates": 
                                [
                                     [[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]
                                ]
                            },
                            "type": "Feature",
                            "properties": 
                            {
                                "prop4": "again",
                                "prop5": 2
                            }
                        }
                    ]
                }
                """
