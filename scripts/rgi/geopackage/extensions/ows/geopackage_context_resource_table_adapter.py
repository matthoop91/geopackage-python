#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""

from sqlite3 import Cursor

from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter
from rgi.geopackage.extensions.ows.context_resource_entry import ContextResourceEntry
from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE
from rgi.geopackage.utility.sql_column_query import SqlColumnQuery
from rgi.geopackage.utility.sql_utility import select_query, select_all_query, table_exists, insert_or_update_row


CONTEXT_RESOURCE_TABLE_NAME = "gpkgext_context_resources"
EXTENSION_NAME = "ows_context"

#TODO: Contact / email the appropriate source for a default request URL.
EXTENSION_CONTEXT_RESOURCE_DEFINITION = "http://www.owscontext.org/#standard"


class GeoPackageContextResourceTableAdapter(ExtensionEntry):
    """
    Object representing a GeoPackage Context Resource.
    """

    def __init__(self):
        super(GeoPackageContextResourceTableAdapter, self).__init__(table_name=CONTEXT_RESOURCE_TABLE_NAME,
                                                                    column_name=None,
                                                                    scope=EXTENSION_READ_WRITE_SCOPE,
                                                                    definition=EXTENSION_CONTEXT_RESOURCE_DEFINITION,
                                                                    extension_name=EXTENSION_NAME)

    @staticmethod
    def create_context_resource_table(cursor):
        """
        Creates the table gpkgext_context_resources to store all Geopackage context resource entries.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor
        """
        cursor.execute(""" 
                        CREATE TABLE IF NOT EXISTS {table_name}
                        (
                        `id`	INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT UNIQUE,
                        'context_id' INTEGER NOT NULL,
                        `title`	TEXT NOT NULL,
                        `code`	TEXT NOT NULL,
                        `abstract` TEXT DEFAULT "",
                        `author` TEXT,
                        `publisher` TEXT,
                        `rights` TEXT,
                        `min_x`	DOUBLE,
                        `min_y`	DOUBLE,
                        `max_x`	DOUBLE,
                        `max_y`	DOUBLE,
                        `srs_id` INTEGER,
                        `min_time`	DATETIME,
                        `max_time`	DATETIME,
                        `description` TEXT,
                        `active` BOOLEAN DEFAULT TRUE,
                        `keywords` TEXT,
                        `min_scale_denominator`	DOUBLE,
                        `max_scale_denominator`	DOUBLE,
                        `order`	DOUBLE,
                        `requestURL` TEXT,
                        `layer_name` TEXT,
                        `query`	TEXT,
                        FOREIGN KEY('context_id') REFERENCES 'gpkg_context'('id'),
                        FOREIGN KEY('srs_id') REFERENCES 'gpkg_spatial_ref_sys'('srs_id')
                        );
                        """.format(table_name=CONTEXT_RESOURCE_TABLE_NAME))

        if GeoPackageExtensionsTableAdapter.has_extension(cursor=cursor,
                                                          extension=GeoPackageContextResourceTableAdapter()):
            GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                             extension=GeoPackageContextResourceTableAdapter())

    @staticmethod
    def get_context_resource_entry(cursor, context_resource_id):
        """
        Returns a single GeoPackage resource entry based on a given resource id.

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_resource_id: the id of the context resource entry we're looking for
        :type context_resource_id: int

        :return A single record in the gpkgext_context_resources table
        :rtype: ContextResourceEntry
        """

        rows = select_query(cursor=cursor,
                            table_name=CONTEXT_RESOURCE_TABLE_NAME,
                            select_columns=['id', 'context_id', 'title', 'code', 'abstract', 'author', 'publisher', 'rights',
                                            'min_x', 'min_y', 'max_x', 'max_y', 'srs_id', 'min_time', 'max_time',
                                            'description', 'active', 'keywords', 'min_scale_denominator',
                                            'max_scale_denominator', 'order', 'requestURL', 'layer_name', 'query'],
                            where_columns_dictionary={'id': context_resource_id})

        if rows is None or len(rows) == 0:
            return None

        row = rows[0]

        return ContextResourceEntry(context_resource_id=row['id'],
                                    context_id=row['context_id'],
                                    title=row['title'],
                                    code=row['code'],
                                    abstract=row['abstract'],
                                    author=row['author'],
                                    publisher=row['publisher'],
                                    rights=row['rights'],
                                    min_x=row['min_x'],
                                    min_y=row['min_y'],
                                    max_x=row['max_x'],
                                    max_y=row['max_y'],
                                    srs_id=row['srs_id'],
                                    min_time=row['min_time'],
                                    max_time=row['max_time'],
                                    description=row['description'],
                                    active=row['active'],
                                    keywords=row['keywords'],
                                    min_scale_denominator=row['min_scale_denominator'],
                                    max_scale_denominator=row['max_scale_denominator'],
                                    order=row['order'],
                                    request_url=row['requestURL'],
                                    layer_name=row['layer_name'],
                                    query=row['query'])

    @staticmethod
    def get_all_context_resource_entries(cursor):
        """
        Returns all GeoPackage context resource entries (rows) from the gpkgext_context_resources table.

        :param cursor: The cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :return: All context resource entries from the gpkgext_context_resources table
        :rtype: list of ContextResourceEntry
        """

        rows = select_all_query(cursor=cursor, table_name=CONTEXT_RESOURCE_TABLE_NAME)

        return [ContextResourceEntry(context_resource_id=row['id'],
                                     context_id=row['context_id'],
                                     title=row['title'],
                                     code=row['code'],
                                     abstract=row['abstract'],
                                     author=row['author'],
                                     publisher=row['publisher'],
                                     rights=row['rights'],
                                     min_x=row['min_x'],
                                     min_y=row['min_y'],
                                     max_x=row['max_x'],
                                     max_y=row['max_y'],
                                     srs_id=row['srs_id'],
                                     min_time=row['min_time'],
                                     max_time=row['max_time'],
                                     description=row['description'],
                                     active=row['active'],
                                     keywords=row['keywords'],
                                     min_scale_denominator=row['min_scale_denominator'],
                                     max_scale_denominator=row['max_scale_denominator'],
                                     order=row['order'],
                                     request_url=row['requestURL'],
                                     layer_name=row['layer_name'],
                                     query=row['query']) for row in rows]

    @staticmethod
    def insert_or_update_context_resource(cursor, context_resource_entry):
        """
        Returns all GeoPackage context resource entries (rows) from the gpkgext_context_resources table.

        :param cursor: The cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param context_resource_entry: A context resource object for a geopackage.
        :type context_resource_entry: ContextResourceEntry
        """

        if not table_exists(cursor, table_name=CONTEXT_RESOURCE_TABLE_NAME):
            GeoPackageContextResourceTableAdapter.create_context_resource_table(cursor=cursor)

        insert_or_update_row(cursor=cursor,
                             table_name=CONTEXT_RESOURCE_TABLE_NAME,
                             sql_columns_list=[
                                     SqlColumnQuery(column_name='id',
                                                    include_in_where_clause=True,
                                                    column_value=context_resource_entry.context_resource_id),
                                     SqlColumnQuery(column_name='context_id',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.context_id),
                                     SqlColumnQuery(column_name='title',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.title),
                                     SqlColumnQuery(column_name='code',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.code),
                                     SqlColumnQuery(column_name='abstract',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.abstract),
                                     SqlColumnQuery(column_name='author',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.author),
                                     SqlColumnQuery(column_name='publisher',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.publisher),
                                     SqlColumnQuery(column_name='rights',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.rights),
                                     SqlColumnQuery(column_name='min_x',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.min_x),
                                     SqlColumnQuery(column_name='min_y',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.min_y),
                                     SqlColumnQuery(column_name='max_x',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.max_x),
                                     SqlColumnQuery(column_name='max_y',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.max_y),
                                     SqlColumnQuery(column_name='srs_id',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.srs_id),
                                     SqlColumnQuery(column_name='min_time',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.min_time),
                                     SqlColumnQuery(column_name='max_time',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.max_time),
                                     SqlColumnQuery(column_name='description',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.description),
                                     SqlColumnQuery(column_name='active',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.active),
                                     SqlColumnQuery(column_name='keywords',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.keywords),
                                     SqlColumnQuery(column_name='min_scale_denominator',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.min_scale_denominator),
                                     SqlColumnQuery(column_name='max_scale_denominator',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.max_scale_denominator),
                                     SqlColumnQuery(column_name='order',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.order),
                                     SqlColumnQuery(column_name='requestURL',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.request_url),
                                     SqlColumnQuery(column_name='layer_name',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.layer_name),
                                     SqlColumnQuery(column_name='query',
                                                    include_in_where_clause=False,
                                                    column_value=context_resource_entry.query)
                                 ])





