#!/usr/bin/python
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Steven D. Lander
Date: 2015-11
Requires: Python Imaging Library (PIL or Pillow), Sqlite3
Description: Test cases for gdal2tiles_parallel.py

Version:
"""
import os
import shutil
from os.path import join, basename
from tempfile import gettempdir
from uuid import uuid4

import pytest

from scripts.rgi.geopackage.common.bounding_box import BoundingBox
from scripts.rgi.geopackage.writers.geopackage_tiles_writer import GeoPackageTilesWriter
from scripts.rgi.geopackage.geopackage import Geopackage
from scripts.rgi.geopackage.nsg_geopackage import NsgGeopackage
from testing.rgi import test_utility
from testing.rgi.test_tiles2gpkg import DEFAULT_TILES_TABLE_NAME


def pytest_addoption(parser):
    """
    Adds command line options to testing
    :param parser: the command line parser
    """
    # adding command line option to exclude the gdal tests
    parser.addoption("--exclude-gdal", action="store_true",
                     help="exclude gdal dependent tests")


def pytest_runtest_setup(item):
    """
    setup method before tests are run
    :param item:
    """
    # adding the ability to skip gdal tests before executing them
    if 'requires_gdal' in item.keywords and item.config.getvalue("--exclude-gdal"):
        pytest.skip("excluding tests that require gdal")

@pytest.fixture(scope="function")
def temp_file_gpkg_ext():
    filename = uuid4().hex + '.gpkg'
    tmp_file = join(gettempdir(), filename)
    yield tmp_file
    os.remove(tmp_file)

@pytest.fixture(scope="function")
def make_gpkg(tiles_table_name='tiles'):
    filename = uuid4().hex + '.gpkg'
    tmp_file = join(gettempdir(), filename)
    yield Geopackage(tmp_file, 4326, tiles_table_name)
    print ("deleting file")
    os.remove(tmp_file)


@pytest.fixture(scope="function")
def make_gpkg_nsg():
    filename = uuid4().hex + '.gpkg'
    tmp_file = join(gettempdir(), filename)
    yield NsgGeopackage(tmp_file, 4326, DEFAULT_TILES_TABLE_NAME)
    print ("deleting nsg file")
    os.remove(tmp_file)

@pytest.fixture(scope="function")
def get_vector_tiles_gpkg(make_gpkg):
    with GeoPackageTilesWriter(gpkg_file_path=make_gpkg.file_path,
                               tile_table_name='my-vector-tiles',
                               srs_id=3857,
                               srs_bounds=BoundingBox(min_x=-50,
                                                      min_y=-50,
                                                      max_x=50,
                                                      max_y=50)) as gpkg_writer:
        gpkg_writer.add_tile(tile_row=1,
                             tile_column=2,
                             tile_zoom=3,
                             tile_data=test_utility.get_mapbox_vector_tile_data())
        return gpkg_writer

@pytest.fixture(scope="function")
def copy_to_temp_file():
    def create_temporary_copy(path):
        temp_path = os.path.join(gettempdir(), basename(path))
        shutil.copy2(path, temp_path)
        return temp_path

    return create_temporary_copy
