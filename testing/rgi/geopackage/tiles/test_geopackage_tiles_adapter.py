#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from sqlite3 import Binary, OperationalError

from pytest import raises

from rgi.geopackage.common.tile_coordinate import TileCoordinate
from rgi.geopackage.tiles.geopackage_abstract_tiles_table_adapter import GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME, \
    GEOPACKAGE_TILE_MATRIX_TABLE_NAME
from rgi.geopackage.tiles.tile_data_information import TileDataInformation
from rgi.geopackage.tiles.tile_matrix_entry import TileMatrixEntry
from rgi.packaging.tiles2gpkg_parallel import img_to_buf
from rgi.geopackage.tiles.geopackage_tiles_table_adapter import GeoPackageTilesTableAdapter
from rgi.geopackage.tiles.tiles_content_entry import TilesContentEntry
from rgi.geopackage.utility.sql_utility import get_database_connection
from PIL.Image import new

from testing.rgi.test_utility import get_raster_data, make_tiles, create_table


class TestGeoPackageTilesTableAdapter(object):

    def test_insert_or_update_invalid_tile_matrix_set_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_gpkg_tile_matrix_row(cursor=db.cursor(),
                                                                                  tile_matrix_entry=TileMatrixEntry(
                                                                                      table_name='table',
                                                                                      zoom_level=0,
                                                                                      matrix_width=1,
                                                                                      matrix_height=1,
                                                                                      tile_width=1,
                                                                                      tile_height=1,
                                                                                      pixel_x_size=1.0,
                                                                                      pixel_y_size=1.0))

    def test_insert_or_update_invalid_pyramid_user_data_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            tile = TileDataInformation(tile_row=1,
                                       tile_column=2,
                                       tile_zoom=3,
                                       tile_data=Binary(get_raster_data("jpeg")))

            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_tile_data(cursor=db.cursor(),
                                                                       tile_column=tile.tile_column,
                                                                       tile_row=tile.tile_row,
                                                                       tile_data=tile.tile_data,
                                                                       zoom_level=tile.zoom_level,
                                                                       table_name=table_name)

    def test_insert_or_update_bulk_invalid_pyramid_user_data_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            tiles = make_tiles(number_of_tiles=2, zoom_level=18)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_tile_data_bulk(cursor=db.cursor(),
                                                                            tiles=tiles,
                                                                            table_name=table_name)

    def test_get_tile_data_invalid_pyramid_user_data_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_tile_data(cursor=db.cursor(),
                                                          table_name=table_name,
                                                          zoom_level=1,
                                                          tile_column=1,
                                                          tile_row=1)

    def test_get_all_tile_data_invalid_pyramid_user_data_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            tiles = make_tiles(number_of_tiles=2, zoom_level=18)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_all_tile_data(cursor=db.cursor(),
                                                              tile_table_name=table_name,
                                                              tiles=tiles)

    def test_delete_zoom_level_invalid_pyramid_user_data_table(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.delete_zoom_level(cursor=db.cursor(),
                                                              tile_table_name=table_name,
                                                              zoom_level=1)

    def test_get_tile_matrix_set_entry_invalid_tile_matrix_set(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_tile_matrix_set_entry_by_table_name(cursor=db.cursor(),
                                                                                    tile_table_name=table_name)

    def test_get_tile_matrix_for_zoom_level_invalid_tile_matrix_set(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_TILE_MATRIX_TABLE_NAME)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db.cursor(),
                                                                           tile_table_name=table_name,
                                                                           zoom_level=1)

    def test_get_tile_matrix_entries_by_table_name_invalid_tile_matrix_set(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_TILE_MATRIX_TABLE_NAME)

            tile_matrix = TileMatrixEntry(table_name=table_name,
                                          zoom_level=1,
                                          pixel_x_size=1.0,
                                          pixel_y_size=2.0,
                                          matrix_width=2,
                                          matrix_height=2,
                                          tile_width=256,
                                          tile_height=256)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_gpkg_tile_matrix_row(cursor=db.cursor(),
                                                                                  tile_matrix_entry=tile_matrix)

    def test_insert_or_update_gpkg_tile_matrix_row_invalid_tile_matrix_set(self, temp_file_gpkg_ext):
        with get_database_connection(temp_file_gpkg_ext) as db:
            table_name = 'tablename'
            create_table(cursor=db.cursor(),
                         table_name=table_name)
            create_table(cursor=db.cursor(),
                         table_name=GEOPACKAGE_TILE_MATRIX_SET_TABLE_NAME)
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_tile_matrix_for_zoom_level(cursor=db.cursor(),
                                                                           tile_table_name=table_name,
                                                                           zoom_level=1)

    def test_insert_gpkg_tile_matrix_row_no_table(self, make_gpkg):

        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_udpate_gpkg_tile_matrix_row(cursor=cursor,
                                                                                  table_name="table",
                                                                                  zoom_level=0,
                                                                                  matrix_width=1,
                                                                                  matrix_height=1,
                                                                                  tile_width=256,
                                                                                  tile_height=256,
                                                                                  pixel_x_size=1.1,
                                                                                  pixel_y_size=2.2)

    def test_insert_gpkg_tile_matrix_set_row_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            tile_table_name = 'table'
            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_gpkg_tile_matrix_set_row(cursor=cursor,
                                                                                      tiles_content=TilesContentEntry(
                                                                                          table_name=tile_table_name,
                                                                                          identifier="id",
                                                                                          min_x=0,
                                                                                          min_y=0,
                                                                                          max_y=0,
                                                                                          max_x=0,
                                                                                          srs_id=0))

    def test_get_gpkg_tile_matrix_rows(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            tile_table_name = 'table_name1'

            tile_entry = TilesContentEntry(table_name=tile_table_name,
                                           identifier="id",
                                           min_x=0,
                                           min_y=0,
                                           max_y=0,
                                           max_x=0,
                                           srs_id=0)

            tile_matrix_1 = TileMatrixEntry(table_name=tile_table_name,
                                            zoom_level=1,
                                            pixel_x_size=1.0,
                                            pixel_y_size=2.0,
                                            matrix_width=2,
                                            matrix_height=2,
                                            tile_width=256,
                                            tile_height=256)

            tile_matrix_2 = TileMatrixEntry(table_name=tile_table_name,
                                            zoom_level=2,
                                            pixel_x_size=1.0,
                                            pixel_y_size=2.0,
                                            matrix_width=4,
                                            matrix_height=4,
                                            tile_width=256,
                                            tile_height=256)

            tile_matrix_3 = TileMatrixEntry(table_name=tile_table_name,
                                            zoom_level=3,
                                            pixel_x_size=1.0,
                                            pixel_y_size=2.0,
                                            matrix_width=16,
                                            matrix_height=16,
                                            tile_width=256,
                                            tile_height=256)

            tile_matrix_4 = TileMatrixEntry(table_name=tile_table_name + "different",
                                            zoom_level=2,
                                            pixel_x_size=1.0,
                                            pixel_y_size=2.0,
                                            matrix_width=4,
                                            matrix_height=4,
                                            tile_width=256,
                                            tile_height=256)

            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=tile_entry)
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(
                                                                           table_name=tile_matrix_4.table_name,
                                                                           identifier="is",
                                                                           min_x=1.0,
                                                                           min_y=2.0,
                                                                           max_x=3.0,
                                                                           max_y=3.0,
                                                                           srs_id=4326))

            for tile_matrix_entry in [tile_matrix_1, tile_matrix_2, tile_matrix_3, tile_matrix_4]:
                GeoPackageTilesTableAdapter.insert_or_update_gpkg_tile_matrix_row(cursor=cursor,
                                                                                  tile_matrix_entry=tile_matrix_entry)
            matrices_returned = GeoPackageTilesTableAdapter.get_tile_matrix_entries_by_table_name(cursor=cursor,
                                                                                                  tile_table_name=tile_table_name)
            assert len(matrices_returned) == 3

    def test_insert_tile_data_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            with raises(ValueError):
                img = new("RGB", (256, 256), "red")
                data = img_to_buf(img, 'jpeg').read()
                GeoPackageTilesTableAdapter.insert_or_update_tile_data(cursor=cursor,
                                                                       table_name="table",
                                                                       tile_column=1,
                                                                       tile_row=1,
                                                                       tile_data=Binary(data),
                                                                       zoom_level=0)

    def test_create_two_pyramid_user_data_tables_with_same_name(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            table_name = "Table repeated yo"
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(
                                                                           table_name=table_name,
                                                                           identifier="blah blah",
                                                                           min_x=0.0,
                                                                           min_y=0.0,
                                                                           max_x=1.0,
                                                                           max_y=1.0,
                                                                           srs_id=4326))
            with raises(ValueError):
                GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                           tiles_content=TilesContentEntry(
                                                                               table_name=table_name,
                                                                               identifier="woot",
                                                                               min_x=-1.0,
                                                                               min_y=-2.0,
                                                                               max_x=2.0,
                                                                               max_y=3.0,
                                                                               srs_id=3395))

    def test_add_tile_data_bulk_no_table(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            with raises(ValueError):
                GeoPackageTilesTableAdapter.insert_or_update_tile_data_bulk(cursor=cursor,
                                                                            table_name="table that doesn't exist",
                                                                            tiles=[TileDataInformation(tile_row=1,
                                                                                                       tile_column=2,
                                                                                                       tile_zoom=3,
                                                                                                       tile_data=get_raster_data(
                                                                                                           "png"))])

    def test_get_all_tile_data(self, make_gpkg):
        gpkg = make_gpkg
        tiles = make_tiles(number_of_tiles=1001, zoom_level=18)
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            table_name = "test tiles"
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(
                                                                           table_name=table_name,
                                                                           identifier="something",
                                                                           min_x=10.0,
                                                                           min_y=11.0,
                                                                           max_x=100.0,
                                                                           max_y=102.0,
                                                                           srs_id=4326))
            GeoPackageTilesTableAdapter.insert_or_update_tile_data_bulk(cursor=cursor,
                                                                        table_name=table_name,
                                                                        tiles=tiles)

            returned_tiles = GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                           tile_table_name=table_name,
                                                                           tiles=tiles)

            assert len(returned_tiles) == len(tiles)

            for expected_tile in tiles:
                assert expected_tile.tile_data == next(returned_tile
                                                       for returned_tile
                                                       in returned_tiles
                                                       if returned_tile.tile_row == expected_tile.tile_row
                                                       and returned_tile.tile_column == expected_tile.tile_column
                                                       and returned_tile.zoom_level == expected_tile.zoom_level).tile_data

    def test_get_all_tile_data_with_empty_coords(self, make_gpkg):
        with get_database_connection(make_gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            table_name = "test tiles"
            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=TilesContentEntry(table_name
                                                                                                       =table_name,
                                                                                                       identifier='id',
                                                                                                       min_x=0,
                                                                                                       min_y=0,
                                                                                                       max_x=1,
                                                                                                       max_y=1,
                                                                                                       srs_id=4326))
            tiles = GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                  tile_table_name=table_name,
                                                                  tiles=[])
            assert len(tiles) == 0

    def test_get_all_tile_data_no_table(self, make_gpkg):

        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            table_name = "test tiles"
            with raises(ValueError):
                GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                              tile_table_name=table_name,
                                                              tiles=[TileCoordinate(tile_row=0,
                                                                                    tile_column=1,
                                                                                    tile_zoom=3)])

    def test_delete_row(self, make_gpkg):
        gpkg = make_gpkg

        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()
            tiles_entry = TilesContentEntry(table_name="testtest",
                                            srs_id=4326,
                                            min_x=0.0,
                                            min_y=0.0,
                                            max_x=1.0,
                                            max_y=1.0,
                                            identifier="something")

            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=tiles_entry)

            tile = TileDataInformation(tile_row=1,
                                       tile_column=2,
                                       tile_zoom=3,
                                       tile_data=Binary(get_raster_data("jpeg")))

            GeoPackageTilesTableAdapter.insert_or_update_tile_data(cursor=cursor,
                                                                   tile_column=tile.tile_column,
                                                                   tile_row=tile.tile_row,
                                                                   tile_data=tile.tile_data,
                                                                   zoom_level=tile.zoom_level,
                                                                   table_name=tiles_entry.table_name)
            db_conn.commit()

            assert GeoPackageTilesTableAdapter.get_tile_data(cursor=cursor,
                                                             tile_row=tile.tile_row,
                                                             tile_column=tile.tile_column,
                                                             zoom_level=tile.zoom_level,
                                                             table_name=tiles_entry.table_name) is not None

            GeoPackageTilesTableAdapter.delete_tile_data(cursor=cursor,
                                                         tile_column=tile.tile_column,
                                                         tile_row=tile.tile_row,
                                                         zoom_level=tile.zoom_level,
                                                         table_name=tiles_entry.table_name)

            assert GeoPackageTilesTableAdapter.get_tile_data(cursor=cursor,
                                                             tile_row=tile.tile_row,
                                                             tile_column=tile.tile_column,
                                                             zoom_level=tile.zoom_level,
                                                             table_name=tiles_entry.table_name) is None

    def test_delete_rows_of_tile_data(self, make_gpkg):

        gpkg = make_gpkg
        with get_database_connection(gpkg.file_path) as db_conn:
            cursor = db_conn.cursor()

            zoom_level_to_delete = 9
            tiles_to_delete = make_tiles(10, zoom_level=zoom_level_to_delete)

            zoom_level_to_stay = 6
            tiles_to_stay = make_tiles(7, zoom_level=zoom_level_to_stay)

            tile_entry = TilesContentEntry(table_name="table test",
                                           identifier="test test",
                                           srs_id=4326,
                                           min_x=0.0,
                                           min_y=0.0,
                                           max_x=12.0,
                                           max_y=13.0)
            all_tiles = []
            for tile in tiles_to_stay:
                all_tiles.append(tile)

            for tile in tiles_to_delete:
                all_tiles.append(tile)

            GeoPackageTilesTableAdapter.create_pyramid_user_data_table(cursor=cursor,
                                                                       tiles_content=tile_entry)

            GeoPackageTilesTableAdapter.insert_or_update_tile_data_bulk(cursor=cursor,
                                                                        table_name=tile_entry.table_name,
                                                                        tiles=all_tiles)
            db_conn.commit()

            assert len(GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                     tile_table_name=tile_entry.table_name,
                                                                     tiles=all_tiles)) == len(all_tiles)

            GeoPackageTilesTableAdapter.delete_zoom_level(cursor=cursor,
                                                          tile_table_name=tile_entry.table_name,
                                                          zoom_level=zoom_level_to_delete)
            db_conn.commit()
            tiles_after_deletion = GeoPackageTilesTableAdapter.get_all_tile_data(cursor=cursor,
                                                                                 tile_table_name=tile_entry.table_name,
                                                                                 tiles=all_tiles)

            assert len(tiles_after_deletion) == len(tiles_to_stay)

            for expected_tile in tiles_to_stay:
                assert expected_tile.tile_data == next(returned_tile
                                                       for returned_tile
                                                       in tiles_after_deletion
                                                       if returned_tile.tile_row == expected_tile.tile_row
                                                       and returned_tile.tile_column == expected_tile.tile_column
                                                       and returned_tile.zoom_level == expected_tile.zoom_level).tile_data
