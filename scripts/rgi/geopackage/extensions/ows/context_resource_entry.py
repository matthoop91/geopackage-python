#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author:
    Frederick Boyd, Jr., Leidos
    Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-12-03
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""


class ContextResourceEntry(object):
    """
    Represents a row in the gpkgext_context_resources table.
    """

    def __init__(self,
                 context_resource_id,
                 context_id,
                 title,
                 code,
                 abstract,
                 author,
                 publisher,
                 rights,
                 min_x,
                 min_y,
                 max_x,
                 max_y,
                 description,
                 keywords,
                 min_scale_denominator,
                 max_scale_denominator,
                 order,
                 layer_name,
                 query,
                 request_url,
                 srs_id,
                 min_time,
                 max_time,
                 active=True
                 ):
        """
        Constructor

        :param context_resource_id: The unique identifier for a GeoPackage context.
        :type context_resource_id: int

        :param context_id: id from gpkgext_context.
        :type context_id: int

        :param title: A human readable title for the OWS context document.
        :type title: str

        :param abstract: A human readable description of the OWS Context document purpose and/or content.
        :type abstract: str

        :param min_x: Bounding box minimum easting or longitude for the users of the context document
        :type min_x: Double

        :param min_y: Bounding box minimum northing or latitude for the users of the context document
        :type min_y: Double

        :param max_x: Bounding box maximum easting or longitude for the users of the context document
        :type max_x: Double

        :param max_y: Bounding box maximum northing or latitude for the users of the context document
        :type max_y: Double

        :param srs_id: Spatial Reference System ID: gpkg_spatial_ref_sys.srs_id for the geographic extents
        :type srs_id: int

        :param author: Identifier for the author of the document
        :type author: str

        :param publisher: Identifier for the publisher of the document
        :type publisher: str

        :param creator: The tool / application used to create the context document and its properties
        :type creator: str

        :param rights: Rights which apply to the context document
        :type rights: str

        :param keywords: Comma-delimited list of keywords related to this context document
        :type keywords: str

        :param min_time: Beginning of the time interval in ISO-8601 format
        :type min_time: datetime

        :param max_time: End of the time interval in ISO-8601 format
        :type max_time: datetime

        :param description: A reference to a description of the Context resource in alternative format
        :type description:  str

        :param active: This flag indicates the state of the resource within the context document.  It can be interpreted
        by the caller as required (this may be defined in a profile or in the specific service extensions).
        :type active:  bool

        :param min_scale_denominator: Minimum scale for the display of the layer
        :type min_scale_denominator:  Double

        :param max_scale_denominator: Maximum scale for the display of the layer
        :type max_scale_denominator:  Double

        :param order: The ascending order of the resource.
        :type order:  Double

        :param request_url: Service request URL or file URL.
        :type request_url: str

        :param code: Code identifying the type of resource.
        :type code:  str

        :param layer_name: A single layer, table, or view name.
        :type layer_name:  str

        :param query: The actual SQL or HTTP query.
        :type query:  str

        """

        self.context_resource_id = context_resource_id
        self.context_id = context_id
        self.title = title
        self.code = code
        self.abstract = abstract
        self.author = author
        self.publisher = publisher
        self.rights = rights
        self.min_x = min_x
        self.min_y = min_y
        self.max_x = max_x
        self.max_y = max_y
        self.srs_id = srs_id
        self.min_time = min_time
        self.max_time = max_time
        self.description = description
        self.active = active
        self.keywords = keywords
        self.min_scale_denominator = min_scale_denominator
        self.max_scale_denominator = max_scale_denominator
        self.order = order
        self.request_url = request_url
        self.layer_name = layer_name
        self.query = query


