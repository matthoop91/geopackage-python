#!/usr/bin/python
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Authors:
    Jenifer Cochran
Date: 2019-2
Requires: Python Imaging Library (PIL or Pillow), Sqlite3
Description: Test cases for gdal2tiles_parallel.py

Version:
"""
from os.path import join, dirname

from rgi.tools.styled_layer_descriptor.styled_layer_descriptor_parser import StyledLayerDescriptorParser
from testing import resources

MOSQUE_W_URL = "http://vtp2018.s3-eu-west-1.amazonaws.com/static/mapstorestyle/sprites/mosque_w.svg"
SQUARE_W_URL = "http://vtp2018.s3-eu-west-1.amazonaws.com/static/mapstorestyle/sprites/square_w.svg"
PILON_WILSON_W_URL = "http://vtp2018.s3-eu-west-1.amazonaws.com/static/mapstorestyle/sprites/pylon_wilson_w.svg"
CIRCLE_W_URL = "http://vtp2018.s3-eu-west-1.amazonaws.com/static/mapstorestyle/sprites/circle_w.svg"


class TestStyledLayerDescriptorParser(object):

    def test_get_external_graphics(self):
        parser = StyledLayerDescriptorParser(sld_file_path=join(dirname(resources.__file__),
                                                                "SLD_Overlay.xml"))
        graphics = parser.get_external_graphics()

        assert len(graphics) == 4

        mosque_w_graphic = next((graphic
                                 for graphic
                                 in graphics
                                 if graphic.url_to_graphic == MOSQUE_W_URL),
                                None)

        assert mosque_w_graphic is not None
        assert mosque_w_graphic.graphic_format == 'image/svg'

        square_w_graphic = next((graphic
                                 for graphic
                                 in graphics
                                 if graphic.url_to_graphic == SQUARE_W_URL),
                                None)

        assert square_w_graphic is not None
        assert square_w_graphic.graphic_format == 'image/svg'

        pilon_wilson_w_graphic = next((graphic
                                       for graphic
                                       in graphics
                                       if graphic.url_to_graphic == PILON_WILSON_W_URL),
                                      None)

        assert pilon_wilson_w_graphic is not None
        assert pilon_wilson_w_graphic.graphic_format == 'image/svg'

        circle_w_graphic = next((graphic
                                 for graphic
                                 in graphics
                                 if graphic.url_to_graphic == CIRCLE_W_URL),
                                None)

        assert circle_w_graphic is not None
        assert circle_w_graphic.graphic_format == 'image/svg'
