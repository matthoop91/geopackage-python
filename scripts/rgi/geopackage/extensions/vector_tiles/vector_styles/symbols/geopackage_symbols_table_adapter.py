#!/usr/bin/python2.7
"""
Copyright (C) 2014 Reinventing Geospatial, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>,
or write to the Free Software Foundation, Inc., 59 Temple Place -
Suite 330, Boston, MA 02111-1307, USA.

Author: Jenifer Cochran, Reinventing Geospatial Inc (RGi)
Date: 2018-11-11
   Requires: sqlite3, argparse
   Optional: Python Imaging Library (PIL or Pillow)
Credits:
  MapProxy imaging functions: http://mapproxy.org
  gdal2mb on github: https://github.com/developmentseed/gdal2mb

Version:
"""
from rgi.geopackage.extensions.extension_entry import ExtensionEntry, EXTENSION_READ_WRITE_SCOPE
from rgi.geopackage.extensions.geopackage_extensions_table_adapter import GeoPackageExtensionsTableAdapter
from rgi.geopackage.extensions.vector_tiles.vector_styles.styles_extension_constants import \
    GEOPACKAGE_SYMBOLS_TABLE_NAME, GEOPACKAGE_STYLESHEETS_EXTENSION_NAME, STYLESHEETS_TILES_DEFINITION
from rgi.geopackage.extensions.vector_tiles.vector_styles.symbols.symbols_entry import SymbolsEntry
from rgi.geopackage.utility.sql_column_query import SqlColumnQuery
from rgi.geopackage.utility.sql_utility import table_exists, select_all_query, insert_or_update_row, select_query


class GeoPackageSymbolsTableAdapter(ExtensionEntry):
    """
    Represents the GeoPackage Symbols table (gpkgext_symbols). This was built off of a pre-liminary schema.
    Schema could be subject to change. Document is located on OGC website:
     https://portal.opengeospatial.org/wiki/pub/VectorTilesPilot/ConvertDocsOutputVTP/VTP/VTPExtension.pdf
    """

    def __init__(self):
        super(GeoPackageSymbolsTableAdapter, self).__init__(table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME,
                                                            column_name=None,
                                                            extension_name=GEOPACKAGE_STYLESHEETS_EXTENSION_NAME,
                                                            definition=STYLESHEETS_TILES_DEFINITION,
                                                            scope=EXTENSION_READ_WRITE_SCOPE)

    @staticmethod
    def create_symbols_table(cursor):
        """
        Creates the gpkgext_symbols table

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor
        """
        cursor.execute("""
                         CREATE TABLE IF NOT EXISTS {table_name}
                         (id            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  -- auto generated primary key
                          symbol_id     TEXT NOT NULL,                               -- a string identifier such as a URI that can uniquely identify the symbol
                          content_type  TEXT NOT NULL,                               -- is the media type (formerly MIME type, e.g., image/svg+xml) of the symbol
                          symbol        BLOB NOT NULL,                               -- is the actual symbol BLOB
                          title         TEXT,                                        -- is a text title
                          description   TEXT,                                        -- is a text description
                          UNIQUE(symbol_id, content_type));
                       """.format(table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME))

        # register the extension
        GeoPackageExtensionsTableAdapter.insert_or_update_extensions_row(cursor=cursor,
                                                                         extension=GeoPackageSymbolsTableAdapter())

    @staticmethod
    def get_all_symbol_entries(cursor):
        """
        Returns a list of Symbol entries that exist in the GeoPackage's gpkgext_symbols table

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :return a list of SymbolsEntry entries that exist in the GeoPackage's gpkgext_symbols table
        :rtype: list of SymbolsEntry
        """
        if not table_exists(cursor, GEOPACKAGE_SYMBOLS_TABLE_NAME):
            return []

        rows = select_all_query(cursor=cursor,
                                table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME)

        return [SymbolsEntry(id=row['id'],
                             symbol_id=row['symbol_id'],
                             content_type=row['content_type'],
                             symbol=row['symbol'],
                             title=row['title'],
                             description=row['description']) for row in rows]

    @staticmethod
    def insert_or_update_symbols_entry(cursor,
                                       symbols_entry):
        """
        Adds an entry to the Symbols Table (gpkgext_symbols) with the values given

        :param cursor: the cursor to the GeoPackage database's connection
        :type cursor: Cursor

        :param symbols_entry: the symbol entry to be added to the table
        :type symbols_entry: SymbolsEntry

        :return Updated SymbolEntry with the id value updated if needed
        """
        if not table_exists(cursor, GEOPACKAGE_SYMBOLS_TABLE_NAME):
            GeoPackageSymbolsTableAdapter.create_symbols_table(cursor=cursor)

        # use the unique constraint to do insert or update if id not set
        if symbols_entry.id is None:
            insert_or_update_row(cursor=cursor,
                                 table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME,
                                 sql_columns_list=[SqlColumnQuery(column_name='id',
                                                                  column_value=symbols_entry.id,
                                                                  include_in_where_clause=False,
                                                                  include_in_set_clause=False),
                                                   SqlColumnQuery(column_name='symbol_id',
                                                                  column_value=symbols_entry.symbol_id),
                                                   SqlColumnQuery(column_name='content_type',
                                                                  column_value=symbols_entry.content_type),
                                                   SqlColumnQuery(column_name='symbol',
                                                                  column_value=symbols_entry.symbol,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='title',
                                                                  column_value=symbols_entry.title,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='description',
                                                                  column_value=symbols_entry.description,
                                                                  include_in_where_clause=False)])
        else:
            insert_or_update_row(cursor=cursor,
                                 table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME,
                                 sql_columns_list=[SqlColumnQuery(column_name='id',
                                                                  column_value=symbols_entry.id),
                                                   SqlColumnQuery(column_name='symbol_id',
                                                                  column_value=symbols_entry.symbol_id,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='content_type',
                                                                  column_value=symbols_entry.content_type,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='symbol',
                                                                  column_value=symbols_entry.symbol,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='title',
                                                                  column_value=symbols_entry.title,
                                                                  include_in_where_clause=False),
                                                   SqlColumnQuery(column_name='description',
                                                                  column_value=symbols_entry.description,
                                                                  include_in_where_clause=False)])

        symbols_entry.id = select_query(cursor=cursor,
                                        select_columns=['id'],
                                        table_name=GEOPACKAGE_SYMBOLS_TABLE_NAME,
                                        where_columns_dictionary={'symbol_id': symbols_entry.symbol_id,
                                                                 'content_type': symbols_entry.content_type,
                                                                 'symbol': symbols_entry.symbol,
                                                                 'description': symbols_entry.description,
                                                                 'title': symbols_entry.title})[0]['id']

        return symbols_entry
